from setuptools import setup, find_packages

# copied from https://github.com/awslabs/git-remote-codecommit/blob/master/setup.py
import os
def read(fname):
  return open(os.path.join(os.path.dirname(__file__), fname)).read()
  

# follow https://github.com/awslabs/git-remote-codecommit/blob/master/setup.py
# and https://packaging.python.org/tutorials/packaging-projects/
from dolphicom import dolphicom_version
setup(
    name='dolphicom',
    version=dolphicom_version,
    author="Shadi Akiki",
    author_email="shadi@akiki.us",
    url='https://gitlab.com/dolphicom/dolphicom',
    description="Automatic detection of dolphin whistles in audio files",
    long_description = 'Automatic detection of dolphin whistles in audio files',
    long_description_content_type="text/markdown",
    
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
    ],
    # http://peak.telecommunity.com/DevCenter/setuptools#declaring-extras-optional-features-with-their-own-dependencies
    extras_require = {
    "minimal": [
      "click==7.1.2",
      "tqdm==4.62.3",
      "pydub==0.25.1",
    ],
    "full": [
      "click==7.1.2",
      "tqdm==4.62.3",
      "pydub==0.25.1",


      "drawille==0.1.0",
      "hdbscan==0.8.27",
      "matplotlib==3.4.2",

      # Update: upgrade to 1.0.3. I can't remember why I intentionally used v0. Not actively using the video creation feature in dolphicom anyway
      #"moviepy==0.2.3.5",
      "moviepy==1.0.3",

      "numpy==1.21.2",

      # Note: pandas dataframes saved with pandas 1.2.5 are not loadable with 1.3.0
      # Update: changing from 1.3.2 since as of today 2021-10-08, it errors on gitlab CI (py 3.9?) as "not available"
      #         The below will download 1.3.3. Note that "==1.3.2" and "==1.3" work on colab with py 3.7
      #         "==1.3" did not work with gitlab ci py 3.9?
      "pandas>=1.3,<1.4",


      # Update: change from 0.24.1 to >=.24,<.25 for the sake of gitlab CI (py 3.9). Similar to pandas version update
      "scikit-learn>=0.24,<0.25",

      "scipy==1.7.0",
      #"scipy>=1, <2",

      "youtube_dl==2021.06.06",
      "scikit_image==0.18.2",

      "dominate==2.6.0",

      # add cloud dependencies: aws, google storage
      # Update: change GCS from == to >= in order to keep any newer versions on python 3.9 if it comes pre-installed (vs colab that has python 3.7)
      # On py 3.9, this is 1.42.0
      # Update: roll back to == since issue was with Gitlab CI. Check notes in .gitlab-ci.yml
      "boto3==1.18.34",
      "google-cloud-storage==1.18.1",
    ]
    },
    entry_points='''
        [console_scripts]
        dolphicom=dolphicom.cli:cli
    ''',
)
