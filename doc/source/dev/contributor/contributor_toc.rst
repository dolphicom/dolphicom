.. _contributor-toc:

=======================
Dolphicom contributor guide
=======================

This guide is designed to help you quickly find the information you need about DOlphicom development. If you're new to this and want to start coding ASAP, you've found the right place.

- `dev-env` - the development environment is mostly just using google's colab for testing code interactively and seeing results. This is a data-intensive project, so unlike standard software projects, data forms an integral part of the development workflow. Visualizing results and judging quality by that is the main validation method at the project's current infancy stage. Contributors could create a personal fork of the Dolphicom repository on GitLab, using git to manage a local repository with development branches, performing an in-place build of Dolphicom, and creating a virtual environment that adds this development version of Dolphicom to the Python path
- `editing-dolphicom` - Dolphicom Python code is mostly split into the root folder in the git repo, called `dolphicom` and 2 subdirectories: `io` and `spectrogram_helpers`.
- `unit-tests` - dolphicom v0.5.0 unit tests are written with python's integrated `unittest` package. TBH, they probably don't pass as of v0.5.0
- `docs` - Documentation is currently just the README file

