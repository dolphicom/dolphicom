.. _scipy-roadmap:

Dolphicom Roadmap
=============

This roadmap page contains only the most important ideas and needs for Dolphicom
going forward.  A more detailed roadmap will be provided if this project survives funding.


Detection improvements
------------------------

Dolphicom v0.5.0 can detect about 7000 dolphin whistles in mobysound.org's 5th conference, bottlenose dolphin dataset.
By inspecting the gallery of spectrograms, it is evident that there are many more whistles that are not being detected.
The gallery is available at https://s3.us-west-2.amazonaws.com/dolphicom/dolphicom_results/t14.3-f2-v20210726a-html_gallery/index.html

Also, dolphicom detection of overlapping whistles needs improvement in order to separate overlapping whistles.

Increasing the detection accuracy to miss less whistles will encourage adoption of the package by various labs working on dolphin acoustics.


Benchmarking implementation
---------------------------

There is currently no single website that benchmarks different models on detection of whistles.

If a single website hosting the spectrograms is developed, dolphicom could get functionality that can publish
individual algorithm detected whistles to the same website for benchmarking of results.


Visualization
-------------

Datasets currently sit hidden in zip files, eg mobysound.org .
Dolphicom v0.5.0 already has functionality to generate spectrograms as images and thumbnails for publishing as websites,
and be able to visualize data audio segments without having to download the full multi-gigabyte zip files.
