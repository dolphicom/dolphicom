.. _governance:

========================
SciPy Project Governance
========================

The purpose of this document is to formalize the governance process
used by the Dolphicom project in both ordinary and extraordinary
situations, and to clarify how decisions are made and how the various
elements of our community interact, including the relationship between
open source collaborative development and work that may be funded by
for-profit or non-profit entities.


The Project
===========

The Dolphicom Project (The Project) is an open source software project.
The goal of The Project is to develop open source software for scientific
analysis of bioacoustics in Python, and, in particular, the ``dolphicom`` package. The Software
developed by The Project is released under the BSD (or similar) open source
license, developed openly and hosted on public GitLab repositories under
the ``dolphicom`` GitLab organization.

The Project is currently developed by 
Shadi Akiki, called Core Developer. It seeks to gain interest for development from other developers, called Contributors. Contributors are individuals who contribute code,
documentation, designs, or other work to the Project. Anyone can be a
Contributor. Contributors can be affiliated with any legal entity or
none. Contributors participate in the project by submitting, reviewing,
and discussing GitLab Pull Requests and Issues and participating in open
and public Project discussions on GitLab, mailing lists, and other
channels. The foundation of Project participation is openness and
transparency.

The Project Community consists of all Contributors and Users of the
Project. Contributors work on behalf of and are responsible to the
larger Project Community and we strive to keep the barrier between
Contributors and Users as low as possible.

The Project is not a legal entity, nor does it currently have any formal
relationships with legal entities.


Governance
==========

This section describes the governance and leadership model of The
Project.

The foundations of Project governance are:

-  openness and transparency
-  active contribution
-  institutional neutrality


As of 2021-08-03, Project leadership is provided by Core Developer,
whose active and consistent contributions have been
recognized by their receiving “commit rights” to the Project GitLab
repositories. In general, all Project decisions are made by
the Core Developer.

This approach serves the project well in its current baby status,
but if the Project grows, there will be a need for
a more formal governance model, including a Benevolent Dictator for Life, Steering Council, etc.


Institutional Partners and funding
==================================

The Core Developer is the primary leadership for the project. No
outside institution, individual, or legal entity has the ability to own,
control, usurp, or influence the project other than by participating in
the Project as Contributors. However, because
institutions can be an important funding mechanism for the project, it
is important to formally acknowledge institutional participation in the
project. These are Institutional Partners.

An Institutional Contributor is any individual Project Contributor who
contributes to the project as part of their official duties at an
Institutional Partner. Likewise, an Institutional Council Member is any
Project Steering Council Member who contributes to the project as part
of their official duties at an Institutional Partner.

With these definitions, an Institutional Partner is any recognized legal
entity in any country that employs at least 1 Institutional Contributor or
Institutional Council Member. Institutional Partners can be for-profit or
non-profit entities.

Institutions become eligible to become an Institutional Partner by
employing individuals who actively contribute to The Project as part of
their official duties. To state this another way, the only way for a
Partner to influence the project is by actively contributing to the open
development of the project, in equal terms to any other member of the
community of Contributors and Council Members. Merely using Project
Software in institutional context does not allow an entity to become an
Institutional Partner. Financial gifts do not enable an entity to become
an Institutional Partner. Once an institution becomes eligible for
Institutional Partnership, the Steering Council must nominate and
approve the Partnership.

If, at some point, an existing Institutional Partner stops having any
contributing employees, then a one year grace period commences. If, at
the end of this one-year period, they continue not to have any
contributing employees, then their Institutional Partnership will
lapse, and resuming it will require going through the normal process
for new Partnerships.

An Institutional Partner is free to pursue funding for their work on The
Project through any legal means. This could involve a non-profit
organization raising money from private foundations and donors or a
for-profit company building proprietary products and services that
leverage Project Software and Services. Funding acquired by
Institutional Partners to work on The Project is called Institutional
Funding. However, no funding obtained by an Institutional Partner can
override the Steering Council. If a Partner has funding to do SciPy work
and the Council decides to not pursue that work as a project, the
Partner is free to pursue it on their own. However, in this situation,
that part of the Partner’s work will not be under the SciPy umbrella and
cannot use the Project trademarks in any way that suggests a formal
relationship.

Institutional Partner benefits are:

-  acknowledgement on the Dolphicom website and in talks
-  ability to acknowledge their own funding sources on the Dolphicom
   website and in talks
-  ability to influence the project through the participation of their
   Contributors
-  invitation to Dolphicom Developer Meetings

There are currently no Institutional Partners.


Document history
================

https://gitlab.com/dolphicom/dolphicom/commits/master/doc/source/dev/governance/governance.rst

Acknowledgements
================

Substantial portions of this document were adapted from the
`SciPy project's governance document
<https://github.com/jupyter/governance/blob/master/governance.md>`_
which in its turn was adapted from the
`Jupyter/IPython project's governance document
<https://github.com/jupyter/governance/blob/master/governance.md>`_ and
`NumPy's governance document
<https://github.com/numpy/numpy/blob/master/doc/source/dev/governance/governance.rst>`_.

License
=======

To the extent possible under law, the authors have waived all
copyright and related or neighboring rights to the Dolphicom project
governance document, as per the `CC-0 public domain dedication / license
<https://creativecommons.org/publicdomain/zero/1.0/>`_.
