## Version 0.6

0.6.0 (2022-03-03)

- add `include_*` arguments to `dolphicom.pipeline.LocalSingle` and `dolphicom.core.AudioFile` in order to optionally disable mending steps


## Version 0.5

0.5.0 (2021-08-02)

- when io.youtube.PipelinePP is run, it saves the io.audiofile object in the pkl file, as well as spectrograms and corpus csv and tars them into the .pkl.tgz file (as opposed to just the pkl file)
- move io.youtube.PipelinePP to .pipeline.PipelinePP
- add io.dir_tgz.DirTgz for grouping results of dolphicom from multiple archives
- add loglog for frequency versus rank in helpers.s3_reporter
- helpers.s43: braille converter to scale by min/10khz instead of min/max (to be tested)


0.5.1 (2021-08-16)

- Add license BSD-3-Clause like scipy and numpy
- dolphicom.pipeline.Pipeline: option to skip generating gallery
- change 10khz limit for transliteration to 5khz
  - Update: tested on gallery 0.5.0 vs 0.5.2 and found that the new 5khz works quite well (links to galleries below in 0.5.2 section)
- split out code from dolphicom.helpers.s3_reporter to dolphicom.corpus.Corpus


0.5.2 (2021-08-24)

- [bugfix] helper.s22c: small yet major bugfix: s22c step of drop bursts removes whistles when there are no bursts => add artificial burst (not fully tested yet)
  - Update: tested dolphicom 0.5.0 vs 0.5.2 on moby-5th-eval2 and found significant improvement
  - Gallery 0.5.0: https://dolphicom.s3.us-west-2.amazonaws.com/dolphicom_results/t11-f1-v20210805a-moby_5th_eval2-dolphicom_pipeline_v050/5th_DCL_Expanded_eval_data/html/p001.html
  - Gallery 0.5.2: https://dolphicom.s3.us-west-2.amazonaws.com/dolphicom_results/t11.2-f1-v20210818a-moby_5th_eval2-dolphicom_pipeline_v052/html/p001.html
- [enh] pipeline: depend on complete_ok.txt or complete_error.txt to decide if done or not
- [feat] corpus: add read_csv and to_mobydiff_csv
- [enh] move .pipeline.PipelinePP to dolphicom.pipeline.local_single.LocalSingle, split out of it the youtube postprocessor part as .io.youtube.PipelinePP, move .io.audio_glob to .pipeline.local_glob
- [enh] audiofile: drop bursts now uses a thinner kernel of width 1 instead of 2 to capture cases of vertical bursts more completely in NOAA PIFSC dataset
- [feat] new step in preprocessing: drop periodic blobs (step s14)
- [feat] .pipeline.local_single to also catch assertion errors and save to complete_error.txt file


0.5.3 (2021-09-05)

- [enh] include in RemoteGs the ability to skip files that are already done
- [feat] add dolphicom.datasets.Dclde2020Mp3 in the spirit of sklearn.datasets
- [enh] rename remote_gs.RemoteGs.run to fit (like sklearn) and its argument processes to n_jobs (again like sklearn) and _per_process to fit_one
  - also rename pipeline.multiple_base.{run_pre,run_post} to {fitone_pre,fitone_post}
- [enh] pipeline: refactor multiple_base.MultipleBase to base_multiple.BaseMultiple + local_glob -> local_multiple
- [feat] pipeline: add generate_html_gallery call to base_multiple and for remote_gs call GsDirStructMirror.run first
- [enh] refactor dolphicom.pipeline.local_single.LocalSingle.run to fit (like sklearn)
- [enh] multiple refactorings in re-arranging classes in folders, eg new folder "core", move youtube to pipeline.remote_youtube, etc
- [enh] refactor save_pkl2 and read_pkl2 to save_pkl and read_pkl and drop the older versions which Im not using
- [enh] add .core.audiofile.AudioFile.fit function that wraps _s0_read etc
- [enh] io.gallery: add more description to html pages


0.5.4rc1 (2021-10-??)

- [enh] datasets.Dclde2020: change bucketname and file prefix after moving to the dolphicom bucket
- [enh] split dependencies into minimal and full, depending on requested extras in pip install step (specified in brackets after package name)
- [feat] add first CLI to convert DCLDE 2018 local disk data from wav to mp3 with channel=0 only



## Version 0.4

0.4.0 (2021-07-21)

- new functions save_pkl2, read_pkl2, to_dict, from_dict which would make the saved pkl files classname agnostic

0.4.1 (2021-07-21)

- refactor dolphin_communication.pipeline.Pipeline to dolphin_communication.io.audiofile.AudioFile
- refactor dolphin_communication.youtube_dl_wrap.YoutubeDlWrap to dc.io.youtube.Youtube
- refactor SpectrogramPlotter to Spectrogram
- refactor s22b_watershed to s22a_watershed and s22{,a}_whistle_connector to s22b_whistle_connector
- rename package altogether from dolphin_communcation to dolphicom

0.4.2 (2021-07-21)

- major fixup to load pkl files agnostically from packagename and classname, need to_dict and from_dict for helpers.s23b_segwrap

0.4.3 (2021-07-23)

- spectrogram_helpers/s21_drop_blobs: re-calibrate some parameters by reducing blur sigma and adding a thresholding by value step
- major: move the thresholding by value for 0s from drop blobs to the read_wav since its a fundamental operation
- io.audiofile.AudioFile: add t_sec_start and t_sec_end to avoid having to read long files altogether
- helpers.strengthen whistle: only vertical line is non-biological, so just exclude that instead of horizontals and surrounds etc
- add step for drop bursts after watershed
- helper.s21a_drop_blobs: change threshold from mean x 3 to 100 pixels by experimentation
- feat: spectrogram.export_corpus_csv, spectrogram.plot_checkpoints, io.audio_glob


## Version 0.3

0.3.0 (2021-07-09)

- s3 reporter: major bugfix of comment out dropping whistles occuring once
- s22b watershed: changed to add input from s12 with horizontal drop burst and s13 + use all spectrogram points for markers instead of just centroid
- s13 remove noise: compute the `f_focus` list instead of hardcoded
- new step: s22a drop blobs
- s23b connector 2: bugfix to match not only small close whistles but long ones as well

0.3.1

- bugfix: misc major bugfixes that were causing missed whistles

0.3.2

- bugfixes for youtube-dl wrapper

0.3.3 (2021-07-13)

- enh, spectrogram: plot corpus to show with and without highlights, so as to visually detect any imagined patterns
- feat, s3 reporter.generate gallery: new function for usage in prep for publication
- bugfix, s13 remove noise: important return of np.copy usage which was causing early return, compute n rand as 20% of time axis points
- enh, utils: major refactor to use new function `np_index_at_value` instead of `argmin(abs(...))` everywhere



## Version 0.2, 2021-07-07

- broken whistles get connected in spectrogram
- improved denoising for less false positives
- not dropping whistles shorter than 0.25s, depend on false positive removal instead
- ...
- Add several ".copy()" for pandas dataframes to avoid warnings about setting on copy or slice. It seems this is new for pandas 1.2.5. Not sure though.
- add step 2.2b, watershed filling for complete whistles
- channel number as input to pipeline
- use `scipy.sparse.csr_matrix` for reduced memory footprint
- added `youtube_dl_wrap` class that can download videos and process them as they are downloaded one at a time
- spectrogram has `plot_corpus` function that plots just the whistles for example
- added entropy calculation to reporter
- whistle timeseries compression and transliteration to braille for entropy
- strengthening whistles has more slopes to capture strong up whistles


## Version 0.1, 2021-05-14

First version

Features:

- read audio file
- build spectrogram
- de-noise spectrogram
- highlight whistles in spectrogram
- split audio file into segments around whistles
- concatenate segments into a single audio file
- generate video of audio spectrogram
