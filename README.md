# dolphicom

Dolphin communication

Automated dolphin whistle extraction, transliteration to braille, corpus building, and entropy calculation.


## Results
### Gallery 1: mobysound.org, DCLDE 5th, melon-headed dolphin + bottlenose dolphin + youtube

TODO: should re-run with dolphicom 0.5.2 since it was an important fix for completeness of whistles

Results of dolphin whistle detection with dolphicom 0.4.3 on the following datasets is available at:

https://s3.us-west-2.amazonaws.com/dolphicom/dolphicom_results/t14.3-f2-v20210726a-html_gallery/index.html

Data sets:

- mobysound.org, 5th conference, bottlenose dolphin
- mobysound.org, 5th conference, melon-headed dolphin
- youtube query for "bottlenose dolphin whistles"

Each page lists 2 kinds of images:

- unhighlighted: just has a box around the detected whistle
- highlighted: marks in yellow on top of the detected whistle region

Hovering over the image will give its full path, including the specific file from which it was obtained and seconds interval.


### Gallery 2a: mobysound.org, DCLDE 5th, eval2 dataset, dolphicom 0.5.2

Gallery for just dolphicom 0.5.2 results on moby-5th-eval2, displaying side-by-side the unhighlighted and highlighted spectrogram

https://dolphicom.s3.us-west-2.amazonaws.com/dolphicom_results/t11.2-f1-v20210818a-moby_5th_eval2-dolphicom_pipeline_v052/html/index.html


### Gallery 2b: mobysound.org, DCLDE 5th, eval2 dataset, comparing dolphicom with manual and pamguard annotations

Results of dolphin whistle detection with dolphicom 0.5.2 on the following datasets is available at:

https://dolphicom.s3.us-west-2.amazonaws.com/dolphicom_results/t11.3-f1-v20210819b-moby_5th_eval2-plotting_mobydiff_annotations-dolphicom_v052/html/index.html

Data set:

- mobysound.org, 5th conference, evaluation-2 dataset ([link](http://www.mobysound.org/workshops_p2.html))

Box color legend:

- Black: dolphicom 0.5.2
- Red: manual annotations as documented at mobysound.org link above
- Green: pamguard 2.01.05

Data behind boxes also available at https://gitlab.com/dolphicom/benchmarks


### Gallery 3: NOAA PIFSC, DCLDE Oahu dataset

Results of dolphicom 0.5.2 on [DCLDE Oahu](http://www.soest.hawaii.edu/ore/dclde/dataset/) (aka DCLDE 2020) dataset

https://storage.googleapis.com/dolphicom/analyses/t17.1-f1-v20210824a-noaa_pifsc-pipeline/html/index.html

Each page lists 2 kinds of images:

- unhighlighted: just has a box around the detected whistle
- highlighted: marks in yellow on top of the detected whistle region

Clicking the image will open it in larger resolution.



## Install

Dependencies

```
sudo apt-get install ffmpeg unzip
```

Install python and pip from ubuntu repos (or with miniconda below)

```
sudo apt-get install python3 python3-pip
```

Or using miniconda

```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
bash # or exit and re-ssh
conda install python pip
```

If using from jupyterlab

```
pip install jupyterlab ipywidgets
jupyter nbextension enable --py widgetsnbextension
```

s3-fu: If will use s3, eg `dolphicom.io.s3_tar_members`:

```
pip install boto3
```

google-storage-fu:

```
!pip install --quiet google-cloud-storage


fn_gcp_key = "gcp-gs-key.json"

# For py usage
import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]=fn_gcp_key

# For CLI usage
# https://serverfault.com/a/901950/394721
!gcloud auth activate-service-account --key-file="$fn_gcp_key"
```

env-fu

```
# with conda (check conda installation step above) or with pew below
conda create -n dolphicom
conda activate --stack dolphicom

# or with pew instead of conda
#pip install pew
#pew new dolphicom
```

Pre-install hdbscan from conda-forge (if installed conda) to avoid building it locally

```
conda config --add channels conda-forge
conda config --set channel_priority strict
conda install hdbscan==0.8.27
```

pip-fu

```
# for whistle detection, install with extras being "full"
pip install --quiet --upgrade "git+https://gitlab.com/dolphicom/dolphicom.git@master#egg=dolphicom[full]"

# for just iterating over files to convert wav to mp3
pip install --quiet --upgrade "git+https://gitlab.com/dolphicom/dolphicom.git@master#egg=dolphicom[minimal]"
```

If on python 3.8 or 3.9 (stable) instead of python 3.7 (colab)

```
# Run this *AFTER* installing dolphicom
pip install --upgrade numpy imageio-ffmpeg moviepy

# As of 2021-07-13
#Successfully uninstalled moviepy-0.2.3.5
#Successfully installed moviepy-1.0.3 proglog-0.1.9
```



## Usage

## Example 1: run the default pipeline on an audio file

The default pipeline consists roughly of:

1. calculate spectrogram
1. remove noise from spectrogram
2. search for whistles in de-noised spectrogram
3. trace the whistles
4. transliterate traces to braille
5. compress the braille
6. build a dictionary of braille representations of whistles
7. count the dictionary entries
8. calculate the dictionary's entropy

Example:

```
dir_out = "t11-f1-v20210802a-moby_5th_eval-dolphicom_pipeline_v050"
!mkdir -p $dir_out

from dolphicom.pipeline.local_single import LocalSingle
p = LocalSingle(dir_out)
fn_wav = "eval_data/Delphinus capensis/CC0707-TA33-070713-202000.wav"
p.fit(fn_wav)
```

This will generate a tgz file for each input audio file of the same basename. It contains:

- a pkl file with the dolphicom AudioFile object
- a directory of spectrograms with 5-second windows of the input file
- a directory of thumbnails of the spectrograms
- a csv of the whistle corpus

To extract the archive: `tar -xzf CC0707-TA33-070713-202000.tgz`


### Example 2: analyze local audio file

This is sort of a manual execution of the pipeline

```
from dolphicom.core.audiofile import AudioFile
fn_in = "yt_vids/Beluga_whale_learns_to_talk_to_a_pod_of_bottlenose_dolphins_using_whistles_and_clicks_af-0ttQr1fvstE.m4a"

# Read the full file (216 seconds long)
p = AudioFile(fn_in)

# Alternatively, read channel 0, drop frequencies under 2 khz, and focus on timerange 197s-203s
# import numpy as np
# def mypreprocess(Sxx, f, t):
#   Sxx[f<2_000, :]=0
#   return Sxx
# p = AudioFile(fn_in, channel=0, spectrogram__generic_func=mypreprocess, t_sec_start=197, t_sec_end=203)

whistle_corpus = p.fit(remove_noise__fast=False)
```

where `whistle_corpus` is a pandas dataframe of rows representing each detected whistle.

It has the whistle ID as `whistle_label`, whistle features in the columns such as `t_sec_min` for the starting time in seconds of the whistle within the audio file.

The traced 1D vector of the whistle top frequency is in field `S_1d`. The transliteration in braille is in field `S_str_1`.

The whistle corpus is also accessible as:

```
p.sp_filt.s23_whistle_corpus
```

To print a summary report of the whistles, including top corpus entries and entropy:

```
p.print_report()
```

To generate some plots showing the spectrograms with overlaid whistle traces in 2D and 1D:

```
p.sp_filt.plot_corpus(sec_min=0, sec_max=50, plot_type='any detection', plot_max=10)
```

To save and load to file

```
# save
p.to_pickle("file.pkl")

# load
p = AudioFile.from_pickle("file.pkl")
```

### Example 3: analyze youtube video

```
# Indigenous Whistle Language In Mexico
url_vid = "https://www.youtube.com/watch?v=6eliANcZdkw"
#!youtube-dl --list-formats "$url_vid"
!youtube-dl --extract-audio --audio-format mp3 "$url_vid"

fn_aud="Indigenous Whistle Language In Mexico-6eliANcZdkw.mp3"
!du -sh "{fn_aud}"

# folder where dolphicom results are saved as pkl files
dir_out = "t01.1-f1-v20220215a-kickapoo-dolphicom_pipeline_v054"
!mkdir -p $dir_out

from dolphicom.pipeline.local_single import LocalSingle
p = LocalSingle(dir_out)
p.fit(fn_aud)
```


**Deprecated instructions here for archive:**

Download, analyze, and save to pkl file.

For query search results, this pipeline will analyze each audio file upon download.
An alternative would be to use youtube-dl to download all files to a local directory, and then run dolphicom on the directory.

In the below, `query` is a list. Each entry can be the reference to a single youtube video, its URL, a URL to a playlist, or whatever other input is supported by youtube-dl.

```
# Beluga_whale_learns_to_talk_to_a_pod_of_bottlenose_dolphins_using_whistles_and_clicks_af-0ttQr1fvstE.m4a
query = ["0ttQr1fvstE"]

# Alternatively, search for the first 250 results for "dolphin bottlenose whistles"
# query = ["ytsearch250:dolphin bottlenose whistles"]

# folder where dolphicom results are saved as pkl files
!mkdir -p "yt_dolphicom"
!mkdir -p "yt_audio"

from dolphicom.pipeline.remote_youtube import Youtube
ydlw = Youtube(verbose=True)
ydlw.fit(query, dir_audio="yt_audio", dir_out="yt_dolphicom")
```

The results are saved to a pkl file, whose filename is shown on stdout at the end of the process.

In that case, analyze the downloaded audio file following example 1.

**End of deprecated instructions**


### Example 4: utility tools to work with tar or zip file in aws s3 without downloading it

For tar files, these support listing contents and extraction.

For zip files, only listing contents is supported.

```
# Install boto3 dependency
pip install boto3
```


List tar contents without downloading it

```
# python -m dolphicom.io.s3_archive_cli -l <s3 bucket> <tar path>
python -m dolphicom.io.s3_archive_cli -l dolphicom "ytq1_250vids__audiofiles_20210714.tar"
```

Extract a single file from the archive without downloading the full tar file

```
# python -m dolphicom.io.s3_archive_cli -e <s3 bucket> <tar path> <local path> <file>
python -m dolphicom.io.s3_archive_cli -e \
  dolphicom "ytq1_250vids__audiofiles_20210714.tar" . \
  yt.txt
```


Extract a few files (FIXME need to fix my argparse usage for this to work, currently just works with 1 file)

```
# python -m dolphicom.io.s3_archive_cli -e <s3 bucket> <tar path> <local path> <file 1> <file 2> ...
python -m dolphicom.io.s3_archive_cli -e \
  dolphicom "ytq1_250vids__audiofiles_20210714.tar" . \
  yt.txt \
  yt_vids/10_Fun_Facts_About_Dolphins-rsYEC-iMUB8.m4a
```

List files in remote zip file on s3

```
python -m dolphicom.io.s3_archive_cli -l dolphicom "mobysound.org-mirror-v20210704/workshops/5th_DCL_Evaluation.zip"
```

For usage from python, check examples in `dolphicom/io/s3_tarfile.py` or `dolphicom/io/s3_zipfile.py`



## Example 5: run the default pipeline on a local directory of audio files

Utility classes are available for a directory of audio files, remote audio files on google storage bucket:

```
dolphicom.pipeline.local_multiple
dolphicom.pipeline.remote_gs
```

(TODO add more documentation)


## Example 6: analyze dolphicom results of dataset of many files

```
dolphicom.io.dir_tgz.DirTgz
```


## Example 7: run detector on DCLDE 2018/2020 datasets (NOAA PIFSC)

```
from dolphicom.datasets import Dclde2020Mp3Ch0 # or Dclde2018Mp3Ch0
detector = Dclde2020Mp3Ch0().get_detector(dst_bucket_name="dolphicom", dirout_dirname_r="analyses", dirout_basename="dolphicom-example_7-dclde2020", dir_ws="workspace")
detector.fit(n_jobs=1)
```


## Example 8: convert DCLDE 2018 2.4TB wav files on local disk to mp3 channel 0 only

DCLDE 2018 homepage: http://sabiod.lis-lab.fr/DCLDE/

Received data by mail on disk on 2021-10-20 from NOAA

Command

```
dolphicom dclde-2018-wav-to-mp3-channel-0 \
  "/media/shadi/921ABEEA1ABECB0D/DCLDE 2018 - NOAA/" \
  "/media/shadi/921ABEEA1ABECB0D/DCLDE 2018 - NOAA - mirror_mp3_ch0/"
```

Reduces a 500 MB wav file to ~ 10 MB mp3 file (mono, only first channel)

Total dataset in wav format is 2.4 TB. In mp3, first channel only, it is ~ 50 GB.

Upload to GCP storage:

```
gsutil config # click link to authenticate, copy project ID
gsutil rsync -r "DCLDE 2018 - NOAA - mirror_mp3_ch0" "gs://dolphicom/datasets/DCLDE 2018 - NOAA - mirror_mp3_ch0"
```


## License

Check [LICENSE.txt](LICENSE.txt)


## Dev notes
### Install

```
# Method 1: With ssh keys set up in gitlab
pip install --quiet --upgrade git+ssh://git@gitlab.com/dolphicom/dolphicom.git@master

# Method 2: with plain username and password
pip install --quiet --upgrade git+https://username:password@gitlab.com/dolphicom/dolphicom.git@master

# Method 3: with git clone then install from local dir
git clone git+ssh://git@gitlab.com/dolphicom/dolphicom.git
cd dolphicom
python setup.py egg_info

# https://stackoverflow.com/questions/57458571/python-setup-py-install-specify-extras-require/65795219#65795219
# or pip3 install -e .[minimal]

# other
git clone .../dolphicom.git
cd dolphicom
pip3 install -r requirements.txt
```

### Usage

When working from the git checkout (in development), replace `dolphicom` with `python3 -m dolphicom.cli`, eg

```
python3 -m dolphicom.cli ...
```

### Unit Tests

Check `.gitlab-ci.yml`

```
python3 test_*.py
```

