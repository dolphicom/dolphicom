import click


@click.group()
def cli():
  pass


from .pipeline.local_multiple import LocalGlob
import os
from pydub import AudioSegment
import datetime as dt
class Converter(LocalGlob):
  def __init__(self, dir_in, dir_out):
    self.dir_in=dir_in
    self.dir_out=dir_out


  def fit_one(self, pool_in_i):
    i, n, fn_wav = pool_in_i
    fn_mp3 = fn_wav.replace(".wav", ".mp3").replace(self.dir_in, self.dir_out)
    #print(f"\tInput: {fn_wav}")
    #print(f"\tTarget: {fn_mp3}")
    if os.path.exists(fn_mp3):
      fn_s = os.path.getsize(fn_mp3)
      if fn_s != 0: return
      os.remove(fn_mp3)

    p_pre = f"{dt.datetime.now()}: File {i+1}/{n}:\t<in>/{fn_wav.replace(self.dir_in, '')}"
    click.echo(p_pre)

    # ETA for n_jobs = 1: 0s
    #print(f"\t{dt.datetime.now()}: mkdir -p ...")
    os.makedirs(os.path.dirname(fn_mp3), exist_ok=True)

    # ETA for n_jobs = 1: 25s
    #print(f"\t{dt.datetime.now()}: read ...")
    a = AudioSegment.from_file(fn_wav)

    # ETA for n_jobs = 1: 0s
    #print(f"\t{dt.datetime.now()}: split ...")
    a = a.split_to_mono()[0]

    # ETA for n_jobs = 1: 1m 20s
    #print(f"\t{dt.datetime.now()}: export ...")
    a.export(fn_mp3, format="mp3")

    #print(f"\t{dt.datetime.now()}: ... done")


@cli.command()
@click.argument('dir_in', type=click.Path(exists=True, file_okay=False, dir_okay=True))
@click.argument('dir_out', type=click.Path(exists=True, file_okay=False, dir_okay=True))
def dclde_2018_wav_to_mp3_channel_0(dir_in, dir_out):
    click.echo('Converting wav files from DCLDE 2018 local folder to mp3 with channel 0 only...')

    #import glob

    #n_exist = len(glob.glob(f"{dir_out}/**/*", recursive=True))
    #if n_exist > 0:
    #  click.warning(f"`dir_out` is not empty. Aborting")
    #  click.exit(1)

    #fn_wav_l = glob.glob(f"{dir_in}/**/*.wav", recursive=True)
    #click.echo(f"Found {len(fn_wav_l)} wav files")

    c = Converter(dir_in, dir_out)
    c.set_glob_str(f"{dir_in}/**/*.wav")
    # Best just use 1 cpu since trying out n_jobs=3 on 4-core machine caused ETA of 3 files = 4m20s whereas 1 file is 2 minutes
    c.fit(n_jobs=1)


if __name__ == '__main__':
    cli()
