from .spectrogram import Spectrogram
from ..ffmpeg import FfmpegMan
import os
from ..utils import my_rm_glob, print_filesize, warn_dolphicom_version, to_dict_dolphicom, from_dict_dolphicom
import pickle
import numpy as np
import scipy
from ..spectrogram_helpers.s3_reporter import Reporter




class AudioFile:
  def __init__(self, fn_in, channel=0, spectrogram__generic_func=None, t_sec_start=None, t_sec_end=None, include_s22b_connect_broken_whistles=True, include_s23_blur=True, include_s23_hdbscan=True, include_s23_mender=True):
    """
    fn_in: path to wav file
    channel: channel number (0-based) to read from wav file, if more than 1 are available
    spectrogram__generic_func: generic function that will be called on the spectrogam as `Sxx = generic_func(Sxx, f, t)` after the s11_read_wav step
    t_sec_start - Seconds location at which to start. Use None to skip
    t_sec_end - Seconds location at which to end. Use None to skip
    include_s22b_connect_broken_whistles: pass False to skip s22b step (second whistle-mending step)
    include_s23_blur - pass False to skip meijering blur (that also contributes to mending)
    include_s23_hdbscan - pass False to skip hdbscan (which contributed to mending)
    include_s23_mender - pass False to skip the mender in s23 (hdbscan segmenter that builds the corpus)
    """
    self.fn_in = fn_in
    self.channel = channel
    self.spectrogram__generic_func = spectrogram__generic_func
    self.t_sec_start = t_sec_start
    self.t_sec_end = t_sec_end
    self.include_s22b_connect_broken_whistles = include_s22b_connect_broken_whistles
    self.include_s23_blur = include_s23_blur
    self.include_s23_hdbscan = include_s23_hdbscan
    self.include_s23_mender = include_s23_mender
    self.sp_filt = Spectrogram()


  def fit(self, remove_noise__fast):
    self._s0_read()
    self._s1_preprocess(remove_noise__fast)
    wc = self._s2_detect_whistles()
    return wc


  def print_report(self):
    if not hasattr(self, "wm"):
      raise Exception("Did you forget to call .fit(False) ?")

    self.wm.print_report()


  def _s0_read(self):
    print_filesize(self.fn_in)
    self.sp_filt.s11_read_wav(self.fn_in, self.channel, self.t_sec_start, self.t_sec_end)


  def _s1_preprocess(self, remove_noise__fast):
    """
    remove_noise__fast: True|False. Passed to spectrogram.s13_remove_noise. True to use thresholding method, False to use flood-blur method
    """
    if self.spectrogram__generic_func is not None:
      self.sp_filt.Sxx = self.spectrogram__generic_func(self.sp_filt.Sxx, self.sp_filt.f, self.sp_filt.t)
      self.sp_filt.Sxx_checkpoints["s11b_spectrogram__generic_func"] = np.copy(self.sp_filt.Sxx)

    self.sp_filt.s12_drop_bursts()
    self.sp_filt.s13_remove_noise(fast=remove_noise__fast)
    self.sp_filt.s14_drop_periodic_blobs()


  def _s2_detect_whistles(self):
    self.sp_filt.s21a_drop_blobs(verbose=False)
    self.sp_filt.s21b_strengthen_whistles()
    #self.sp_filt.s21a_drop_blobs(stage=2, verbose=False)
    self.sp_filt.s22a_watershed()
    self.sp_filt.s22c_drop_bursts()
    if self.include_s22b_connect_broken_whistles: self.sp_filt.s22b_connect_broken_whistles()

    # proceed with hdbscan and braille and report
    self.sp_filt.s23_segment_hdbscan(self.include_s23_blur, self.include_s23_hdbscan, self.include_s23_mender)
    self.sp_filt.s4_to_braille(verbose=False)
    self.wm = self.sp_filt.s3_report()

    return self.sp_filt.s23_whistle_corpus


  def create_video(self, full, video_type, with_marker):
    """
    full: False for just video of whistle segments in original audio. True for full original audio.
    video_type: key from self.sp_filt.Sxx_checkpoints
    with_marker: True - Break each 5-second spectrogram into 5 png's displaying a marker at seconds 0, 1, 2, 3, 4
                 False - Do not display marker, which creates 5x less png files, and hence is faster

    """

    #PNGDIR="marley2017a-spectrogram-whistle_subset-v20210511a"
    #PNGDIR="wdp_yt_nyt-spectrogram-whistle_subset-v20210511a"
    fn_base = os.path.basename(self.fn_in).replace(".wav", "")
    PNGDIR=f"ws-pipeline_yt_whistles-v20210511a/{fn_base}"
    self.PNGDIR=PNGDIR

    #!mkdir -p "$PNGDIR"
    os.makedirs(PNGDIR, exist_ok=True)

    #!rm -rf "$PNGDIR"/*
    my_rm_glob(f"{PNGDIR}/*")

    if not full:
      if len(self.wm.t_whistle)==0:
        print(f"No detected whistles in {PNGDIR}. Skipping video generation.")
        self.FNMP4 = None
        return None

    print(f"wm.generate_png (ETA 5m on marley 2017 7m video, full={full}, with_marker={with_marker})")
    self.wm.generate_png(video_type, PNGDIR, full, with_marker)

    fm = FfmpegMan(self.wm)
    print("fm.generate_wav")
    #!rm -rf "$PNGDIR"/*wav
    my_rm_glob(f"{PNGDIR}/*wav")
    fm.generate_wav(self.fn_in)
    print("fm.merge_png_wav_cmd")
    #!rm -rf "$PNGDIR"/*mp4
    my_rm_glob(f"{PNGDIR}/*mp4")
    fm.merge_png_wav_cmd()

    print("fm.concat_mp4_cmd")
    FNMP4=f"{PNGDIR}.mp4"
    #!rm -rf "$FNMP4"
    my_rm_glob(FNMP4)
    #!rm -rf f"{PNGDIR}/concat.txt"
    my_rm_glob(f"{PNGDIR}/concat.txt")
    fm.concat_mp4_cmd(PNGDIR, FNMP4)

    print(f"Saved output into {FNMP4}")
    if FNMP4 is not None: print_filesize(FNMP4)
    self.FNMP4 = FNMP4
    return FNMP4


  #def pipeline_combo_detect_video(fn_in):
  #  sp_filt, wm = pipeline_detect_whistles(fn_in)
  #  FNMP4 = pipeline_create_video(fn_in, wm)
  #  return sp_filt, wm, FNMP4


  def to_pickle(self, fn_pkl, all_checkpoints=False):
    """
    Saves the data contained within the class and not the class itself.
    Useful to avoid being tied to a class name, in case a refactoring is needed.
    Otherwise, pkl files with the class name before refactoring would no longer be loadable.
    This function looks like the clone function in that it accesses members individually.
    """
    d1 = self.to_dict(all_checkpoints)
    d2 = to_dict_dolphicom("core.audiofile.AudioFile", d1)
    with open(fn_pkl, "wb") as f:
      pickle.dump(d2, f)


  def to_dict(self, all_checkpoints):
    # main dict that will contain all the data
    data = {}

    # Nested dict for audio file info
    d3 = {}
    d3["fn_in"] = self.fn_in
    d3["channel"] = self.channel
    d3["t_sec_start"] = self.t_sec_start
    d3["t_sec_end"] = self.t_sec_end
    d3["include_s22b_connect_broken_whistles"] = self.include_s22b_connect_broken_whistles

    # self.spectrogram__generic_func
    data["audio_file"] = d3

    # nested dict for spectrogram
    data["spectrogram"] = self.sp_filt.to_dict(all_checkpoints)

    if hasattr(self, "wm"):
      data["reporter"] = self.wm.to_dict()

    return data


  @classmethod
  def from_pickle(cls, fn_pkl, verbose):
    """
    Similar to from_pickle, but loading the pkl file saved by to_pickle
    verbose: False for no print messages
    """
    with open(fn_pkl, "rb") as f:
      d2 = pickle.load(f)

    d1 = from_dict_dolphicom(d2, "core.audiofile.AudioFile", verbose)

    self = cls.from_dict(d1, verbose)
    return self


  @classmethod
  def from_dict(cls, data, verbose):
    d3 = data["audio_file"]
    # FIXME am I missing in the below: t_sec_start and t_sec_end?
    self = cls(fn_in = d3["fn_in"], channel = d3["channel"], spectrogram__generic_func = None, include_s22b_connect_broken_whistles=d3["include_s22b_connect_broken_whistles"])

    self.sp_filt = Spectrogram.from_dict(data["spectrogram"], verbose)

    if "reporter" in data:
      self.wm = Reporter.from_dict(data["reporter"], self.sp_filt)

    return self


  # p_pkl -> self
  def clone(p_pkl):
    # some patches for reading older versions
    if not hasattr(p_pkl, "channel"): p_pkl.channel=0
    if not hasattr(p_pkl.sp_filt, "channel"): p_pkl.sp_filt.channel=0
    if not hasattr(p_pkl, "include_s22b_connect_broken_whistles"): p_pkl.include_s22b_connect_broken_whistles = True

    if not hasattr(p_pkl.sp_filt, "Sxx"):
      print("No Sxx field in spectrogram. Perhaps no audio in file or AbortPipeline returned during execution. Will not clone and return as is instead")
      return p_pkl

    if type(p_pkl.sp_filt.Sxx) == np.ndarray: p_pkl.sp_filt.Sxx = scipy.sparse.csr_matrix(p_pkl.sp_filt.Sxx)

    # start clone
    # FIXME am I missing in the below: t_sec_start and t_sec_end?
    p_self = p_pkl.__class__(p_pkl.fn_in, p_pkl.channel, p_pkl.spectrogram__generic_func, include_s22b_connect_broken_whistles=p_pkl.include_s22b_connect_broken_whistles) # AudioFile

    #p_self.sp_filt = p_pkl.sp_filt.__class__() # Spectrogram
    p_self.sp_filt.fn_in = p_pkl.sp_filt.fn_in
    p_self.sp_filt.channel = p_pkl.sp_filt.channel
    p_self.sp_filt.f = p_pkl.sp_filt.f
    p_self.sp_filt.t = p_pkl.sp_filt.t

    # Update: Sxx is stored as csr_matrix in the pkl file for space saving, but load it as numpy array to avoid the weird non-interoperability between numpy as csr_matrix
    # Update: keep as csr to avoid running out of memory on colab as of 2021-07-07
    #p_self.sp_filt.Sxx = p_pkl.sp_filt.Sxx.toarray()
    p_self.sp_filt.Sxx = p_pkl.sp_filt.Sxx

    p_self.sp_filt.sec_all = p_pkl.sp_filt.sec_all

    #for k, v in p_pkl.sp_filt.Sxx_checkpoints.items():
    #  p_self.sp_filt.Sxx_checkpoints[k] = v

    # This is dict of csr_matrix, and it's ok not to convert back to numpy array here
    p_self.sp_filt.Sxx_checkpoints = p_pkl.sp_filt.Sxx_checkpoints

    # since the checkpoint s11_original does not get compressed well after pickling
    # re-read it from raw. This way, the saved pickle file can still get compressed to 20 MB for the 1h audio yt-q1 file whose pickle is 2 GB
    if os.path.exists(p_self.sp_filt.fn_in):
      _, _, p_self.sp_filt.Sxx_checkpoints["s11_original"], _ = p_self.sp_filt._s11_read_wav_no_save_members(p_self.sp_filt.fn_in, p_self.channel, p_self.t_sec_start, p_self.t_sec_end)
    else:
      print(f"[WARNING] Input wav file doesnt exist. Skipping reading it into Sxx_checkpoints['s11_original']: {p_self.sp_filt.fn_in}")

    # FIXME what about Sxx_checkpoints["s12b_drop_bursts"]

    if hasattr(p_pkl.sp_filt, "s22_connections"):
      p_self.sp_filt.s22_connections = p_pkl.sp_filt.s22_connections.copy()
    else:
      print("[WARNING] AudioFile.s22_connections is missing")

    if hasattr(p_pkl.sp_filt, "s23_connections"):
      p_self.sp_filt.s23_connections = p_pkl.sp_filt.s23_connections.copy()
    else:
      print("[WARNING] AudioFile.s23_connections is missing")

    if hasattr(p_pkl.sp_filt, "s23_whistle_corpus"):
      p_self.sp_filt.s23_whistle_corpus = p_pkl.sp_filt.s23_whistle_corpus.copy()
    else:
      print("[WARNING] AudioFile.s23_whistle_corpus is missing")

    if hasattr(p_pkl.sp_filt, "s23_wsw"):
      p_self.sp_filt.s23_wsw = p_pkl.sp_filt.s23_wsw
    else:
      print("[WARNING] AudioFile.s23_wsw is missing")

    if hasattr(p_pkl, "wm"):
      p_self.wm = p_pkl.wm.__class__(p_self.sp_filt) # Reporter
      p_self.wm.Sxx_mask    = p_pkl.wm.Sxx_mask
      p_self.wm.idx_whistle = p_pkl.wm.idx_whistle
      p_self.wm.t_whistle   = p_pkl.wm.t_whistle
      p_self.wm.idx_keep    = p_pkl.wm.idx_keep
      p_self.wm.plt_params    = p_pkl.wm.plt_params
    else:
      print("[WARNING] AudioFile.Reporter is missing")

    return p_self
