from scipy.stats import entropy
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd



class HistPlotter:
  def __init__(self, all_df, dataset_name, annot_l):
    self.all_df = all_df

    self.dataset_name = dataset_name

    annot_missing = set(annot_l).difference(set(all_df["annotator"].tolist()))
    if len(annot_missing) > 0:
      annot_ms = ", ".join(sorted(list(annot_missing)))
      raise ValueError(f"{len(annot_missing)} annotator(s) are not present in the corpus dataframe: {annot_ms}")

    self.annot_l = annot_l

  def hist_t_len(self):
    hist_bins = np.arange(0,3,.1) - .05
    for k in self.annot_l:
      self.all_df.set_index("annotator").loc[k]["t_len_sec"].hist(label=k, density=True, bins=hist_bins, alpha=.5)

    plt.xlabel("whistle length in seconds")
    plt.ylabel("whistle count as density")
    plt.title(f"Compare whistle lengths of annotations \n Dataset: {self.dataset_name}")
    plt.legend()
    plt.show()


  def hist_f_len(self):
    THRESH_F_MAX = self.all_df.f_hz_max.max()//1_000*1_000 + 1_000
    hist_bins = np.arange(0, THRESH_F_MAX, 1_000) - 500
    for k in self.annot_l:
      self.all_df.set_index("annotator").loc[k]["f_len_hz"].hist(label=k, density=True, bins=hist_bins, alpha=.5)

    plt.xlabel("whistle length in khz")
    plt.ylabel("whistle count as density")
    plt.title(f"Compare whistle lengths of annotations \n Dataset: {self.dataset_name}")
    #plt.axvline(x=   0)
    #plt.axvline(x=1000)
    #plt.axvline(x=2000)
    plt.legend()
    plt.show()


  def hist_f_xxx(self, fx):
    THRESH_F_MAX = self.all_df[fx].max()//1_000*1_000 + 1_000

    hist_bins = np.arange(0, THRESH_F_MAX, 1000) - 500
    for k in self.annot_l:
      self.all_df.set_index("annotator").loc[k][fx].hist(label=k, density=True, bins=hist_bins, alpha=.5)

    plt.xlabel(f"whistle {fx} in khz")
    plt.ylabel("whistle count as density")
    plt.title(f"Compare whistles of annotations \n Dataset: {self.dataset_name}")

    #plt.axvline(x=   0)
    #plt.axvline(x=1000)
    #plt.axvline(x=2000)
    plt.legend()
    plt.show()

  
  #@classmethod
  #def hist_all(cls, all_df):
  #  hp = cls(all_df)
  #  hp.hist_t_len()
  #  hp.hist_f_len()
  #  hp.hist_f_xxx("f_hz_min")
  #  hp.hist_f_xxx("f_hz_max")



class Corpus:
  columns_required = {
        'label_whistle', 't_idx_min', 't_idx_max', 'f_idx_min', 'f_idx_max',
        't_sec_min', 't_sec_max', 'f_hz_min', 'f_hz_max', 't_len_sec',
        'f_len_hz', 'mask_area_pct', 'mask_freq_pct', 'mask_time_pct',
        't_len_idx', 'f_len_idx', 'area_idx', 'S_str_1', 'file',
        'annotator',
      }

  def __init__(self, s23_whistle_corpus):
    """
    s23_whistle_corpus: pandas dataframe with required columns. Check Corpus.columns_required for full list.
      "annotator" in the list is for the source of whistle detector, eg dolphicom, manual, pamguard, etc
    """
    columns_missing = self.columns_required.difference(set(s23_whistle_corpus.columns.tolist()))
    if len(columns_missing)>0:
      cm_str = ", ".join(sorted(list(columns_missing)))
      raise ValueError(f"Missing columns in corpus input: {cm_str}")

    self.s23_whistle_corpus = s23_whistle_corpus

  
  def print_report(self, dataset_name):
    # (not sure yet why different than previous line)
    print("#"*100)
    print(f"# detected whistles (filtered, exact, mended(2)): {self.s23_whistle_corpus.shape[0]}")
    print("#"*100)
    print("")

    self.hist(dataset_name, annot_l=None)

    print("")
    print("Whistle type counts")

    #fx_S_str = "S_str_2"
    fx_S_str = "S_str_1"

    S_count = self.s23_whistle_corpus[fx_S_str].value_counts()
    #S_count = S_count[S_count>1]

    print("#"*100)
    print(f"# unique whistle types: {S_count.shape[0]}")
    print("#"*100)
    print("")

    print("Top 20 whistles:")
    print(S_count.head(20))

    print("Bottom 20 whistles:")
    print(S_count.tail(20))

    print(f"Number of whistles: {S_count.sum()}")

    # average length without filtering out unclustered
    #S_len = p.s23_whistle_corpus.S_str.apply(len).sort_values()

    def print_avglen_ent(sc2, title):
      print("-------------")
      print(f"Stats for: {title}")

      print(f"Number of unique whistles: {sc2.shape[0]}")

      S_len = sc2.reset_index()
      S_len = S_len.rename(columns={"index": fx_S_str, fx_S_str: "n"})
      S_len["l"] = S_len[fx_S_str].apply(len)
      S_len["l*n"] = (S_len.l * S_len.n)
      a = S_len["l*n"].sum()/(S_len.n.sum())
      print(f'Average length of all whistles: {a.round(1)}')
      e = entropy(sc2.values, base=2)
      print(f"Entropy of all whistles: {e.round(1)}")

      plt.plot(sc2.values, marker="|")
      plt.ylim(0)
      plt.ylabel("Frequency")
      plt.xlabel("Rank of whistle type")
      plt.title(f"Histogram ({title})")
      plt.show()

      if sc2.shape[0]>10:
        plt.plot(sc2.values[10:], marker="|")
        plt.ylim(0)
        plt.ylabel("Frequency")
        plt.xlabel("Rank of whistle type")
        plt.title(f"Histogram from 10th whistle type onwards ({title})")
        plt.show()

      plt.loglog(sc2.values, marker="|")
      plt.ylim(0)
      plt.ylabel("log(Frequency)")
      plt.xlabel("log(Rank) of whistle type")
      plt.title(f"Histogram in loglog ({title})")
      plt.show()

      plt.plot(np.divide(S_len["l*n"].cumsum(), S_len.n.cumsum()).values, marker="|")
      plt.ylim(1)
      plt.ylabel("Number of characters per transliterated whistle")
      plt.xlabel("Rank of whistle type")
      plt.title(f"Cumulative average ({title})")
      plt.show()

      # plot entropy timeseries as more of low frequency corpus is included
      ent_l = []
      for i in range(sc2.shape[0]): ent_l.append(entropy(sc2.iloc[:(i+1)].values, base=2))
      plt.plot(ent_l, marker="|")
      plt.ylim(0)
      plt.ylabel("Entropy (base 2)")
      plt.xlabel("Rank of whistle type")
      plt.title(f"Cumulative entropy ({title})")
      plt.show()


    # average length withOUT filtering out unclustered
    print_avglen_ent(S_count, "all")

    if S_count.min()==1:
      # average length after excluding unclustered
      print_avglen_ent(S_count[S_count > 1], "excluding count=1")


  @classmethod
  def read_csv_dolphicom(cls, fn_csv):
    """
    Read a csv saved by dolphicom as AudioFile.sp_filt.s23_whistle_corpus.to_csv(...)
    """
    df = pd.read_csv(fn_csv)
    if "file" not in df.columns: df["file"] = fn_csv
    return cls(df)


  @classmethod
  def from_mobydiff_df(cls, all_df):
    """
    Read a dataframe from mobydiff csv files.
    These files have less fields, hence need more "dummy" values
    """

    # should have these fields, along with t_sec_min, .... the Corpus constructor will raise an exception listing the missing fields.
    assert "csv_p1" in all_df.columns
    assert "csv_p2" in all_df.columns
    assert "tonal_id" in all_df.columns

    # should not have these fields since they will be created by "rename" below
    assert "file" not in all_df.columns
    assert "annotator" not in all_df.columns
    assert "label_whistle" not in all_df.columns

    # add missing fields
    temp_df = all_df.copy()
    col_missing = ["S_str_1", "area_idx", "f_idx_max", "f_idx_min", "f_len_hz", "f_len_idx", "mask_area_pct", "mask_freq_pct", "mask_time_pct", "t_idx_max", "t_idx_min", "t_len_idx", "t_len_sec"]
    for col_i in col_missing:
      assert col_i not in temp_df.columns # make sure it doesn't exist, to avoid over-writing a valid field
      temp_df[col_i] = None

    temp_df.rename(columns={"csv_p1": "file", "csv_p2": "annotator", "tonal_id": "label_whistle"}, inplace=True)

    return cls(temp_df)


  def to_mobydiff_csv(self, fn_csv):
    cols_use = ["t_sec_min", "t_sec_max", "f_hz_min", "f_hz_max", "label_whistle"]
    df_wc = self.s23_whistle_corpus.copy()
    df_wc = df_wc.sort_values(cols_use)
    df_wc = df_wc[cols_use]
    #df_wc = df_wc.round(5)

    # Update: for t_sec_* change from round(1) to round(2) to match pamguard and manual in terms of required precision
    df_wc[["t_sec_min", "t_sec_max"]] = df_wc[["t_sec_min", "t_sec_max"]].round(2)
    df_wc[["f_hz_min", "f_hz_max"]] = df_wc[["f_hz_min", "f_hz_max"]].astype(int) # //1000
    df_wc.rename(columns={
          #"t_sec_min": "t_sec__min", "t_sec_max": "t_sec__max",
          #"f_hz_min": "f_khz__min", "f_hz_max": "f_khz__max",
          "label_whistle": "tonal_id"
        }, inplace=True)
    #print(df_wc.head())
    df_wc.to_csv(fn_csv, index=False)


  def hist(self, dataset_name, annot_l=None):
    if annot_l is None:
      annot_l = sorted(list(set(self.s23_whistle_corpus.annotator.tolist())))

    hp = HistPlotter(self.s23_whistle_corpus, dataset_name, annot_l)
    hp.hist_t_len()
    hp.hist_f_len()
    hp.hist_f_xxx("f_hz_min")
    hp.hist_f_xxx("f_hz_max")


