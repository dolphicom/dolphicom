#from tqdm.notebook import tqdm
from tqdm.auto import tqdm

# Scipy imports
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.io.wavfile.read.html
from scipy.io import wavfile
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html
from scipy import signal
from scipy import ndimage
from scipy.sparse import csr_matrix
import scipy
from scipy.signal import decimate

import numpy as np
import matplotlib.pyplot as plt

import os

from ..spectrogram_helpers.s12_drop_bursts import s12_drop_bursts as s12_drop_bursts__helper
from ..spectrogram_helpers.s13_remove_noise__blurflood import remove_noise as remove_noise__blurflood, get_f_focus
from ..spectrogram_helpers.s14_drop_periodic_blobs import S14DropPeriodicBlobs, S14AbortNoPeriodicBlobs
from ..spectrogram_helpers.s21b_strengthen_whistles import S21StrengthenWhistles
from ..spectrogram_helpers.s21a_drop_blobs import s21a_drop_blobs as s21a_drop_blobs__helper
from ..spectrogram_helpers.s22b_whistle_connector import chunked_connector
from ..spectrogram_helpers.s22a_watershed import S22aWatershed
from ..spectrogram_helpers.s23b_whistle_segwrap import WhistleSegWrap
from ..spectrogram_helpers.s3_reporter import Reporter
from ..spectrogram_helpers.s41_stats import S41Stats
from ..spectrogram_helpers.s42_tracer import S42Tracer
from ..spectrogram_helpers.s43_transliterator import S43WhistleToBraille, s43_replace_similar_br, plot_s43_braille_examples
from ..spectrogram_helpers.s44_subtitles import S44Subtitles

from ..corpus_ytq1_v20210524e import plot_s43_examples as plot_ytq1_v20210524e_s43_examples, plot_corpus_examples as plot_ytq1_v20210524e_corpus_examples
#from .corpus_ytq1_v20210524e import assert_s43_examples as assert_ytq1_v20210524e_s43_examples

import pandas as pd

from ..utils import AbortPipeline, iter_batch, np_index_at_value, pydub_readaudio, my_imshow

import gc
from pydub import AudioSegment
from skimage.filters import gaussian


class Spectrogram:

  def __init__(self):
    self.Sxx_checkpoints = {}

  def _s11_read_wav_no_save_members(self, fn_in, channel, t_sec_start, t_sec_end):
    """
    fn_in: path to wav file
    """
    # Update: no need for this anymore since using pydub_readaudio which has it
    #song = AudioSegment.from_file(fn_in)
    #print(f"Reading file of length {int(song.duration_seconds)} seconds: '{fn_in}'")

    # Method 1: use scipy.io.wavfile
    # fs_ori, data = wavfile.read(fn_in)
    #data = data[:,0]

    # Method 2: use pydub to read other formats than wav too
    fs_ori, data = pydub_readaudio(fn_in, t_sec_start, t_sec_end)

    if len(data.shape)==2:
      print(f"[WARNING] wav file contains {data.shape[1]} channels. Reading channel {channel}")
      data = data[:, channel]

    if len(data.shape)>2:
      raise Exception(f"Got data with more than 2 dimensions. Got {len(data.shape)} dimensions for {fn_in}")

    # downsample to get closer to 44.1 kHz
    # Also done in Gruden 2013 https://eprints.soton.ac.uk/400139/1/JASA-00512_R1.pdf
    # It's ok to use the decimate function since I'm just dealing with the frequency domain
    # Ref: https://stackoverflow.com/a/55209505/4126114
    #   and comment there that references a blog post that recommends scikit-learn's resample
    #   https://scikit-learn.org/stable/modules/generated/sklearn.utils.resample.html
    fs_target = 44100     # like marley 2017 and youtube downloaded wav files
    assert fs_ori >= fs_target
    decimate__q = fs_ori//fs_target # 4
    fs_new = fs_ori//decimate__q
    if decimate__q != 1:
      print(f"[WARNING] Downsampling from {fs_ori} Hz to {fs_new} Hz")
      data = decimate(data, q=decimate__q)

    # calculate spectrogram
    f, t, Sxx = signal.spectrogram(data, fs_new)

    # take log for clearer whistles as per trial and error on marley 2017
    Sxx = np.log(1+Sxx)

    if (Sxx==0).all():
      raise AbortPipeline("Empty spectrogram => no audio. Aborting")

    # faster sec_all calculation
    sec_all = np.where(np.diff(t.astype(int), prepend=-1)==1)[0].tolist()

    return f, t, Sxx, sec_all


  def s11_read_wav(self, fn_in, channel, t_sec_start, t_sec_end):
    print("Step 1.1: read wav file")

    self.fn_in = fn_in
    self.channel = channel
    self.t_sec_start = t_sec_start
    self.t_sec_end = t_sec_end

    f, t, Sxx, sec_all = self._s11_read_wav_no_save_members(fn_in, channel, t_sec_start, t_sec_end)

    # cast numbers < 25% of max (the 99% quantile) to 0 in order to avoid connectedness due to small non-zero numbers
    # This step is fundamental and hence is included as part of the reading function, not any of the postprocessing functions
    thresh_val = np.quantile(Sxx, .99)*1/10
    Sxx[Sxx < thresh_val] = 0

    # save as class members
    self.f = f
    self.t = t
    self.Sxx = Sxx
    self.Sxx_checkpoints["s11_original"] = np.copy(self.Sxx)
    self.sec_all = sec_all


  def _drop_bursts(self, hard, cp_k1, cp_k2, verbose):
    """
    hard: Use false for usage on noisy background. Use true for usage on small vertical spikes with not a lot of background

    Update 2021-06-04: changed from 500x2 filter which was way larger than 129 frequencies to 30x2 and mode=nearest and just subtract from input
    Will have to keep an eye out on how this behaves. I may have already tested this on Marley 2017 and decided it was not a good idea, but it
    avoids the issue of different max values for first 30s versus full 1h of full ytq1 audio file
    Update: added notes that this is not a vanilla burst subtraction, but an "average" subtraction and clipping at 0
    Also, add back horizontal filtering over time and parametrize the filters by seconds and hertz instead of number of points
    """

    # The below number of points are larger than what a whistle can cover vertically and horizontally
    # I determined them by trial and error on sample whistles
    # Update: Not a good idea to change n_hz from 5 khz to 2.5 khz since it eats out from the dolphin whistles
    #n_hz, n_sec = 30, 300
    n_sec = np_index_at_value(self.t, 1.5) # 1.5 seconds
    n_hz = np_index_at_value(self.f, 5000)
    #n_hz = np_index_at_value(self.f, 2500)

    # Update 2021-08-23: change from 2 points to 1 point, which captures vertical bursts better for NOAA PIFSC
    # (since it matches 1-pt wide lines, and not just 2-pt wide lines as a minimum)
    n_width = 1 # 2

    #self.Sxx = s12_drop_bursts__helper(self.Sxx, 500, 2, hard, verbose) # vertical
    self.Sxx = s12_drop_bursts__helper(self.Sxx, n_hz,   n_width, hard, verbose) # vertical

    # Update: the horizontal filtering was good for Marley 2017 but not for the youtube audio files
    # which had noise in the 7.4khz-7.9khz range on top of whistles. Instead of this approach,
    # rely on the f_focus parameter in remove_noise__blurflood which can drop the noise even
    # if it is overlaid by a whistle
    # Update: calculating it for usage in s22a_watershed
    self.Sxx_checkpoints[cp_k1] = s12_drop_bursts__helper(self.Sxx,    n_width, n_sec, hard, verbose) # horizontal over time

    self.Sxx_checkpoints[cp_k2] = np.copy(self.Sxx)


  def s12_drop_bursts(self):
    print("Step 1.2: drop bursts")
    self._drop_bursts(False, "s12c_drop_bursts_hori_and_vert", "s12b_drop_bursts", False)


  # Update 2021-06-07: replace this with the utils.remove_noise function that I developed in notebook t07.2c
  # https://colab.research.google.com/drive/1SUwas0-syDGCfIAP38i31phXSn7wu27Z#scrollTo=HmmQKWOCIVYb
  # It keeps more whistles, including those with value < 4
  # Update: keep this as a "fast" alternative to the blur-flood method
  def _s13_remove_noise__thresholds(self):
    # Some more processing with opening and dilation
    # method 1, works
    # FIXME: using "4" as threshold, but maybe this varies by audio file?
    # Could use a percentile instead?
    self.Sxx[self.Sxx < 4] = 0

    # In notebook t07.2b, I observed a consistent noise between 7.5kHz and 7.7kHz on the ytq1 query (250 videos for bottlenose dolphin whistle)
    # Removing them here, deterministically, without recalculation
    # https://colab.research.google.com/drive/1SUwas0-syDGCfIAP38i31phXSn7wu27Z#scrollTo=qRjhPKFVmtNs
    self.Sxx[(7400 <= self.f) & (self.f <= 7900), :] = 0

    # Dilate with small diagonal kernel
    self.Sxx = ndimage.grey_dilation(self.Sxx, size=(5,5))

    # Do this after the ndimage opening and dilation
    #self.Sxx[self.f < 5000, :] = 0
    self.Sxx[self.f < 6000, :] = 0


  def _s13_remove_noise__blurflood(self):
    # Still dropping all content below 6kHz
    # Instead of zero'ing it, just cut off from the matrix => smaller matrix => faster calculations
    # Update: back to zero'ing for the sake of being aligned with the raw spectrogram
    # Update: commenting out in favor of using the flood-fill noise removal in those frequency ranges
    #f_min, f_max = 6000, 16000
    #f_keep = (self.f >= f_min) & (self.f <= f_max)
    #self.Sxx[~f_keep, :] = 0

    # call remove_noise function in batches of 5 seconds
    #sps = 195
    #assert (self.t[sps] - 1) < .1
    # Update: mobysounds.org wav files are sampled at different frequencies
    sps = np_index_at_value(self.t, 1)
    #assert np.abs(sps-195) < 5

    # Update: use 15 seconds per batch instead of 5 seconds
    # Update: go back to 15 seconds per batch since a bit slower with 15s
    # Need to optimize the underlying code anyway, especially after the addition of watershed step, which makes the high precision in this step kind of useless
    b_size = sps*5

    # Last entry in batches might be too small, so merge it with the previous one
    #if b_range.shape[0] > 2:
    #    b_range[-2] = b_range[-1]
    #    b_range = b_range[:-1]

    # build list of frequency pairs to focus on in noise removal
    # Update: compute f_focus windows from spectrogram
    #f_6000hz, f_7400hz, f_7900hz, f_16khz = [np_index_at_value(x) for x in [6000, 7400, 7900, 16000]]
    #f_max = self.f.shape[0] - 1
    #f_focus = [
    #  (       0, f_6000hz),
    #  (f_7400hz, f_7900hz),
    #  (f_16khz , f_max   ),
    #]


    Sxx2 = self.Sxx.copy()
    clean_l = []
    for b_start, b_end, Sxx_sub in iter_batch(Sxx2, b_size, "s13 remove noise (long, blur-flood)"):
      f_focus = get_f_focus(Sxx_sub, verbose=False)
      sub_clean = remove_noise__blurflood(Sxx_sub, f_focus, do_plot=False)
      assert not np.isnan(sub_clean).any()
      clean_l.append(sub_clean)

    # done
    self.Sxx = np.concatenate(clean_l, axis=1)
    print(f"After s13, self.Sxx.shape={self.Sxx.shape} versus original size {Sxx2.shape}")
    assert self.Sxx.shape == Sxx2.shape

    # for some reason, numpy gets a "matrix" instead of an ndarray, so convert back
    # Update: this was happening when I had csr_matrix passed into the remove noise function.
    # Now that I found out that csr_matrix doesn't mix well with numpy,
    # I'm sticking to csr_matrix in the checkpoints dict only, hence this should still be a regular array here
    assert type(self.Sxx) == np.ndarray


  def s13_remove_noise(self, fast):
    print(f"Step 1.3: remove noise")

    if fast:
      self._s13_remove_noise__thresholds()
    else:
      self._s13_remove_noise__blurflood()

    # Sxx after s1_preprocess, i.e. after the time-consuming remove_noise operation
    # Use csr_matrix to save memory space
    self.Sxx_checkpoints["s13_remove_noise"] = scipy.sparse.csr_matrix(np.copy(self.Sxx))


  def s14_drop_periodic_blobs(self):
    print("Step 1.4: drop periodic blobs (~10khz, 8-second intervals)")

    # S_in = p.sp_filt.Sxx_checkpoints["s13_remove_noise"].toarray().copy()
    S_in = self.Sxx.copy()

    s14 = S14DropPeriodicBlobs(S_in, self.t, self.f, verbose=False)
    try:
      s14.calc_in_out_band()
      s14.find_peaks()
      s14.calc_peak_start()
      s14.label_bursts()

      print(f"Found {s14.peaks_vec.shape[0]} periodic blobs at 8-second intervals")
      self.Sxx = s14.S_out.copy()
      assert type(self.Sxx) == np.ndarray

    except S14AbortNoPeriodicBlobs as e:
      print(str(e))

    # this is the spectrogram without the periodic blobs
    # or it is the same spectrogram as in s13 if no periodic blobs found
    self.Sxx_checkpoints["s14_drop_periodic_blobs"] = scipy.sparse.csr_matrix(np.copy(self.Sxx))


  def s21a_drop_blobs(self, verbose=False):
    msg, k = "Step 2.1a: drop blobs", "s21a_drop_blobs"
    #msg, k = "Step 2.1c: drop blobs, second time", "s21c_drop_blobs_again"
    #raise ValueError(f"s21a_drop_blobs(stage={stage}) is not supported. Allowed values: 1, 2")

    print(msg)
    if not self.Sxx.any():
      raise AbortPipeline("Spectrogram is completely empty. Will abort the spectrogram processing.")

    self.Sxx = s21a_drop_blobs__helper(self.Sxx, verbose)
    self.Sxx_checkpoints[k] = scipy.sparse.csr_matrix(np.copy(self.Sxx))


  def s21b_strengthen_whistles(self):
    print("Step 2.1b: sp.strengthen_whistles (ETA 17s on 7m video, RAM 12 GB?; ETA ~1m on 1h audio file full of whistles)")

    # FIXME: I chose the KDIM by trial and error comparing the time position 246s (straight up) to 0s (no whistle)
    # Update: reducing from 15 to 10 to capture more of the inverted U top
    # Update: moving to function
    #KDIM=15
    #KDIM=10
    s21 = S21StrengthenWhistles(self.Sxx, detailed=False, verbose=False)
    self.Sxx = (
         # This will generate a choppy whistle if it has a thin 1-pixel connection at some point, but does a good job at ridding from noise
         s21.sloped_opening(11, True)
        # This is a bit too long for whistles, but does a good job at ridding from noise
         + s21.sloped_opening(15, False)
    )

    #plt.imshow(Sxx_i, origin="lower")
    #plt.show()
    #
    #plt.imshow(S_stronger, origin="lower")
    #plt.show()

    # Memory garbage collection is useful at this stage
    gc.collect()

    # save to checkpoints
    # Update: this step is not so slow, and Sxx at this stage takes up a significant amount of memory wrt colab. Skipping this checkpoint.
    # Update: saving this again with csr_matrix
    # self.Sxx_checkpoints["s21b_strengthen_whistles"] = np.copy(self.Sxx)
    self.Sxx_checkpoints["s21b_strengthen_whistles"] = scipy.sparse.csr_matrix(np.copy(self.Sxx))


  def s22b_connect_broken_whistles(self):
    print("Step 2.2c: sp.connect_broken_whistles (ETA ~ 2.5 minutes on 1h audio)")
    self.Sxx, self.s22_connections = chunked_connector(self.t, self.f, self.Sxx, batch_size_sec=5)
    # storing checkpoint since s22 is time-consuming (5m on 1h audio file)
    # Update: change from s22 to s22d since moved till after 2nd pass of remove noise and after s22b watershed
    self.Sxx_checkpoints["s22b_connect_broken_whistles"] = scipy.sparse.csr_matrix(np.copy(self.Sxx))


  def s22a_watershed(self):
    print("Step 2.2a: watershed")

    # assert types
    typemap = {
      "s11_original": np.ndarray,
      "s12b_drop_bursts": np.ndarray,
      "s12c_drop_bursts_hori_and_vert": np.ndarray,
      "s22b_connect_broken_whistles": scipy.sparse.csr_matrix,
    }
    for k,v in typemap.items():
      if k in self.Sxx_checkpoints.keys():
        assert type(self.Sxx_checkpoints[k]) == v

    assert type(self.Sxx) == np.ndarray

    Sxx_s12 = self._s22a_watershed("s12c_drop_bursts_hori_and_vert")
    Sxx_s13 = self._s22a_watershed("s13_remove_noise")

    # combine
    #self.Sxx = (Sxx_s12 + Sxx_s13)/2
    self.Sxx = np.maximum(Sxx_s12, Sxx_s13)
    self.Sxx_checkpoints["s22a_watershed"] = scipy.sparse.csr_matrix(np.copy(self.Sxx))


  def _s22a_watershed(self, k):
    """
    k: key in Sxx_checkpoints to use as input
    """
    t_5s = np_index_at_value(self.t, 5)

    watershed_l = []
    for i_start, i_end, _ in iter_batch(self.Sxx, t_5s, "s22a_watershed"):
      Sxx_ori = self.Sxx_checkpoints["s11_original"][:, i_start:i_end]

      # Update: testing if better results getting back from s13 with watershed
      #Sxx_input = self.Sxx_checkpoints["s12b_drop_bursts"][:, i_start:i_end]
      #Sxx_input = self.Sxx_checkpoints["s13_remove_noise"][:, i_start:i_end].toarray()
      Sxx_input = self.Sxx_checkpoints[k][:, i_start:i_end]
      if type(Sxx_input) == scipy.sparse.csr_matrix: Sxx_input = Sxx_input.toarray()

      # Update: could just get markers from s21, which at this stage is at the latest Sxx value
      #Sxx_markers = self.Sxx_checkpoints["s22b_connect_broken_whistles"][:, i_start:i_end].toarray()
      Sxx_markers = self.Sxx[:, i_start:i_end]

      # run watershed
      s22a_watershed = S22aWatershed(Sxx_ori, Sxx_input, Sxx_markers, verbose=False)
      s22a_watershed.calc_input()
      s22a_watershed.calc_markers()
      s22a_watershed.calc_watershed()
      watershed_l.append(s22a_watershed.watershed)

    # combine
    o = np.concatenate(watershed_l, axis=1)
    return o


  def s22c_drop_bursts(self):
    """
    Since watershed multiplies the mask with the input being the max of s12b (drop bursts hor and vert) and s13 (remove noise),
    it gets some vertical branching.
    Drop bursts again to get rid of them.
    Do this before the connector to avoid false connections between the vertical lines
    FIXME should rename this to s22b and rename the s22b to s22c
    """
    print("Step 2.2b: drop bursts (like step 1.2)")
    self._drop_bursts(True, "s22c_dropb_hori_and_vert", "s22c_drop_bursts", False)


  def s23_segment_hdbscan(self, include_s23_blur=True, include_s23_hdbscan=True, include_s23_mender=True):
    # use hdbscan to label whistles and segment them
    # also merge some whistles together if they're close
    print("Step 2.3: sp.segment_hdbscan + connect broken again (ETA 1m on 1h audio)")
    self.s23_wsw = WhistleSegWrap(self, verbose=False, include_hdbscan=include_s23_hdbscan)
    if include_s23_blur:
        self.s23_wsw.blur()
    else:
        # since not blurring, interested in keeping whistle splits.
        # Drop the small values inside those gaps, so that the 2 segments are not combined in the segmenter
        self.Sxx[self.Sxx < (self.Sxx.max()/2)] = 0

    self.s23_wsw.segment_init()

    if include_s23_mender:
        self.s23_wsw.get_close()
        self.s23_connections = self.s23_wsw.cl_all.copy()

        if self.s23_connections.shape[0] > 0:
          self.s23_connections["t_sec_mid"] = self.s23_connections.eval("(t_sec_start + t_sec_end)/2")
          self.s23_connections["f_hz_mid"]  = self.s23_connections.eval("(f_hz_start  + f_hz_end )/2")

        self.s23_wsw.connect()

    # done with mending if requested
    self.s23_wsw.segment_repeat()

    self.Sxx = self.s23_wsw.sp_filt.Sxx # replace with this re-connected version
    self.s23_whistle_corpus = self.s23_wsw.ws_final.whistle_corpus2.copy()
    #self.s23_whistle_corpus["S"] = self.s23_wsw.ws_final.whistle_corpus2["S"].apply(lambda S: np.copy(S)) # pandas.DF.copy doesn't deep-copy numpy arrays

    self.s23_wsw.remove_intermediate_variables()


  def s3_report(self):
    print("Step 3: report")
    wm = Reporter(self)
    #wm.get_t_whistles(show_hist=False)
    wm.get_t_whistles()
    wm.convert_twhistle_to_plotrange_params()

    # Update: no need to plot and print when building wm
    #wm.plot_t_whistles()
    #wm.print_report()

    return wm


  def s4_to_braille(self, verbose):
    if self.s23_whistle_corpus.shape[0]==0:
      raise AbortPipeline("No whistles detected. Skipping steps 4.{1,2,3,4} altogether")

    print("Step 4.1: spectrogram.s4_to_braille.s41_stats")
    s41_s = S41Stats(self.t, self.f, self.Sxx, self.s23_whistle_corpus)

    if verbose:
      s41_s.plot_1_hist_area_tlen_flen()
      s41_s.plot_2_hist_2d_tlen_flen()

    s41_s.set_S_samelen()
    whistle_corpus3 = s41_s.whistle_corpus3 # copy df out

    if verbose:
      s41_s.plot_3_S_samelen(100) # sample first 100 whistles
      #s41_s.plot_3_S_samelen(100*10) # full 1k whistles

    print("Step 4.2: spectrogram.s4_to_braille.s42_tracer")
    s42_t = S42Tracer(whistle_corpus3)
    s42_t.trace_whistles(self.f)
    whistle_corpus3 = s42_t.whistle_corpus3

    if verbose:
      s42_t.plot_examples_1()
      s42_t.plot_examples_2(self.t, self.f)
      s42_t.plot_examples_3_gradient()

    print("Step 4.3: spectrogram.s4_to_braille.s43_braille")

    if verbose:
        plot_s43_braille_examples(whistle_corpus3)
        plot_ytq1_v20210524e_s43_examples(whistle_corpus3)

    #assert_ytq1_v20210524e_s43_examples(whistle_corpus3)

    whistle_corpus3["S_str_1"] = whistle_corpus3.S_1d.apply(S43WhistleToBraille(False).piecewise)

    # Disable the slash replacement for now
    #whistle_corpus3["S_str_2"] = s43_replace_similar_br(whistle_corpus3["S_str_1"], verbose=verbose)
    whistle_corpus3["S_str_2"] = whistle_corpus3["S_str_1"].copy()

    # use string representation (mix of braille and slashes) as cluster label
    whistle_corpus3["label_cluster"] = whistle_corpus3["S_str_2"]
    # whistle_corpus3.rename(columns={"mask_area_pct": "_mask_area_pct"}, inplace=True) # hide this column from plotting

    if verbose:
        plot_ytq1_v20210524e_corpus_examples(self)

        # more examples from all audio file
        print("Examples for mended(1)")
        self.plot_corpus(0, int(p.sp_filt.t.max()+1), "mended(1)", 2)
        print("Examples for mended(2)")
        self.plot_corpus(0, int(p.sp_filt.t.max()+1), "mended(2)", 2)
        print("Example whistles")
        self.plot_corpus(0, int(p.sp_filt.t.max()+1), "whistles", 2)
        print("Example any detection")
        self.plot_corpus(0, int(p.sp_filt.t.max()+1), "any detection", 2)
        print("Example everything")
        self.plot_corpus(0, int(p.sp_filt.t.max()+1), "all", 2)

    print("Step 4.4: spectrogram.s4_to_braille.s44_subtitles")
    subtit = S44Subtitles(whistle_corpus3)
    subtit.df()

    if verbose: print(subtit.whistle_subtitles.head())

    fn_srt = "ytq1_subtitles.srt"
    subtit.write(fn_srt)
    print(f"Subtitles saved into {fn_srt}")

    self.s23_whistle_corpus = whistle_corpus3

    print("Step 4.5: spectrogram.s4_to_braille.plot_cluster_label_examples")
    if verbose: self.plot_cluster_label_examples()


  def plot_range(self, sec_start, sec_marker, show, save_dir=None, save_suffix=None, sec_more_markers=[], Sxx_mask=None, Sxx_plot=None):
    """
    sec_start: seconds at which to plot the spectrogram, in time
    sec_marker: position in seconds at which to place a vertical line as a marker. None to skip
    show: True to display with plt.show, False otherwise
    save_dir: None to not save png to a directory
    save_suffix: string to append to the png filename as "filename-suffix.png"
    sec_more_markers: list of locations in seconds where to place more vertical lines
    Sxx_mask: same size as self.Sxx, with nan where there are no whistles, 1 or other where there are whistles
    Sxx_plot: Spectrogram to plot. Default None will evaluate to self.Sxx. Could also be self.Sxx_checkpoints["s11_original"] or another value from the dictionary.
    """
    #i_offset = 1000 # 500. # fixed window size. 1000 ~ 5 seconds
    #i2_idx = i1_idx + i_offset
    f, t, Sxx, sec_all = self.f, self.t, self.Sxx, self.sec_all
    Sxx = self.Sxx if Sxx_plot is None else Sxx_plot

    #s23_whistle_corpus: metadata attached to each whistle
    #  Fields that show up in plot: label_whistle (whistle ID?), label_cluster (cluster ID)
    s23_whistle_corpus = getattr(self, "s23_whistle_corpus", None)
    s22_connections = getattr(self, "s22_connections", None)
    s23_connections = getattr(self, "s23_connections", None)

    idx_start = sec_all[sec_start] # index for second number sec_i
    sec_end = min(len(sec_all)-1, sec_start+5)
    idx_end = sec_all[sec_end] # index of 5 seconds later

    if idx_start == idx_end: return None

    fig = plt.figure(figsize=(20,5))
    #plt.pcolormesh(t[:500], f, np.log(Sxx[:,:500]), shading='gouraud', cmap="Greys")
    #plt.pcolormesh(t[-750:-250], f, np.log(Sxx[:,-750:-250]), shading='gouraud', cmap="Greys")

    # Update: need to convert sparse matrix to array for pcolormesh
    # If getting Sxx_checkpoints["s11_original"], then not csr_matrix
    # If getting s13_remove_noise key, then it is a csr_matrix
    Sxx_sub = Sxx[:,idx_start:idx_end]
    if type(Sxx_sub)==scipy.sparse.csr_matrix: Sxx_sub = Sxx_sub.toarray()

    #plt.pcolormesh(t[idx_start:idx_end], f, Sxx[:,idx_start:idx_end], shading='gouraud', cmap="Greys")
    plt.pcolormesh(t[idx_start:idx_end], f, Sxx_sub, shading='gouraud', cmap="Greys")


    def calc_sec_min(idx_start):
      sec_init = t[idx_start]
      min_init = int(sec_init//60)
      sec_mod = int(sec_init%60)
      return sec_init, min_init, sec_mod

    _, t_start_min, t_start_sec = calc_sec_min(idx_start)
    _, t_end_min, t_end_sec = calc_sec_min(idx_end)

    # draw time marker
    if sec_marker is not None:
        idx_marker = sec_all[sec_marker]
        t_marker_sec_init, t_marker_min, t_marker_sec_mod = calc_sec_min(idx_marker)
        plt.axvline(t_marker_sec_init, color="blue", alpha=.5)

    # draw more markers
    for sm_i in sec_more_markers:
      idx_marker = sec_all[sm_i]
      t_more_marker, _, _ = calc_sec_min(idx_marker)
      plt.axvline(t_more_marker, color="red", alpha=.5)

    # draw circles on mended(1) areas
    if (s22_connections is not None) & (Sxx_mask is not None):
      if s22_connections.shape[0] > 0:
        get_idx = lambda v: (sec_start <= v) & (sec_end >= v)
        mend1_sub = s22_connections[get_idx(s22_connections.t1)]
        for mend1_i in mend1_sub.itertuples(index=False):
          plt.plot([mend1_i.t1,mend1_i.t0_n], [mend1_i.f1, mend1_i.f0_n], color="green", linewidth=5, alpha=.3)
          plt.scatter( mend1_i.t_mid, mend1_i.f_mid, s=300, color="green", alpha=.3 )
          plt.axvline(x=mend1_i.t_mid, color="green", alpha=.3)
          plt.text(x=mend1_i.t_mid+.1, y=mend1_i.f_mid+1000, s="m1", color="white", bbox=dict(facecolor='green', alpha=0.5))



    # to be moved to plot_range
    if (s23_connections is not None) & (Sxx_mask is not None):
      if s23_connections.shape[0] > 0:
        get_idx = lambda v: (sec_start <= v) & (sec_end >= v)
        mend2_sub = s23_connections[get_idx(s23_connections.t_sec_start)]
        for mend2_i in mend2_sub.itertuples(index=False):
          plt.plot([mend2_i.t_sec_start, mend2_i.t_sec_end], [mend2_i.f_hz_start, mend2_i.f_hz_end], color="green", linewidth=5, alpha=.3)
          plt.scatter( mend2_i.t_sec_mid, mend2_i.f_hz_mid, s=300, color="green", alpha=.3 )
          plt.axvline(x=mend2_i.t_sec_mid, color="green", alpha=.3)
          plt.text(x=mend2_i.t_sec_mid+.1, y=mend2_i.f_hz_mid+1000, s="m2", color="white", bbox=dict(facecolor='green', alpha=0.5))


    # axes and title
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    #time_range = f"{t_start_min}:{t_start_sec}-{t_end_min}:{t_end_sec}"
    time_range = "%0.4i:%0.2i-%0.4i:%0.2i"%(t_start_min, t_start_sec, t_end_min, t_end_sec)

    fn_base = os.path.basename(self.fn_in)

    if s23_whistle_corpus is not None:
      # Update 2021-06-15: include whistles that end *after* the displayed frame, but crop the rectangle
      #wc3_5s = s23_whistle_corpus[(s23_whistle_corpus.t_sec_min>=sec_start) & (s23_whistle_corpus.t_sec_max<=sec_end)].copy()

      get_idx = lambda v: (sec_start <= v) & (v <= sec_end)
      idx_t1 = get_idx(s23_whistle_corpus.t_sec_min)
      idx_t2 = get_idx(s23_whistle_corpus.t_sec_max)
      wc3_5s = s23_whistle_corpus[idx_t1 | idx_t2].copy()
      #print("# whistles: ", wc3_5s.shape)

      if wc3_5s.shape[0] > 0:
        # Use filename from whistle corpus if present. Useful for multi-file corpus
        fn_b1 = wc3_5s.iloc[0].dir_base if "dir_base" in wc3_5s.columns else None
        fn_b2 = wc3_5s.iloc[0].fn_pkl   if "fn_pkl"   in wc3_5s.columns else None
        if fn_b1 is not None and fn_b2 is not None:
          fn_base = os.path.join(fn_b1, fn_b2)
        elif fn_b2 is not None:
          fn_base = fn_b2

    if sec_marker is None:
      title_i = f"{fn_base}\nTime: {time_range}"
    else:
      time_marker = "%0.4i:%0.2i"%(t_marker_min, t_marker_sec_mod)
      title_i = f"{fn_base}\nTime: {time_range}, Marker: {time_marker}"

    plt.title(title_i)

    # Update: disable colorbar
    #plt.colorbar()

    # draw a mask on the whistles *after* the colorbar is drawn
    # Red: is whistle, Orange: was whistle but dropped
    if Sxx_mask is not None:
      Sm_sub = Sxx_mask[:,idx_start:idx_end]
      if type(Sm_sub)==scipy.sparse.csr_matrix: Sm_sub = Sm_sub.toarray()
      Sm_sub[Sm_sub==0] = np.nan
      plt.contourf(t[idx_start:idx_end],
                     f,
                     Sm_sub,
                     colors=["yellow"], alpha=.2)

    # write cluster labels on plot
    if s23_whistle_corpus is not None:
      ax = plt.gca()
      for i, wc3_i in wc3_5s.iterrows():
        text_val = f"#{int(wc3_i.label_whistle)}"
        if "mask_area_pct" in s23_whistle_corpus.columns: text_val = f"{text_val}, {wc3_i.mask_area_pct} A%"
        if "label_cluster" in s23_whistle_corpus.columns: text_val = f"{text_val}, Cl {wc3_i.label_cluster}"
        plt.text(wc3_i.t_sec_min, wc3_i.f_hz_max+200, text_val)
        # rectangle width in time is capped at the "sec_end" limit of the plot to avoid overflowing outside the 5-second frame limit
        rect = plt.Rectangle((wc3_i.t_sec_min, wc3_i.f_hz_min),
                             min(wc3_i.t_sec_max, sec_end) - wc3_i.t_sec_min,
                             wc3_i.f_hz_max-wc3_i.f_hz_min,
                             angle=0.0, facecolor="none", edgecolor="black")
        ax.add_patch(rect)

        # draw trace of whistle
        if Sxx_mask is not None:
          S_x = self.t[wc3_i.t_idx_min:(wc3_i.t_idx_min+wc3_i.t_len_idx)]
          plt.plot(S_x, wc3_i.f_hz_min + wc3_i.S_1d)

    if save_dir is not None:
      if sec_marker is None:
        fn_range = f"{save_dir}/range_{time_range}-{save_suffix}.png"
      else:
        fn_range = f"{save_dir}/range_{time_range}-marker_{time_marker}-{save_suffix}.png"

      plt.savefig(fn_range)
      plt.close()
      return fn_range

    if show:
      plt.show()
      plt.close()
      return None

    return fig


  def plot_corpus(self, sec_min=0, sec_max=50, plot_type="any detection", plot_max=10):
    """
    Wraps the plot_range function, and can filter sections of type <plot_type>.

    Parameters:
      plot_type: all, whistles, mended(1), mended(2), any detection
      plot_max: -1 for no limit, 10 for 10 plots
    """
    assert plot_type in ["all", "whistles", "mended(1)", "mended(2)", "any detection"]
    wc = self.s23_whistle_corpus

    # Method 1: Update: after using sparse matrices for Sxx, need to deal with this differently
    #Sxx_mask = np.full_like(self.Sxx, np.nan)
    #Sxx_mask[self.Sxx!=0] = 1

    # Method 2: this is how to do this with sparse matrices
    Sxx_mask = scipy.sparse.csr_matrix(self.Sxx.shape)
    #Sxx_mask[:,:] = np.nan
    Sxx_mask[self.Sxx!=0] = 1

    # adding mended whistles
    t, f = self.t, self.f
    mend_1 = self.s22_connections
    mend_2 = self.s23_connections

    Sxx_plot = None
    if "s11_original" in self.Sxx_checkpoints:
      Sxx_plot = self.Sxx_checkpoints["s11_original"]
    else:
      if "s13_remove_noise" in self.Sxx_checkpoints:
        Sxx_plot = self.Sxx_checkpoints["s13_remove_noise"]
        print("Couldnt find key s11_original in spectrogram.Sxx_checkpoints. Fell back to key s13_remove_noise")
      else:
        raise Exception("Couldnt find key s11_original nor s13_remove_noise in spectrogram.Sxx_checkpoints. Aborting")

    # Update: perhaps could have simply done t_max = self.t.max() ? lol
    # Update: using self.t.max() for simplicity
    # t_max = max(wc.t_sec_min.max(), mend_1.t1.max() if mend_1.shape[0]>0 else 0, mend_2.t_sec_start.max() if mend_2.shape[0]>0 else 0)
    t_max = self.t.max()

    for sec_st in np.arange(sec_min,sec_max,5):
      if plot_type != "all":
        if sec_st + 5 > t_max:
          print(f"[WARNING] No data left after {sec_st} seconds. Will break")
          break

      get_idx = lambda v: (sec_st <= v) & ((sec_st+5) >= v)
      wh_sub = wc[get_idx(wc.t_sec_min)]
      mend1_sub = pd.DataFrame([]) if mend_1.shape[0]==0 else mend_1[get_idx(mend_1.t1)]
      mend2_sub = pd.DataFrame([]) if mend_2.shape[0]==0 else mend_2[get_idx(mend_2.t_sec_start)]

      if plot_type=="whistles":
        if wh_sub.shape[0]==0: continue
      elif plot_type=="mended(1)":
        if mend1_sub.shape[0]==0: continue
      elif plot_type=="mended(2)":
        if mend2_sub.shape[0]==0: continue
      elif plot_type=="any detection":
        if wh_sub.shape[0]==0 and mend1_sub.shape[0]==0 and mend2_sub.shape[0]==0: continue
      elif plot_type=="all":
        pass
      else:
        raise ValueError(f"Unsupported plot_type={plot_type}")

      # First without highlight
      self.plot_range(
                        sec_st, sec_st, False, None, None, [],
                        Sxx_mask=None,
                        Sxx_plot = Sxx_plot
                        )
      plt.show()
      plt.close()

      # Next with highlight
      self.plot_range(
                        sec_st, sec_st, False, None, None, [],
                        Sxx_mask=Sxx_mask,
                        Sxx_plot = Sxx_plot
                        )
      plt.show()
      plt.close()

      # Note that plot_max=-1 will never hit this
      plot_max -= 1
      if plot_max == 0:
        print("Reached limit plot_max")
        break

  def plot_cluster_label_examples(self):
      t, f = self.t, self.f

      max_len_f = np_index_at_value(self.f, 10e3)

      # Update: using S_str_1 instead of 2
      #cluster_labels = self.s23_whistle_corpus.S_str_2
      cluster_labels = self.s23_whistle_corpus.S_str_1

      labels_count = pd.Series(cluster_labels).value_counts().head(10)
      labels_uniq = labels_count.index.tolist() # sorted from largest to smallest cluster
      print("Plotting below clusters")
      print(labels_count)

      #x_plt, y_plt = 10, 10
      x_plt, y_plt = 5, 5
      #x_plt, y_plt = 3, 3
      n_plt = x_plt * y_plt

      for j, l_ex in enumerate(labels_uniq):
          print("*"*50)
          print(f"Plot #{j}")
          feat_sub = self.s23_whistle_corpus[cluster_labels==l_ex]

          fig, ax = plt.subplots(x_plt, y_plt, figsize=(x_plt*2,y_plt), sharex=True, sharey=True)
          ax = ax.reshape((1,-1)).squeeze()
          for i in range(min(feat_sub.shape[0], n_plt)):
            wh_i = feat_sub.iloc[i]
            #ax[i].pcolormesh(t[:max_len_t], f[:max_len_f], wh_i.S_samelen, cmap="Greys") # , shading='gouraud'
            ax[i].plot(t[:wh_i.S_1d.shape[0]], wh_i.S_1d)
            ax[i].axhline(6000, color="red", alpha=.5)

            if i==0:
              ax[i].set_ylabel("Frequency (Hz, rebased)")
              ax[i].set_xlabel("Time (seconds, rebased)")
              ax[i].set_title(f"Cluster ID {l_ex}. n = {labels_count.loc[l_ex]}") # . (l1,l2,l3: {map_labels_dict[l_ex]})
              #ax[i].set_title(f"Cluster ID {l_ex}. n = {labels_count.loc[l_ex]}")

            ax[i].set_xlim(0, 1) # since max whistle is 1 second
            ax[i].set_ylim(0, f[max_len_f])

          fig.tight_layout()
          plt.show()


  def to_dict(self, all_checkpoints):
    d2 = {}

    if not hasattr(self, "fn_in"):
      print("Prior to 0.4.0, a missing fn_in was equivalent to a no-audio file which raised an AbortPipeline. Returning empty dict")
      return d2

    d2["fn_in"] = self.fn_in
    d2["channel"] = self.channel
    d2["t_sec_start"] = self.t_sec_start
    d2["t_sec_end"] = self.t_sec_end

    if not hasattr(self, "Sxx"):
      print("No Sxx field in spectrogram. Perhaps no audio in file or AbortPipeline returned during execution. Will not clone and return as is instead")
      return d2

    # if Sxx is available, then f,t,sec_all are as well
    d2["f"] = self.f
    d2["t"] = self.t
    d2["sec_all"] = self.sec_all

    # store as csr_matrix
    Sxx = scipy.sparse.csr_matrix(self.Sxx) if type(self.Sxx) == np.ndarray else self.Sxx
    d2["Sxx"] = Sxx

    # This is dict of csr_matrix, and it's ok not to convert back to numpy array here
    cp_nosave_l = [] if all_checkpoints else ["s11_original", "s11b_spectrogram__generic_func", "s12b_drop_bursts", "s12c_drop_bursts_hori_and_vert"]
    d2["Sxx_checkpoints"] = {k: v for k, v in self.Sxx_checkpoints.items() if k not in cp_nosave_l}

    if hasattr(self, "s22_connections"): d2["s22_connections"] = self.s22_connections.copy()
    if hasattr(self, "s23_connections"): d2["s23_connections"] = self.s23_connections.copy()
    if hasattr(self, "s23_whistle_corpus"): d2["s23_whistle_corpus"] = self.s23_whistle_corpus.copy()
    if hasattr(self, "s23_wsw"):
      # Just store ws_init, cl_all, and ws_final class members
      # d2["s23_wsw"] = self.s23_wsw
      d2["s23_wsw"] = self.s23_wsw.to_dict()

    return d2


  @classmethod
  def from_dict(cls, d2, verbose):
    self = cls()

    if not "fn_in" in d2:
      # Note similar return in to_dict
      print("Prior to 0.4.0, a missing fn_in was equivalent to a no-audio file which raised an AbortPipeline. Returning empty class")
      return self

    self.fn_in = d2["fn_in"]
    self.channel = d2["channel"]
    self.t_sec_start = d2["t_sec_start"]
    self.t_sec_end = d2["t_sec_end"]

    if "Sxx" not in d2:
      print("No Sxx field in spectrogram data dict. Perhaps no audio in file or AbortPipeline returned during execution. Will not clone and will return as is instead")
      return self

    self.f = d2["f"]
    self.t = d2["t"]
    self.sec_all = d2["sec_all"]

    # read as is, whether np.array or csr_matrix
    self.Sxx = d2["Sxx"]

    # This is dict of csr_matrix, and it's ok not to convert back to numpy array here
    self.Sxx_checkpoints = d2["Sxx_checkpoints"]

    # since the checkpoint s11_original does not get compressed well after pickling
    # re-read it from raw. This way, the saved pickle file can still get compressed to 20 MB for the 1h audio yt-q1 file whose pickle is 2 GB
    if os.path.exists(self.fn_in):
      if verbose:
        print(f"[WARNING] Input wav file exists. Reading it into Sxx_checkpoints['s11_original']: {self.fn_in}")
      _, _, self.Sxx_checkpoints["s11_original"], _ = self._s11_read_wav_no_save_members(self.fn_in, self.channel, self.t_sec_start, self.t_sec_end)

    if "s22_connections" in d2:    self.s22_connections = d2["s22_connections"].copy()
    if "s23_connections" in d2:    self.s23_connections = d2["s23_connections"].copy()
    if "s23_whistle_corpus" in d2: self.s23_whistle_corpus = d2["s23_whistle_corpus"].copy()
    if "s23_wsw" in d2:
      # Update: since only storing few field members, need to extract them here
      #self.s23_wsw = d2["s23_wsw"]
      self.s23_wsw = WhistleSegWrap.from_dict(d2["s23_wsw"], self)

    return self


  def export_corpus_csv(self, fn_csv):
    # skip some fields
    cols_all = self.s23_whistle_corpus.columns
    cols_skip = ["label_whistle", "S", "S_samelen", "S_zeroed", "S_1d", "S_gradient", "S_str_2", "label_cluster"]
    cols_save = [x for x in cols_all if x not in cols_skip]

    # bring S_str_1 first since it's the signature of the whistle
    cols_save = ["S_str_1"] + [x for x in cols_save if x not in ["S_str_1"]]

    # round some fields
    df_csv = self.s23_whistle_corpus[cols_save].copy()
    fx_round = ["t_sec_min", "t_sec_max", "f_hz_min", "f_hz_max", "t_len_sec", "f_len_hz"]
    for fx_i in fx_round: df_csv[fx_i] = df_csv[fx_i].round(1)

    # sort
    df_csv.sort_values(["t_idx_min", "f_idx_min"], inplace=True)

    # export
    df_csv.to_csv(fn_csv, index=False)


  def plot_checkpoints(self, t_sec_start, use_imshow=False):
    """
    t_sec_start: seconds at which to draw the 5-sec plot
    use_imshow: False to use greyscale plots with t in seconds on x axis and f in hz on y axis
        True to use imshow with colored colormap and colorbar showing spectrogram value
    """
    # plot window of 5 seconds
    t_sec_end = t_sec_start + 5
    t_idx_start = np_index_at_value(self.t, t_sec_start)
    t_idx_end   = np_index_at_value(self.t, t_sec_end)

    #t_i = self.t[t_idx_start:t_idx_end]
    #f_i = self.f

    my_plot = lambda t_sec_start, S_i:  self.plot_range(t_sec_start, t_sec_start, True, None, None, [], None, S_i)

    cpk_l = sorted(list(self.Sxx_checkpoints.keys()))
    for cpk_i in cpk_l:
      S_i = self.Sxx_checkpoints[cpk_i]
      print(cpk_i)
      if use_imshow:
        S_i = S_i[:, t_idx_start:t_idx_end]
        if type(S_i) == csr_matrix: S_i = S_i.toarray()
        my_imshow(S_i, cpk_i)
      else:
        my_plot(t_sec_start, S_i)

    # latest value
    S_i = self.Sxx
    if use_imshow: S_i = S_i[:, t_idx_start:t_idx_end]
    if type(S_i) == csr_matrix: S_i = S_i.toarray()
    print("latest")

    if use_imshow:
      my_imshow(S_i, "latest")
    else:
      my_plot(t_sec_start, S_i)
