import glob
from tqdm.auto import tqdm
import os
from ..core.audiofile import AudioFile
import numpy as np
import scipy
import tarfile
import pandas as pd
from matplotlib import pyplot as plt
import datetime as dt
import pickle
from ..utils import to_dict_dolphicom, from_dict_dolphicom
import time
from .s3_tarfile import S3TarFile
import youtube_dl


def S_zeroed_to_csr(Szi):
  assert type(Szi) == np.ndarray
  return scipy.sparse.csr_matrix(Szi)


def my_replace(x):
  x = os.path.basename(x).replace("-dolphicom.pkl.tgz", "")
  # bottlenose
  if x=="Tt-B14h43m26s17apr2007y": return "Tt-070417-144326"
  if x=="Tt-B17h34m17s22apr2007y": return "Tt-070422-173417"
  x = x.replace("Qx-Tt-SC03-TAT03", "Qx_Tt_SC03_TAT03")
  x = x.replace("QX-Tt-CC0604-TAT11", "QX_Tt_CC0604_TAT11")
  x = x.replace("Qx-Tt-SCI0608-N1", "Qx_Tt_SCI0608_N1")
  x = x.replace("Qx-Tt-SCI0608-Ziph", "Qx_Tt_SCI0608_Ziph")
  x = x.replace("_4-1", "_41")
  x = x.replace("_4-2", "_42")
  return x


class YtdlCacheTarDateExtractor:
  def extract_yt_uid_from_audio_filenames_in_s3_tar(self, s3_bucket, s3_tarpath):
    # Update: instead of full download of tar for the filenames,
    # use the tar_s3_members to get file names without download
    #!aws s3 cp s3://dolphicom/ytq1_250vids__audiofiles_20210714.tar .
    #ydl_fn = sorted(glob.glob("yt_vids/*"))

    #s3_bucket, s3_tarpath = "dolphicom", "ytq1_250vids__audiofiles_20210714.tar"

    tf = S3TarFile(s3_bucket, s3_tarpath, False)
    tsm_gen = tf.getmembers()
    ydl_fn = list(tqdm(tsm_gen))
    ydl_fn = [x.name for x in ydl_fn]
    #len(ydl_fn), ydl_fn[:3]

    #  FIXME: I could have used the yt.txt instead of getting filenames etc
    drop_fn = [
      "yt.txt",
      'yt_cache/youtube-sigfuncs',
     'yt_cache/youtube-sigfuncs/js_e5748921_107.json',
     'yt_cache/youtube-sigfuncs/js_e5748921_103.json',
    ]
    ydl_fn = [x for x in ydl_fn if x not in drop_fn]
    #len(ydl_fn), ydl_fn[:10]

    self.ydl_fn = ydl_fn

    self.ydl_uid = [os.path.splitext(x)[0][-11:] for x in self.ydl_fn]
    #ydl_uid[:3]

  def extract_yt_uid_from_cachelist_in_s3_tar(self, s3_bucket, s3_tarpath):
    """
    Alternative to extract_yt_uid_from_audio_filenames_in_s3_tar
    It's faster because yt.txt is at the beginning of the tar and it contains all the UID's
    cachelist in the function name refers to the yt.txt file
    """
    tf = S3TarFile(s3_bucket, s3_tarpath, False)
    cache_fn = "yt.txt"
    if not os.path.exists(cache_fn):
      tarinfo = tf.getmember(cache_fn)
      tf.extractall(path=".", members=[tarinfo])

    cache_df = pd.read_csv(cache_fn, sep=" ", header=None, names=["ydl_src", "ydl_uid"])
    assert set(cache_df.ydl_src.tolist())==set(["youtube"]) # all from youtube. Can lift this condition later?
    self.ydl_uid = cache_df.ydl_uid.tolist()


  def download_upload_date(self):
    # some UIDs which fail to get date in API JSON and which I ran on my local machine to avoid the http 429 error
    external_dates = {
      '2gvgkHSyKFE': '20130122', 'HiTwhYoCm8U': '20151031', 'XGaHcKcQO_E': '20191122',
      'PokWw2WtMY4': '20120513', 'BFRdq_BmSIw': '20170207', 'cwjqRsqHe8M': '20130630',
      'Bj4VPeA7SJo': '20200621', 'jOPLHPKqTLQ': '20150823', 'U1NXK0burQk': '20190714',
      'XlTn5V9e9OY': '20200826', 'MxlmxEakxPc': '20170207'
    }

    self.ydl_dt = []
    print(">>> CAN IGNORE YOUTUBE HTTP 429 RESPONSE SINCE WEBPAGE FAILURE WILL FALL BACK TO API JSON")
    print(">>> IT WILL STILL AFFECT SOME VIDEOS WHOSE DATE IS NOT AVAILABLE IN THE API JSON")
    with youtube_dl.YoutubeDL({}) as ydl:
      for i, uid_i in enumerate(self.ydl_uid):
        #if i<17: continue
        print(f"len(ydl_dt)={len(self.ydl_dt)}, len(ydl_uid)={len(self.ydl_uid)}")

        if uid_i in external_dates:
          self.ydl_dt.append(external_dates[uid_i])
          continue

        time.sleep(100/1000) # sleep n*100 ms to avoid youtube http 429: too many requests
        try:
          info_dict = ydl.extract_info(uid_i, download=False)
          self.ydl_dt.append(info_dict["upload_date"])
        except youtube_dl.DownloadError as e:
          print(f"Error for {uid_i}: {e}")
          self.ydl_dt.append(None)

    #len(self.ydl_dt), self.ydl_dt[:3]

  def make_map(self, dir_pkltgz):
    # create map from pkl.tgz filename to youtube upload date
    #dir_pkltgz = "t14.2-f1-v20210724a-ytq1_dolphicom_0.4.3-pkl_tgz"
    map_pkltgz_dt = {}
    assert len(self.ydl_uid)==len(self.ydl_dt)
    for uid_i, dt_i in zip(self.ydl_uid, self.ydl_dt):
      # Identify corresponding file by checking if it contains the UID
      ptgz_i = glob.glob(f"{dir_pkltgz}/*{uid_i}*")

      if len(ptgz_i)==0:
        print(f"[WARNING] Couldn't find file in '{dir_pkltgz}' for UID: {uid_i}")
        continue

      ptgz_i = os.path.basename(ptgz_i[0])
      ptgz_i = ptgz_i.replace("-dolphicom.pkl.tgz", "") # pre v0.4.3 but keeping anyway
      ptgz_i = ptgz_i.replace("-dolphicom.tgz", "")
      map_pkltgz_dt[ptgz_i] = {"file_in": ptgz_i, "file_uid": uid_i, "file_dt": dt_i}

    ydl_df_fn_dt = pd.DataFrame(map_pkltgz_dt.values())
    #ydl_df_fn_dt.head()
    self.ydl_df_fn_dt = ydl_df_fn_dt


class ReadDir:
  """
  Helper class for DirTgz.read_dir
  """
  def __init__(self, dir_pkltgz):
    self.dir_pkltgz = dir_pkltgz
    self.wc_all = {}
    self.fmeta_all = {}
    # keep as class member in case this needs to be changed during usage
    self.fx_drop = ['S', 'S_samelen', 'S_gradient', 'S_str_2', 'label_cluster']


  def read_dir(self):
    if self.dir_pkltgz is None: raise Exception("Missing class member dir_pkltgz => cannot run read_dir")

    # Update: no longer using the .pkl.tgz suffix since the .tgz contains more than just the pkl file, eg spectrograms and thumbnails
    #glob_ptgz = sorted(glob.glob(f"{self.dir_pkltgz}/*.pkl.tgz"))
    glob_ptgz = sorted(glob.glob(f"{self.dir_pkltgz}/*.tgz"))
    #glob_ptgz = glob_ptgz[:10] # FIXME
    with tqdm(glob_ptgz) as pbar:
      for full_ptgz in pbar:
        if full_ptgz in self.wc_all: continue
        base_ptgz = os.path.basename(full_ptgz)
        pbar.set_postfix_str(base_ptgz)
        res = self.read_file(full_ptgz)
        if res is None:
          self.wc_all[full_ptgz] = None
          self.fmeta_all[full_ptgz] = None
          continue
  
        wc_i, fmeta_i = res
        self.wc_all[full_ptgz] = wc_i
        self.fmeta_all[full_ptgz] = fmeta_i


  def read_file(self, full_ptgz):
      base_ptgz = os.path.basename(full_ptgz)
      #print(base_ptgz)

      # Update: instead of being semi-hard-coded, get from tar_members
      #base_pkl = base_ptgz.replace(".pkl.tgz", ".pkl")
      #full_pkl = f"{self.dir_pkltgz}/{base_pkl}"

      # Update: use python tarfile to get filenames and extract
      #!tar -xzf "$full_ptgz"

      tar_members = []
      with tarfile.open(full_ptgz, "r:gz") as tar:
        tar_members = tar.getmembers()

        full_pkl = [x.name for x in tar_members if x.name.endswith(".pkl")]
        if len(full_pkl)==0: return
        full_pkl = full_pkl[0]

        # tar.extract_all()
        tar.extract(full_pkl)

      # temporary bugfix since different folder name before tarring
      #!mv "t14.2-f1-v20210724a-moby_5th_bottlenose-dolphicom_0.4.3-pkl_tgz"/*.pkl \
      #    "t14.2-f3-v20210724a-moby_5th_bottlenose-dolphicom_0.4.3-pkl_tgz/"

      assert os.path.exists(full_pkl)

      p = AudioFile.from_pickle(full_pkl, False)
      if not hasattr(p.sp_filt, "s23_whistle_corpus"):
        os.remove(full_pkl)
        return

      # drop some fields to save ram
      for fx_i in self.fx_drop:
        if fx_i in p.sp_filt.s23_whistle_corpus.columns:
          del p.sp_filt.s23_whistle_corpus[fx_i]

      # convert S_zeroed to csr_matrix to save ram
      if "S_zeroed" in p.sp_filt.s23_whistle_corpus.columns:
        p.sp_filt.s23_whistle_corpus["S_zeroed"] = p.sp_filt.s23_whistle_corpus["S_zeroed"].apply(S_zeroed_to_csr)

      # append the filename
      p.sp_filt.s23_whistle_corpus["file_in"] = os.path.basename(base_ptgz) #full_pkl)

      # gather the min/max of time of the audio file
      fmeta_i = {
          "file_in": os.path.basename(base_ptgz), #full_pkl),
          "file_t_min": p.sp_filt.t.min(),
          "file_t_max": p.sp_filt.t.max(),
        }

      # clean up
      os.remove(full_pkl)

      # gather in dict
      wc_i = p.sp_filt.s23_whistle_corpus

      return wc_i, fmeta_i


  def dict_to_df(self):
    # convert to pandas dataframes
    self.wc_df = pd.concat([v for k,v in self.wc_all.items() if v is not None and "S_1d" in v.columns])

    # fmeta_all to dataframe
    # dict({'file_parent':k}, **v)
    # ["file_parent", "file_in"]
    self.fmeta_df = pd.DataFrame([v for k,v in self.fmeta_all.items() if v is not None]).sort_values("file_in")
    assert self.fmeta_df["file_t_min"].max() < .1


  def extract_file_dt__dclde(self):
    # file_parent (instead of file_in)
    fn_l1 = self.fmeta_df.file_in.apply(lambda x: dt.datetime.strptime(' '.join(os.path.basename(x).split('_')[1:3]), "%Y%m%d %H%M%S")).tolist()
    return fn_l1


  def extract_file_dt__mobysound(self):
    """
    Currently only applies to filenames of mobysound.org, 5th conference, melon and bottlenose dolphin files.
    """
    fmeta_df = self.fmeta_df

    fn_l1 = fmeta_df.file_in.apply(my_replace).tolist()
    fn_l1 = [x.split("-") for x in fn_l1]

    # FIXME: dropping the underscore suffix might be getting multiple files that run at the same time
    fn_l1 = [dt.datetime.strptime(f"{x[1]} {x[2].split('_')[0]}", "%y%m%d %H%M%S") for x in fn_l1]
    return fn_l1


  def extract_file_dt__youtube(self, s3_bucket, s3_tarpath, dir_pkltgz):
    """
    Since downloaded youtube files don't have a recording date in their filename, extract the upload_date from youtube
    Note about s3_bucket and s3_tarpath arguments:
      To get the youtube UIDs that were downloaded for the DirTgz, I am using the tar containing a copy of the youtube audio files.
      Alternatively, I could have extracted from the dolphicom.pipeline pkl file from AudioFile.file_in.

    s3_bucket: aws s3 bucket that contains an archive of the youtube audio filenames, with the UID.
    s3_tarpath: aws s3 path to the tar file containing a copy of the youtube audio files.
    """
    self.yafd = YtdlCacheTarDateExtractor()
    # Update: use the yt.txt cache file instead of extracting filenames. It is equivalent and saves time.
    # self.yafd.extract_yt_uid_from_audio_filenames_in_s3_tar("dolphicom", "ytq1_250vids__audiofiles_20210714.tar")
    #self.yafd.extract_yt_uid_from_audio_filenames_in_s3_tar(s3_bucket, s3_tarpath)
    self.yafd.extract_yt_uid_from_cachelist_in_s3_tar(s3_bucket, s3_tarpath)

    self.yafd.download_upload_date()

    # create map from filenames in pkl_tgz folder to youtube upload date
    #dir_pkltgz = "t14.2-f1-v20210724a-ytq1_dolphicom_0.4.3-pkl_tgz"
    self.yafd.make_map(dir_pkltgz)

    # merge with fmeta_df
    # This ensures that file_dt is in the same order as file_in
    fmeta_df = self.fmeta_df[["file_in"]].copy()
    fmeta_df["file_in"] = fmeta_df.file_in.str.replace("-dolphicom.pkl", "")
    fmeta_df = fmeta_df.merge(self.yafd.ydl_df_fn_dt, how="left", on="file_in")
    fmeta_df["file_dt"] = pd.to_datetime(fmeta_df["file_dt"], format="%Y%m%d")
    return fmeta_df.file_dt.tolist()


  def append_file_dt(self, file_dt):
    """
    file_dt: a list of dates corresponding to each audio file in fmeta_df.file_in. Must match the order. Can be returned by DirTgz.extract_file_dt__youtube or DirTgz.extract_file_dt__mobysound
    """
    assert type(file_dt)==list
    fmeta_df = self.fmeta_df

    fmeta_df["file_dt"] = file_dt

    # calculate file_dt_end from file_dt and file_t_max (proxy to length of file)
    fmeta_df["file_dt_end"] = fmeta_df["file_dt"] + pd.to_timedelta(fmeta_df["file_t_max"].round(0), unit='s')

    self.fmeta_df = fmeta_df


  def merge_wc_fmeta(self):
    wc_df = self.wc_df

    print("before", wc_df.shape)
    wc_df = wc_df.merge(self.fmeta_df, how="left", on="file_in")
    print("after", wc_df.shape)

    wc_df = wc_df.sort_values(["file_dt", "t_sec_min", "t_sec_max", "f_hz_min", "f_hz_max"], ascending=True).reset_index(drop=True)
    # wc_df["file_dt"].dtype # dtype('<M8[ns]')
    self.wc_df = wc_df


  def append_overlap(self):
    """
    FIXME: in the tracks calculation, all I use is overlap==0 or !=0
    In here, I calculate the overlap as an integer.
    It is useful for the statistics shown at the end of this function,
    but it is not useful for further computations.
    Consequently, since the for loop takes a bit of time (at least as long
    as the for loop in the tracks_calculate function, which is necessary
    for the csv export, this function may be skipped altogether if
    the files are too long causing this for loop to be too slow.
    """
    wc_df = self.wc_df
    overlap_prev = [0] * wc_df.shape[0]
    my_comp = lambda w0, w1: (w1.t_sec_min <= w0.t_sec_min <= w1.t_sec_max)

    n_max = 6
    for i in tqdm(range(n_max, wc_df.shape[0])):
      w6 = wc_df.iloc[i-n_max]
      w5 = wc_df.iloc[i-n_max+1]
      w4 = wc_df.iloc[i-n_max+2]
      w3 = wc_df.iloc[i-n_max+3]
      w2 = wc_df.iloc[i-n_max+4]
      w1 = wc_df.iloc[i-n_max+5]
      w0 = wc_df.iloc[i  ]
      c01 = my_comp(w0, w1)
      c02 = my_comp(w0, w2)
      c03 = my_comp(w0, w3)
      c04 = my_comp(w0, w4)
      c05 = my_comp(w0, w5)
      c06 = my_comp(w0, w6)
      overlap_prev[i] = int(c01) + int(c02) + int(c03) + int(c04) + int(c05) + int(c06)

    # save in dataframe
    self.wc_df["overlap_prev"] = overlap_prev

    print(f"max(overlap_prev) = {max(overlap_prev)}")
    print("overlap_prev statistics")
    print(pd.Series(overlap_prev).value_counts())



class DirTgz:
  """
  Read a directory of .tgz files that were exported, tar'd, and gzip'd from AudioFile.to_pickle's pkl file
  Note: only deals with flat directory ATM. TODO Support nested dir structure.

  Arguments:
    dir_pkltgz: string. Path to the directory containing the tgz files
    description: string. Description of the dataset. Used in the plot title.
    date_extractor: string: mobysound|youtube. Used to extract the recording date of the file. If mobysound, extract from the filename. If youtube, extract from the upload date field in the youtube json API.

  Example:

    from dolphicom.io.dir_tgz import DirTgz
    dtgz = DirTgz(dir_pkltgz="path/to/folder/", description="description of folder", date_extractor="mobysound")

    # read all dir
    dtgz.read_dir()

    # plot file lengths and recording timestamps
    dtgz.plot_fmeta_filelen()
    dtgz.plot_file_dt()

    # export the whistles as a csv of different tracks, such that each track has no overlays in whistles
    dtgz.tracks_calculate()
    dtgz.tracks_plot(ts_start="2021-01-01 00:00:00", ts_end="2021-01-01 00:00:05")
    dtgz.tracks_to_csv(fn_csv="tracks.csv")

    # save to pickle file and load from pickle file
    dtgz.to_pickle(fn_pkl="dolphicom_dirtgz.pkl")
    dtgz = DirTgz.from_pickle(fn_pkl="dolphicom_dirtgz.pkl")

    # dataframes with main data
    wc_df, fmeta_df = dtgz.wc_df, dtgz.fmeta_df
  """
  def __init__(self, dir_pkltgz, description, date_extractor):
    self.dir_pkltgz = dir_pkltgz
    self.description = description
    self.date_extractor = date_extractor
    self.rd_helper = ReadDir(self.dir_pkltgz)


  def read_dir(self):
    self.rd_helper.read_dir() # will raise Exception if self.dir_pkltgz is None
    self.rd_helper.dict_to_df()
    if self.date_extractor=="mobysound":
      file_dt = self.rd_helper.extract_file_dt__mobysound()
    elif self.date_extractor=="youtube":
      # FIXME
      file_dt = self.rd_helper.extract_file_dt__youtube(s3_bucket=..., s3_tarpath=..., dir_pkltgz=self.dir_pkltgz)
    elif self.date_extractor=="DCLDE":
      file_dt = self.rd_helper.extract_file_dt__dclde()
    else:
      raise ValueError(f"Unsupported date_extractor={date_extractor}. Valid values: mobysound, youtube")

    # works with mobysound.org, 5th conference, bottlenose and melon-headed dolphins
    self.rd_helper.append_file_dt(file_dt)

    # calculate number of overlapping whistles
    self.rd_helper.merge_wc_fmeta()
    self.rd_helper.append_overlap()


  def plot_fmeta_filelen(self):
    self.rd_helper.fmeta_df.file_t_max.sort_values().reset_index(drop=True).plot(marker="|")
    plt.title(f"Audio file durations of \n {self.description}")
    plt.ylabel("seconds")
    plt.xlabel("Rank of audio files by length, sorted from shortest to longest")
    plt.show()


  def plot_file_dt(self):
    fmeta_df = self.rd_helper.fmeta_df
    plt.figure(figsize=(20,1))
    #plt.plot(wc_df.file_dt.values, np.zeros(shape=wc_df.shape[0]), color="blue")
    plt.plot(fmeta_df.file_dt.values, np.zeros(shape=fmeta_df.shape[0]), color="blue")
    plt.scatter(fmeta_df.file_dt.values, np.zeros(shape=fmeta_df.shape[0]), marker="|", color="green")
    # whistles and recording times are almost fully overlaid, so whistles won't show
    #plt.scatter(wc_df.file_dt.values, np.zeros(shape=wc_df.shape[0]), marker="|", color="red")
    plt.title(f"Timeline of recording times in \n {self.description}")
    plt.show()



  def tracks_calculate(self):
    """
    Calculate track number, which is different than the number of overlaps
    Examples
    Overlap Track Drawing

    0  0               ---------------
    0  0 ----------

    0  0                         -------------
    1  1        --------------
    0  0 -------------

    0  0                     -------------
    1  2            -------
    1  1       -------
    0  0    -----

    2 2       ----------
    1 1    ---------
    0 0 --------

    2 3          ----------
    2 2       ----------
    1 1    ---------
    0 0 --------
    """
    wc_df = self.rd_helper.wc_df.copy()

    wc_df["whistle_dt_min"] = wc_df["file_dt"] + (wc_df["t_sec_min"]*1000).astype("timedelta64[ms]")
    wc_df["whistle_dt_max"] = wc_df["file_dt"] + (wc_df["t_sec_max"]*1000).astype("timedelta64[ms]")

    # construct dataframe of seconds and tracks such that
    # each track doesn't have any overlapping whistles, thus doesn't lose any whistle parts
    # The algo below selects the smallest track number whose last whistle doesn't overlap with the current whistle
    df_l = []
    track_last_ts = []
    for i in tqdm(range(wc_df.shape[0])):
      wh_i = wc_df.iloc[i]

      # init
      if len(track_last_ts)==0: track_last_ts.append(wh_i["whistle_dt_min"] - pd.to_timedelta(1, "s"))

      # find the smallest track that ends before the new whistle starts
      found_track = False
      for track_number in range(len(track_last_ts)):
        if wh_i["whistle_dt_min"] > track_last_ts[track_number]:
          found_track = True
          break

      if not found_track:
        # need a new track
        track_number = len(track_last_ts)
        track_last_ts.append(wh_i["whistle_dt_max"])
      else:
        # update the last ts on the track
        track_last_ts[track_number] = wh_i["whistle_dt_max"]

      #print(f"i={i}, t0={wh_i['whistle_dt_min']}, t1={wh_i['whistle_dt_max']}, track_number={track_number}, overlap_prev={wh_i.overlap_prev}") #", track_last_ts={track_last_ts}")

      t_i = pd.date_range(wh_i["whistle_dt_min"], wh_i["whistle_dt_max"], periods=wh_i["t_len_idx"])
      df_i = pd.DataFrame({f"f_hz_{track_number}": wh_i.S_1d + wh_i.f_hz_min, "t_sec": t_i})
      #df_i.shape
      df_l.append(df_i)

    #
    print(f"Number of tracks used: {len(track_last_ts)}")

    # few plots for verification
    for i in range(5):
      df_l[i].set_index("t_sec").plot()
      plt.show()

    # FIXME this conat takes some time. Maybe store lists of dict instead and just create dataframe here
    # (instead of create dataframes in list then concat here)
    df_a = pd.concat(df_l, axis=0)

    cols_f = sorted([x for x in df_a.columns if x.startswith("f_hz_")], key=lambda y: int(y.replace("f_hz_","")))
    self.tracks_cols_f = cols_f

    # round frequencies
    # also for the sake of the csv
    df_a[cols_f] = df_a[cols_f].round(0)

    # round to nearest milli-second
    df_a["t_sec"] = df_a["t_sec"].dt.round('ms')

    self.tracks_df = df_a


  def tracks_plot(self, ts_start, ts_end):
    """
    Example:
    dtgz.tracks_plot("2007-09-28 03:31:14", "2007-09-28 03:31:19")
    """
    plt.figure(figsize=(20,3))

    # bottlenose
    # df_plt = df_a.set_index("t_sec").loc["2006-08-14 12:15:39":"2006-08-14 12:15:44"]

    # melon
    #df_plt = self.tracks_df.set_index("t_sec").loc["2007-09-28 03:31:14":"2007-09-28 03:31:19"]

    df_plt = self.tracks_df.set_index("t_sec").loc[ts_start:ts_end]

    for i in range(df_plt.values.shape[1]):
      plt.scatter(df_plt.index.values, df_plt.values[:,i], marker="|", label=f"Track {i}", s=1)

    plt.title(f"{self.description} \n {ts_start}:{ts_end}")
    plt.legend()
    plt.show()


  def tracks_to_csv(self, fn_csv):
    # save to csv to share
    #fn_csv = "t13-f1-v20210721a-moby_5th_melon.csv"
    #fn_csv = "t13-f1-v20210723a-moby_5th_melon.csv"
    #fn_csv = "t13-f2-v20210723a-moby_5th_bottlenose.csv"

    # copy from upstream
    cols_l = ['t_sec'] + self.tracks_cols_f
    self.tracks_df[cols_l].to_csv(fn_csv, index=False)

    # drop useless .0
    #!sed -i "s/\.0,/,/g" "$fn_csv"
    #!sed "s/\.0,/,/g" "$fn_csv" | sed "s/\.0$//g"

    # slurp file and replace ".0," with ","
    with open(fn_csv, "r") as f1:
      x = f1.read().replace(".0,", ",").replace(".0\n","")
      with open(fn_csv, "w") as f3:
        f3.write(x)


  def to_pickle(self, fn_pkl):
    #fn_pkl = "t13-f3-v20210727a-notebook_variables-moby_5th_bottlenose-dolphicom_v043.pkl"
    #fn_pkl = "t13-f4-v20210727a-notebook_variables-moby_5th_melon-dolphicom_v043.pkl"
    d1 = self.to_dict()
    d2 = to_dict_dolphicom("io.dir_tgz.DirTgz", d1)
    with open(fn_pkl, "wb") as f:
      pickle.dump(d2, f)


  def to_dict(self):
    d1 = {
      "dir_pkltgz": self.dir_pkltgz,
      "description": self.description,
      "date_extractor": self.date_extractor,

      "wc_all": self.rd_helper.wc_all,
      "fmeta_all": self.rd_helper.fmeta_all,
      "fx_drop": self.rd_helper.fx_drop,
      "wc_df": self.rd_helper.wc_df,
      "fmeta_df": self.rd_helper.fmeta_df,

      "tracks_cols_f": self.tracks_cols_f,
      "tracks_df": self.tracks_df,
    }
    return d1


  @classmethod
  def from_pickle(cls, fn_pkl):
    with open(fn_pkl, "rb") as f:
      d2 = pickle.load(f)

    # do not use: cls.__module__+'.'+cls.__name__ so that I can freely refactor class names without breaking file loading
    d1 = from_dict_dolphicom(d2, "io.dir_tgz.DirTgz", False)
    return cls.from_dict(d1)


  @classmethod
  def from_dict(cls, d1):
    if "dir_pkltgz" not in d1:
      raise Exception("No dir_pkltgz key in dict. Perhaps pkl file was not a DirTgz file?")

    self = cls(d1["dir_pkltgz"], d1["description"], d1["date_extractor"])

    self.rd_helper.wc_all = d1["wc_all"]
    self.rd_helper.fmeta_all = d1["fmeta_all"]
    self.rd_helper.fx_drop = d1["fx_drop"]
    self.rd_helper.wc_df = d1["wc_df"]
    self.rd_helper.fmeta_df = d1["fmeta_df"]

    self.tracks_cols_f = d1["tracks_cols_f"]
    self.tracks_df = d1["tracks_df"]
    return self
