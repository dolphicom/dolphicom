import boto3
import io
import tarfile

class S3File(io.BytesIO):
    def __init__(self, bucket_name, key_name, s3client, verbose):
        super().__init__()
        self.bucket_name = bucket_name
        self.key_name = key_name
        self.s3client = s3client
        self.offset = 0
        self.total_download = 0
        self.verbose = verbose

    def close(self):
        return

    def read(self, size):
        self.total_download += size
        if self.verbose: print('read: offset = {} bytes, size = {} bytes, total download = {} kiB'.format(self.offset, size, self.total_download//1000))

        start = self.offset
        end = self.offset + size - 1
        try:
            s3_object = self.s3client.get_object(Bucket=self.bucket_name, Key=self.key_name, Range="bytes=%d-%d" % (start, end))
        except:
            return bytearray()
        self.offset = self.offset + size
        result = s3_object['Body'].read()
        return result

    def seek(self, offset, whence=0):
        if whence == 0:
            if self.verbose: print('seek: offset {} -> {} (diff = {} kiB)'.format(self.offset, offset, (offset-self.offset)//1000))
            self.offset = offset

    def tell(self):
        return self.offset


class S3ArchiveFile:
  def __init__(self, bucket_name, file_name, verbose):
    self.bucket_name = bucket_name
    self.file_name = file_name
    self.verbose = verbose

  def print_list(self):
    raise Exception("Should implement by derived class")
  
  def extractall_str(self, path, members_str):
    raise Exception("Should implement by derived class")



class S3TarFile(S3ArchiveFile):
  """
  Like TarFile, but for s3 objects.
  Currently just for TarFile.getmembers, but without downloading full file
  Copied originally from
    How to list files inside Tar in aws s3 without downloading it
    https://stackoverflow.com/questions/56086604/how-to-list-files-inside-tar-in-aws-s3-without-downloading-it
  and modified as needed.

  Note that for a 1GB tar file with 321 audio files, this consumed 220 KB and took 2 minutes on colab as of 2021-07-28.
  OTOH a full download took 16 seconds, but consumed 1 GB.
  """
  def __init__(self, bucket_name, file_name, verbose):
    """
    No need for open function and constructor separately, because in TarFile, open is just a shortcut to the constructor
    Ref: https://github.com/python/cpython/blob/3.9/Lib/tarfile.py#L1552

    bucket_name: name of bucket
    file_name: path to tar
    verbose: True for displaying details, False otherwise

    Example:
    # use without "with S3TarFile(...) as tarf: ..." to indicate that we're holding on to the cached data
    tarf = S3TarFile("bucket", "file", True)

    # next()
    print(tarf.next()) # cache miss, one entry
    print(tarf.next()) # cache miss, another entry

    # getmembers()
    from tqdm.auto import tqdm
    members = list(tqdm(tarf.getmembers())) # first 2 entries are cache hit, others are cache miss
    members = list(tqdm(tarf.getmembers())) # all are cache hit

    # extractall(...)
    tarf.extractall(members[3:5]) # extracts these 2 files from the tar without downloading the full tar

    # Usage from CLI like tarfile
    # https://docs.python.org/3/library/tarfile.html#command-line-interface
    python -m dolphicom.io.s3_tarfile -l bucket monty.tar
    python -m dolphicom.io.s3_tarfile -3 bucket monty.tar . file_in_tar.txt
    """
    #bucket_name, file_name = "bucket", "file.tar"
    super().__init__(bucket_name, file_name, verbose)
    self._members = {}

    s3client = boto3.client("s3")
    self.s3file = S3File(bucket_name, file_name, s3client, self.verbose)

    # FIXME should I use "r:*" like in tarfile.main ?
    # https://github.com/python/cpython/blob/3.9/Lib/tarfile.py#L2528
    self.tarf = tarfile.open(mode="r", fileobj=self.s3file)

    self.memgen = self.getmembers()



  def next(self):
    return next(self.memgen)


  def getmembers(self):
    """
    Like TarFile.getmembers, but returns generator instead
    """
    return self._getmembers(stop_at=None)


  def _getmembers(self, stop_at):
    # return what is cached first, as a generator to be consistent with the cash-miss case
    for k, tarinfo in self._members.items():
      yield tarinfo
      if stop_at is not None:
        if tarinfo.name==stop_at: return

    # cache-miss, need to read file
    tarinfo = 1 # dummy
    while tarinfo is not None:
      tarinfo = self.tarf.next()
      if tarinfo is None: break
      if self.verbose: print(tarinfo.name)
      #if tarinfo.name == name_search:
      #  break
      self._members[tarinfo.name] = tarinfo
      yield tarinfo
      if stop_at is not None:
        if tarinfo.name==stop_at: return


  def getmember(self, name):
    _ = list(self._getmembers(stop_at=name))
    if name not in self._members.keys():
      raise ValueError(f"File {name} not found")
    return self._members[name]


  def __exit__(self, type, value, traceback):
    """
    Wrapper of TarFile.__exit__
    https://github.com/python/cpython/blob/3.9/Lib/tarfile.py#L2462
    """
    self.tarf.__exit__(type, value, traceback)


  def extractall(self, path=".", members=None):
    assert type(path)==str
    if members is None:
      self.tarf.extractall(path=path)
      return

    assert type(members)==list

    # check all ok
    for mem_i in members:
      assert type(mem_i)==tarfile.TarInfo
      #if type(mem_i)==str: raise ValueError("extractall(string) not supported yet")
      if mem_i.name not in self._members.keys():
        raise ValueError(f"{mem_i.name} not found in archive")

    # actual extraction
    self.tarf.extractall(path=path, members=members)


  def print_list(self):
        bucket_name, src = self.bucket_name, self.file_name

        tarinfo = 1
        while tarinfo is not None:
          tarinfo = self.next()
          if tarinfo is None: break
          print(tarinfo.name)


  def extractall_str(self, path, members_str):
        """
        Similar to extractall, except that it takes a list of strings for members_str
        """
        for x in members_str: assert type(x)==str
        if (members_str is None) or (len(members_str)==0):
          # just call the original extractall function
          return self.extractall(path)

        # Instead of getting all, stop when all files identified
        # members_tarinfo = self.getmembers()

        # Iterate over files and stop when all found
        tarinfo = 1
        members_tarinfo = []
        file_notfound = set([x for x in members_str]) # copy the list
        while tarinfo is not None:
          tarinfo = self.next()
          if tarinfo is None: break
          if tarinfo.name in members_str:
            members_tarinfo.append(tarinfo)
            file_notfound.remove(tarinfo.name)
          if len(file_notfound) == 0: break

        # if still missing some files
        if len(file_notfound) > 0:
            file_notfound = sorted(list(file_notfound))
            print("Files not found in archive:")
            for f in file_notfound: print(f)
            raise Exception("Some files not found in archive. Aborting")

        # no need since members == tarinfo_only
        # tarinfo_only = [x for x in members if x.name in file_only]
        #self.extractall(path=path, members=tarinfo_only)
        self.extractall(path=path, members=members_tarinfo)
        if self.verbose:
            if path == '.':
                msg = '{!r} file is extracted.'.format(src)
            else:
                msg = ('{!r} file is extracted '
                       'into {!r} directory.').format(src, path)
            print(msg)
