from ..utils import gsutil_ls, os_touch
import re
from tqdm.auto import tqdm
import os


class GsDirStructMirror:
  """
  Helper class to the GalleryHtmlGenerator.
  Reads lists of files on google storage of spectrograms etc and mirrors dir structure locally
  but with empty files to avoid downloading everything again.
  Useful as a basis to generate the html files with proper links, but not have to download all the spectrogram files.

  Update 2021-10-24: rename variables for clarity, dirout_remote -> dirout_dirname_r (like os.path.dirname, eg "analyses"), dirout_local -> dirout_basename (like os.path.basename, eg "t1-v211024-...")
  These variable names match the content type.

  Example usage (before the GalleryHtmlGenerator.run call)
    bucket_name = "dolphicom"
    dirout_basename = "t17.1-f1-v20210824a-noaa_pifsc-pipeline"
    GsDirStructMirror.run(bucket_name, "analyses", dirout_basename, ".")
    GalleryHtmlGenerator.run(dir_lookup=f"./{dirout_basename}", html_title_prefix="dataset name")
  """

  # keyword to check for fn_dirout
  # Update: instead of complete_ok.txt, check for corpus.csv, which skips folders that do not have whistles
  kw_dirout = "corpus.csv"

  def __init__(self, bucket_name, dirout_dirname_r, dirout_basename, dirout_dirname_l):
    #dir_out = "t17.1-f1-v20210811a-noaa_pifsc-pipeline"
    #dir_out = "t17.1-f1-v20210819a-noaa_pifsc-pipeline"
    #dir_out = "t17.1-f1-v20210824a-noaa_pifsc-pipeline"
    self.bucket_name = bucket_name
    #self.dirout_dirname_r = dirout_dirname_r
    #self.dirout_basename = dirout_basename
    #self.dirout_dirname_l = dirout_dirname_l
    
    self.dirout_full_r = os.path.join(dirout_dirname_r, dirout_basename)
    self.dirout_full_l = os.path.join(dirout_dirname_l, dirout_basename)


  def ls(self):
    # _complete_ok
    self.fn_dirout = gsutil_ls(self.bucket_name, prefix=self.dirout_full_r, suffix=self.kw_dirout)
    # Expect: 1705/1705_20170827_174622_524_ch_1/complete_ok.txt

    # ls_spectrogram_png__png(self):
    self.fn_spec_full = gsutil_ls(self.bucket_name, prefix=self.dirout_full_r, suffix_re=re.compile("spectrogram/*.png"))
    # Expect: 1705/1705_20170827_174922_524_ch_1/spectrogram_png/range_0000:04-0000:09-1_nohighlight.png

    # ls_spectrogram_thumbnails__png(self):
    self.fn_spec_thumb = [x.replace("spectrogram_png", "spectrogram_thumbnails") for x in self.fn_spec_full]
    # Expect: 1705/1705_20170827_174922_524_ch_1/spectrogram_thumbnails/range_0000:04-0000:09-1_nohighlight.png


  def create(self):
    #!ls "$dir_out"/1705/1705_20171101_031519_521_ch_1/spectrogram_png/range_0000:00-0000:05-1_nohighlight.png
    #!rm -rf "$dirout_full_l"/*

    # check that empty and create
    #assert len(glob.glob(f"{self.dirout_full_l}/*"))==0
    assert not os.path.exists(self.dirout_full_l)
    os.makedirs(self.dirout_full_l, exist_ok=True)

    # create dirs
    d1 = [x.replace(self.kw_dirout, "spectrogram_png") for x in self.fn_dirout]
    d2 = [x.replace(self.kw_dirout, "spectrogram_thumbnails") for x in self.fn_dirout]

    # !xargs -d '\n' mkdir -p -- < create_dirout.txt
    for d3 in tqdm(d1+d2, desc="create/mkdir"):
      os.makedirs(f"{self.dirout_full_l}/{d3}", exist_ok=True)


    # touch__ls_spec_both(self):
    #!xargs -d '\n' touch -- < ls_spec_both.txt
    for fi in tqdm(self.fn_spec_full+self.fn_spec_thumb, desc="create/touch"):
      fi = f"{self.dirout_full_l}/{fi}"
      os_touch(fi)


  def rmdir__dir_out__empty(self, raise_if_any):
    # rmdir empty directories
    # Below throws some errors, but works
    # Update: dolphicom no longer creates empty directories
    # Update: yes but this notebook will create empty folders for spectrogram_png and thumbnails for audio files that have no whistles detected
    # because these files will have a complete_ok.txt file in their gs folder, but no spectrogram_png dir, so need this again
    # Update: after changing from complete_ok.txt to corpus.csv, this function is no longer needed
    for root, dirs, files in os.walk(self.dirout_full_l):
      if len(os.listdir(root))==0:
        if raise_if_any: raise Exception(f"Found empty dir despite having fixed this issue. Check {root}")
        os.rmdir(root)
        # FIXME: need recursion up to grandparents too until self.dirout_full_l is hit
        parent = os.path.dirname(root)
        if len(os.listdir(parent))==0:
          if raise_if_any: raise Exception(f"Found empty dir despite having fixed this issue. Check {parent}")
          os.rmdir(parent)


  def count_png(self):
    # memory-intensive
    # n_png = len([file_i for root, dirs, file_l in os.walk(self.dirout_full_l) for file_i in file_l if file_i.endswith(".png")]

    # repeat without list
    n_png = 0
    for root, dirs, file_l in os.walk(self.dirout_full_l):
      for file_i in file_l:
        if file_i.endswith(".png"):
          n_png += 1

    print(f"Number of png files: {n_png}")
    # 168k -> 40k after filter false pos
    # 127k after dolphicom 0.5.2 which doesn't need filtering false positives
    #      Not sure why so far off from 40k


  @classmethod
  def run(cls, bucket_name, dirout_dirname_r, dirout_basename, dirout_dirname_l):
    x = cls(bucket_name, dirout_dirname_r, dirout_basename, dirout_dirname_l)
    x.ls()
    x.create()
    x.rmdir__dir_out__empty(raise_if_any=True)
    x.count_png()
