import glob
import dominate
from dominate.tags import *
from dominate.util import raw
import os
from .gs_dir_struct_mirror import GsDirStructMirror
import datetime as dt
from .. import dolphicom_version

class GalleryHtmlGenerator:
  """
  Generates html to browse png files on the internet, eg on aws s3
  Class based on code in t11.2
  Run this *AFTER* generating all the spectrograms of all the files.

  Example usage:

    # optional: mirror a dir structure from google storage without downloading the files
    GsDirStructMirror.run(bucket_name, dirout_remote, self.dir_lookup)

    GalleryHtmlGenerator.run(dir_lookup="t11.2-f1-v123-dataset-pipeline", html_title_prefix="dataset name")

  """
  def __init__(self, dir_lookup, html_title_prefix):
    # Important for ".replace()" call later so that the "/" is also dropped, so that os.path.join("..", "path") will generate "../path", otherwise os.path.join("..", "/path") generates "/path"
    if not dir_lookup.endswith("/"): dir_lookup += "/"
    self.dir_lookup = dir_lookup
    #self.html_title_prefix = "dolphicom-0.5.0 spectrogram gallery - t11-f1-v20210805a"
    self.html_title_prefix = html_title_prefix


  def calc_dir_root_l(self):
    #dir_lookup = "/content/t11-f1-v20210805a-moby_5th_eval-dolphicom_pipeline_v050/5th_DCL_Expanded_eval_data/"
    dir_lookup = self.dir_lookup

    # get root folders (the ones that contain parents of "spectrogram_png", "spectrogram_thumbnails", and "corpus.csv")
    glob_s = os.path.join(dir_lookup, "**/spectrogram_png")
    #glob_s = os.path.join(dir_lookup, "*/*/*/corpus.csv")
    #print(glob_s)
    dir_root_l = sorted(glob.glob(glob_s, recursive=True))
    dir_root_l = [os.path.dirname(x) for x in dir_root_l]
    #dir_root_l[:3]

    self.dir_root_l = dir_root_l


  def calc_fn_page_l(self):
    dir_lookup = self.dir_lookup
    dir_root_l = self.dir_root_l

    fn_page_l = []
    page_size = 100
    fn_page_i = []
    page_i_n = 0

    for dir_root_i in dir_root_l:
      root_thumb_l = sorted(glob.glob(os.path.join(dir_root_i, "spectrogram_thumbnails", "*.png")))
      root_full_l  = sorted(glob.glob(os.path.join(dir_root_i, "spectrogram_png", "*.png")))
      assert len(root_thumb_l) == len(root_full_l)
      rel_thumb_l = [x.replace(dir_lookup, "") for x in root_thumb_l]
      rel_full_l = [x.replace(dir_lookup, "") for x in root_full_l]
      dir_rel_i = dir_root_i.replace(dir_lookup, "")

      fn_zip_i = [{"thumb": x[0], "full": x[1]} for x in zip(rel_thumb_l, rel_full_l)]

      assert page_i_n < page_size
      page_remaining = page_size - page_i_n
      if len(fn_zip_i) <= page_remaining:
        fn_page_j = {"dir": dir_rel_i,
                    "images_n": len(fn_zip_i),
                    "images_l": fn_zip_i
                    }
        fn_page_i.append(fn_page_j)
        page_i_n += fn_page_j["images_n"]
        # flush
        if page_i_n>=page_size:
          fn_page_l.append(fn_page_i)
          fn_page_i = []
          page_i_n = 0
        continue

      if len(fn_zip_i) - page_remaining <= page_size:
        # can do in 2 slices
        range_i = [(0, page_remaining), (page_remaining, len(fn_zip_i))]
      else:
        # need 3 or more slices
        range_i = [0] + range(page_remaining, len(root_thumb_l), page_size)
        range_i = [(range_i[j-1], range_i[j]) for j in range(1,len(range_i))]

      for j0, j1 in range_i:
        fn_page_j = {"dir": dir_rel_i,
                    "images_n": len(fn_zip_i[j0:j1]),
                    "images_l": fn_zip_i[j0:j1]
                    }
        fn_page_i.append(fn_page_j)
        page_i_n += fn_page_j["images_n"]
        # flush
        if page_i_n>=page_size:
          fn_page_l.append(fn_page_i)
          fn_page_i = []
          page_i_n = 0

    # any unflushed entries
    if len(fn_page_i) > 0:
      fn_page_l.append(fn_page_i)
      fn_page_i = []
      page_i_n = 0

    #len(fn_page_l)

    self.fn_page_l = fn_page_l


  def generate_html_pages(self):
    # convert datastructure (list (page) of list (section) of list (100 images)) to html
    dir_lookup = self.dir_lookup
    fn_page_l = self.fn_page_l

    #!rm -rf "$dir_lookup"/html
    #!mkdir -p "$dir_lookup"/html
    os.makedirs(f"{dir_lookup}/html", exist_ok=True)

    for l1_i, l1_page in enumerate(fn_page_l):
      #print(f"page {l1_i+1}/{len(fn_page_l)}")
      i_zeros = "%03i"%(l1_i+1)
      i_prev_zeros =  "%03i"%(l1_i+1-1) if l1_i != 0 else None
      i_next_zeros =  "%03i"%(l1_i+1+1) if l1_i != (len(fn_page_l)+1) else None

      title=f'{self.html_title_prefix} - p {l1_i+1}/{len(fn_page_l)}'
      doc = dominate.document(title=title)
      with doc:
        with div():
          attr(cls='header')
          a("Home", "index.html")
          if i_prev_zeros is None: raw("<strike>Previous</strike>")
          else: a("Previous", f"p{i_prev_zeros}.html")
          if i_next_zeros is None: raw("<strike>Next</strike>")
          else: a("Next", f"p{i_next_zeros}.html")
        with div():
          attr(cls='body')
          for l2_section in l1_page:
            #print(f"\tDir: {l2_section['dir']}")
            h2(l2_section['dir'])
            for l3_image in l2_section["images_l"]:
              #print(f"\t\tImage: {l3_image['thumb']}")
              #print(f"\t\tImage: {l3_image['full']}")
              a(img(src=os.path.join("..", l3_image['thumb']),
                    title=os.path.basename(l3_image['thumb'])),
                href=os.path.join("..", l3_image['full'])
              )

      with open(f'{dir_lookup}/html/p{i_zeros}.html', 'w') as f:
        f.write(doc.render())


  def generate_html_index(self):
    # create index.html page
    # convert datastructure (list (page) of list (section) of list (100 images)) to html
    dir_lookup = self.dir_lookup

    if os.path.exists(f"{dir_lookup}/html/index.html"):
      os.remove(f"{dir_lookup}/html/index.html")
      
    page_l = sorted(glob.glob(f"{dir_lookup}/html/*.html"))

    title=f'{self.html_title_prefix} - Home'
    doc = dominate.document(title=title)
    with doc:
      with div():
        attr(cls='body')

        with div():
          h2("Dolphicom results")
          p("These are results of automatic recognition of dolphin whistles by <a href=\"https://gitlab.com/dolphicom/dolphicom\">dolphicom</a>")
          p("The images in the pages below are audio spectrograms of the audio dataset. The overlaid boxes and yellow markings represent dolphicom's detected whistles. The '#number' above each box is the ID of the corresponding whistle.")
          p("For more details, check the dolphicom repository linked above")
          p(f"Date generated: {dt.datetime.now()}")
          p(f"Short description: {self.html_title_prefix}")
          p(f"Dataset: check short description above. Directory name: {dir_lookup}") # FIXME maybe instead of passing html_title_prefix, I should pass a name for the dataset
          p(f"Dolphicom version: {dolphicom_version}")

        with div():
          h2("Spectrograms and detected whistles by dolphicom")
          for page_i in page_l:
            page_i2 = os.path.basename(page_i)
            a(page_i2, href=page_i2)

    with open(f'{dir_lookup}/html/index.html', 'w') as f:
      f.write(doc.render())



  @classmethod
  def run(cls, dir_lookup, html_title_prefix):
    # Hence it is the parent of the PipelinePP generated folder for each audio file
    ghg = cls(dir_lookup, html_title_prefix)
    ghg.calc_dir_root_l()
    ghg.calc_fn_page_l()
    ghg.generate_html_pages()
    ghg.generate_html_index()
    print(f"Done generating html gallery in '{dir_lookup}/html'")
