from .s3_tarfile import S3TarFile
from .s3_zipfile import S3ZipFile


# Provide a main for execution like tarfile
# https://github.com/python/cpython/blob/3.9/Lib/tarfile.py#L2462
def main():
    import argparse

    description = 'A simple command-line interface for s3_tarfile module.'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        help='Verbose output')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-l', '--list', nargs=2,
                       metavar=('<bucket>', '<tarfile>'),
                       help='Show listing of a tarfile in a s3 bucket')

    # thought this would be nargs='+', but it works with 4. Not sure which is right though
    # Update: FIXME this is wrong and only works for exactly one file. Need to figure out how argparse would allow multiple "file" entries
    group.add_argument('-e', '--extract', nargs=4,
                       metavar=('<bucket>', '<tarfile>', '<output_dir>', '<file>'),
                       help='Extract tarfile from s3 bucket into target dir. Pass "file" to extract one file only.')
    args = parser.parse_args()

    if args.list is not None:
        if len(args.list) != 2: parser.exit(1, parser.format_help())

        bucket_name, src = args.list
        
        # if tarfile.is_tarfile(src):
        cls = None
        if src.endswith(".tar"):
          cls = S3TarFile
        elif src.endswith(".zip"):
          cls = S3ZipFile
        else:
          parser.exit(1, '{!r} is not a tar/zip archive.\n'.format(src))

        tf = cls(bucket_name, src, args.verbose)
        tf.print_list()



    elif args.extract is not None:
        file_only = None
        if len(args.extract) == 2:
            bucket_name, src = args.extract
            curdir = os.curdir
        elif len(args.extract) == 3:
            bucket_name, src, curdir = args.extract
        elif len(args.extract) >= 4:
            bucket_name, src, curdir = [args.extract.pop(0) for i in range(3)]
            file_only = args.extract
        else:
            parser.exit(1, parser.format_help())

        # if is_tarfile(src):
        cls = None
        if src.endswith(".tar"):
          cls = S3TarFile
        elif src.endswith(".zip"):
          raise Exception("zip not implemented yet")
          # cls = S3ZipFile
        else:
          parser.exit(1, '{!r} is not a tar/zip archive.\n'.format(src))

        tf = cls(bucket_name, src, args.verbose)
        try:
          tf.extractall_str(curdir, file_only)
        except Exception as e:
          parser.exit(2, str(e))


if __name__ == '__main__':
    main()
