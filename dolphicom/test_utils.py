# https://docs.python.org/3/library/unittest.html
import unittest
import numpy as np


class TestUtils(unittest.TestCase):
    def test_my_rm_glob(self):
      #!touch bla bli
      with open("bla", "w") as f: f.write("bla")
      with open("bli", "w") as f: f.write("bli")
      import os
      assert os.path.exists("bla")
      assert os.path.exists("bli")
      from .utils import my_rm_glob
      my_rm_glob("bl*")
      assert not os.path.exists("bla")
      assert not os.path.exists("bli")


    def test_np_rolling_savgol_piecewise(self):
      from .utils import np_apply_piecewise_notnan

      np.testing.assert_almost_equal(
          np_apply_piecewise_notnan(
              np.array([np.nan, np.nan, 1, 2, 3]),
              func_isnan=lambda v: v,
              func_notnan=lambda v: v+1,
              verbose=True
          ),
          np.array([np.nan, np.nan, 2, 3, 4])
      )

      np.testing.assert_almost_equal(
          np_apply_piecewise_notnan(
              np.array([np.nan, np.nan, 1, 2, 3, np.nan]),
              func_isnan=lambda v: [np.nan],
              func_notnan=lambda v: [np.mean(v)],
              verbose=True
          ),
          np.array([np.nan, 2, np.nan])
      )



    def test_np_rolling_savgol_piecewise(self):
      from .utils import np_rolling_savgol_piecewise
      np.testing.assert_almost_equal(
        np_rolling_savgol_piecewise(np.array([1,2,3,4,5,6,7,8]), 5),
        #np.array([np.nan, np.nan, 3, 4, 5, 6, np.nan, np.nan])
        # after moving to savgol filter
        np.array([1, 2, 3, 4, 5, 6, 7, 8])
      )

      # issue fix: need max+1 in piece-wise for loop, otherwise last non-nan segment will be missed
      np.testing.assert_almost_equal(
        np_rolling_savgol_piecewise(np.array([np.nan,2,3,4,5,6,7,8]), 5),
        #np.array([np.nan, np.nan, np.nan, 4, 5, 6, np.nan, np.nan])
        # after update to savgol filter
        np.array([np.nan, 2, 3, 4, 5, 6, 7, 8])
      )


    def test_close_to_reference(self):
      # Tests
      from .utils import close_to_reference
      assert close_to_reference([1], [0], [ 1]) == [False]
      assert close_to_reference([1], [0], [10]) == [True]


    def test_close_to_zero(self):
      # Tests
      from .utils import close_to_zero
      assert close_to_zero([4]) == [False]
      assert close_to_zero([4, 50]) == [True, False]
      assert close_to_zero([13,11]) == [False, False]
      assert close_to_zero([]) == []

      # Case of repeated value: inspired me to use "set" in the function
      # so that there is only one center of gravity
      # Important to notice the 7 and 8 in the below example
      assert close_to_zero([64, 1, 1, 8, 54, 1, 4, 1, 139, 7]) == [False, True, True, True, False, True, True, True, False, True]

      # Case of x then x-1 causing x and x-1 to make their own set (when min_samples was = 1)
      assert close_to_zero([14, 4]) == [False, True]
      assert close_to_zero([14, 4, 3]) == [False, True, True] # check that 3 and 4 stay with 0

      # case that makes sense in terms of lengths in later usage for whistles
      assert close_to_zero([21,119]) == [False, False]

      # another example showing the effect of density
      assert close_to_zero([64, 54, 139, 19    ]) == [False, False, False, False     ]
      assert close_to_zero([64, 54, 139, 19, 10]) == [False, False, False, True, True]


    def test_iter_batch(self):
      from .utils import iter_batch
      x = np.arange(100).reshape((2,50))
      y = list(iter_batch(x, 20, "bla"))
      y = np.concatenate([z[2] for z in y], axis=1)
      np.testing.assert_equal(y, x)



if __name__ == '__main__':
    unittest.main()
