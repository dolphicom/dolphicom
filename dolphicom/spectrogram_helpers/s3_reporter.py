from tqdm.auto import tqdm
import numpy as np
from scipy import ndimage
from matplotlib import pyplot as plt
import pandas as pd
import os
from ..core.corpus import Corpus


class Reporter:
  def __init__(self, sp_filt):
    self.sp_filt = sp_filt


  def get_t_whistles(self):
    # Update: after sparse matrices, need to change this
    # idx_whistle = (self.sp_filt.Sxx!=0).any(axis=0)
    idx_whistle = np.full_like(self.sp_filt.t, fill_value=False, dtype=bool)
    idx_whistle[self.sp_filt.Sxx.nonzero()[1]] = True

    # The "append=0" will cause the w_{start,end} to be one point longer than the idx_whistle vector
    # but that's ok because the w_end will be used in the right-side of slicing, which will include the last point of the slice
    # if it's len(vector)+1, as it is
    w_edges = np.diff(idx_whistle.astype(int), prepend=0, append=0)
    w_start, w_end = np.where(w_edges==+1)[0], np.where(w_edges==-1)[0]

    if len(w_start) != len(w_end):
      raise ValueError(f"len(w_start)={len(w_start)} != len(w_end)={len(w_end)}. Aborting")

    # Update: after sparse matrices, this doesn't work, but I don't really need it anyway
    # It used to make contours around the whistle instead of just full colors in yellow
    #self.Sxx_mask = self._plot_getmask()
    self.Sxx_mask = self.sp_filt.Sxx

    self.idx_whistle = idx_whistle

    # Note that this includes whistles that were noise, i.e. before filtering
    self.t_whistle = self.sp_filt.t[w_start]

    # Update 2021-06-11: bring back the idx_keep array, but this time from the whistle corpus variable
    t_keep = np.zeros_like(self.sp_filt.t)
    wc = self.sp_filt.s23_whistle_corpus
    # iterate over dataframe while preserving dtype http://stackoverflow.com/questions/48722661/ddg#48722907
    for row in wc.itertuples(index=False): t_keep[ row.t_idx_min : row.t_idx_max ] = 1
    self.idx_keep = t_keep == 1


  def plot_t_whistles(self):
    if len(self.t_whistle)==0:
      print("No whistles => skip empty plotting")
      return

    t = self.sp_filt.t
    idx_whistle, idx_keep = self.idx_whistle, self.idx_keep
    
    plt.figure(figsize=(20,1))
    plt.plot(t, 1+0*t, color="blue")
    plt.scatter(t[idx_whistle], 1+0*t[idx_whistle], color="orange", marker="|")
    plt.scatter(t[idx_keep   ], 1+0*t[idx_keep   ], color="red",    marker="|")
    plt.xlabel("seconds")
    plt.ylabel("ignore")
    plt.show()


  # automatic time-start, whistle-marker calculations
  def convert_twhistle_to_plotrange_params(self):
    self.plt_params = {}
    self.plt_params["segment"] = self._get_plotrange_params_segment()
    self.plt_params["full"] = self._get_plotrange_params_full()
    self.plt_params["key"] = None # updated later

  def _get_plotrange_params_segment(self):
    t_whistle = self.t_whistle
    
    plt_params = []
    t_last = -1000
    PLT_LEN = 5
    for t_i in t_whistle:
      if t_i >= t_last + PLT_LEN:
        # convert to int since can only put markers at int positions ATM
        t_last = int(t_i)
        plt_i = {}
        # start 1 second earlier for the sake of showing any preceding and missed whistles
        plt_i["sec_start"] = max(t_last - 1, 0)
        plt_i["sec_marker"] = plt_i["sec_start"] # dummy
        plt_i["sec_whistle_markers"] = set([]) # use set since we're inserting int values of the exact whistle start times
        plt_params.append(plt_i)

      # convert to int since can only put markers at int positions ATM
      plt_params[-1]["sec_whistle_markers"].add(int(t_i))

    # convert all sec_whistle_markers from set to list
    for i in range(len(plt_params)):
      x = plt_params[i]["sec_whistle_markers"]
      plt_params[i]["sec_whistle_markers"] = sorted(list(x))

    return plt_params


  def _get_plotrange_params_full(self):
    t_max = np.max(self.sp_filt.t)

    plt_params = []
    PLT_LEN = 5
    for t_i in range(0, int(t_max)+1, PLT_LEN):
        plt_i = {}
        # No need to start 1 second earlier since showing all time not just segments
        plt_i["sec_start"] = t_i # - 1
        plt_i["sec_marker"] = plt_i["sec_start"] # dummy
        plt_i["sec_whistle_markers"] = []
        plt_params.append(plt_i)

    return plt_params


  def print_report(self):
    print("")
    print("Report")
    print(f"# detected whistles (unfiltered, approximated): {len(self.t_whistle)}")
    print(f"# 5-second segments containing whistles (unfiltered, approximated): {len(self.plt_params['segment'])}")
    print("")
    print(f"# mended(1) whistles: {self.sp_filt.s22_connections.shape[0]}")
    print(f"# mended(2) whistles: {self.sp_filt.s23_connections.shape[0]}")
    if hasattr(self.sp_filt, "s23_wsw"): print(f"# detected whistles (pre-filtering, exact): {self.sp_filt.s23_wsw.ws_init.whistle_corpus2.shape[0]}")
    if hasattr(self.sp_filt, "s23_wsw"): print(f"# detected whistles (post-filtering, exact, mended(2)): {self.sp_filt.s23_wsw.ws_final.whistle_corpus2.shape[0]}")

    # not sure yet why different than previous line
    c = Corpus(self.sp_filt.s23_whistle_corpus)
    c.print_report()


  # Update: after change to sparse matrices, need to change this
  # Don't need it anyway
  """
  def _plot_getmask(self):
    def sxx_to_mask(Sxx_i):
      Sxx_i = (Sxx_i!=0).astype(int)
      Sxx_i = ndimage.morphological_gradient(Sxx_i, size=(3,3)).astype(float)
      return Sxx_i

    Sxx_mask = sxx_to_mask(self.sp_filt.Sxx)
    Sxx_mask[Sxx_mask==0] = np.nan # set nan *after* the 2*a + 1*b operation above because otherwise I get 2*nan + 1*1 = nan
    return Sxx_mask
  """


  def _plot_frame(self, sec_start):
    return self.sp_filt.plot_range(sec_start, sec_start, True, None, None, [], self.Sxx_mask)


  def generate_png(self, video_type, PNGDIR, full, with_marker):
    """
    with_marker: True - Break each 5-second spectrogram into 5 png's displaying a marker at seconds 0, 1, 2, 3, 4
                 False - Do not display marker, which creates 5x less png files, and hence is faster
    video_type: key in self.sp_filt.Sxx_checkpoints.keys()
    """
    if video_type not in self.sp_filt.Sxx_checkpoints.keys():
      raise ValueError(f"Invalid video_type={video_type}. Please pass one of {self.sp_filt.Sxx_checkpoints.keys()}")

    # save to multiple png to generate video
    self.plt_params["key"] = "full" if full else "segment"
    plt_params_use = self.plt_params[self.plt_params["key"]]
    
    i_step = 5
    j_step = 1 if with_marker else 5
    fn_all = []
    for i, plt_i in enumerate(tqdm(plt_params_use)):
      for j in range(0, i_step, j_step):
        fn_i = self.sp_filt.plot_range(
            sec_start=plt_i["sec_start"],
            sec_marker=plt_i["sec_marker"] + j,
            show=False,
            save_dir=PNGDIR,
            save_suffix=None,
            sec_more_markers=plt_i["sec_whistle_markers"],
            Sxx_mask=self.Sxx_mask,
            Sxx_plot=self.sp_filt.Sxx_checkpoints[video_type]
          )
        if fn_i is not None: fn_all.append((i,j,j_step,fn_i))

    self.fn_all = fn_all


  def generate_gallery(self, PNGDIR):
    """
    PNGDIR: directory in which to place the png files of spectrograms
    """

    # use earliest checkpoint
    video_type = sorted(self.sp_filt.Sxx_checkpoints.keys())[0]
    print(f"s3_reporter.generate_gallery: Using video_type={video_type} for '{os.path.basename(self.sp_filt.fn_in)}'")

    # save to multiple png to generate video
    plt_params_use = self.plt_params["segment"]
    
    fn_all = []
    for i, plt_i in enumerate(tqdm(plt_params_use, desc="s3_reporter.generate_gallery")):
        # first without highlight
        _ = self.sp_filt.plot_range(
            sec_start=plt_i["sec_start"],
            sec_marker=None,
            show=False,
            save_dir=PNGDIR,
            save_suffix="1_nohighlight",
            sec_more_markers=plt_i["sec_whistle_markers"],
            Sxx_mask=None,
            Sxx_plot=self.sp_filt.Sxx_checkpoints[video_type]
          )
        # second with highlight
        _ = self.sp_filt.plot_range(
            sec_start=plt_i["sec_start"],
            sec_marker=None,
            show=False,
            save_dir=PNGDIR,
            save_suffix="2_highlighted",
            sec_more_markers=plt_i["sec_whistle_markers"],
            Sxx_mask=self.Sxx_mask,
            Sxx_plot=self.sp_filt.Sxx_checkpoints[video_type]
          )


  def to_dict(self):
    d4 = {}
    d4["Sxx_mask"]    = self.Sxx_mask
    d4["idx_whistle"] = self.idx_whistle
    d4["t_whistle"]   = self.t_whistle
    d4["idx_keep"]    = self.idx_keep
    d4["plt_params"]  = self.plt_params
    return d4


  @classmethod
  def from_dict(cls, d4, sp_filt):
    self = cls(sp_filt)
    self.Sxx_mask    = d4["Sxx_mask"]
    self.idx_whistle = d4["idx_whistle"]
    self.t_whistle   = d4["t_whistle"]
    self.idx_keep    = d4["idx_keep"]
    self.plt_params  = d4["plt_params"]
    return self
