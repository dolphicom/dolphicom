"""
Segment the spectrogram by whistle using hdbscan to label whistles separately.
This is better than skimage.measure.label because the latter will give multiple labels if there are discontinuities.
Do not use the WhistleSegmenter directly, but rather via the whistle_segwrap.WhistleSegWrap class which can also connect close whistles together
"""

import numpy as np
import pandas as pd
from scipy import ndimage
from skimage.measure import label
from tqdm.auto import tqdm
from ..utils import np_wide_to_long_pd
from ..utils import AbortPipeline


class WhistleSegmenter:

  def __init__(self, t, f, Sxx_filt, verbose):
    if np.isnan(Sxx_filt).any():
      raise ValueError("Error in s23a_whistle_segmenter.WhistleSegmenter: Please replace nan values from Sxx before passing to this function")

    self.t, self.f = t, f
    self.Sxx_filt = Sxx_filt
    self.verbose = verbose


  def wide_to_long(self):
    # Method 2: use the raw spectrogram with values maybe hdbscan doesn't need the filtered mask above
    # To avoid running out of memory, filter the raw spectrogam on the filtered mask with padding
    # Update: just pass the filtered mask since remove_noise with the flood-filling is really good

    if not (self.Sxx_filt!=0).any():
      #self.Sxx_skimage = pd.DataFrame([])
      #return
      raise AbortPipeline("s23a.wide_to_long: all spectrogram at this stage is 0. Aborting")

    # TESTING FIXME
    #Sxx_use1 = np.copy(p.wm.sp_filt.Sxx)
    Sxx_use1 = np.copy(self.Sxx_filt!=0)

    #Sxx_use1 = np.copy(p.wm.Sxx_mask)
    #Sxx_use1[np.isnan(Sxx_use1)]=0
    #Sxx_use1 = Sxx_use1!=0

    Sxx_use = np_wide_to_long_pd(Sxx_use1)
    Sxx_use["f_hz"]  = self.f[Sxx_use.f_idx]
    Sxx_use["t_sec"] = self.t[Sxx_use.t_idx]

    # use skimage label to add an ID per blob
    # Flatten values with "!=0" to get a single label per blob
    # Transpose to get increasing labels in time first and in frequency next, then transpose back
    Sxx_use1_labels = label((Sxx_use1!=0).transpose()).transpose()
    Sxx_labels = np_wide_to_long_pd(Sxx_use1_labels)

    # combine
    Sxx_use["skimage_label"] = Sxx_labels["S"].astype(np.int64).values

    #print(Sxx_use.shape)

    del Sxx_use1
    del Sxx_labels
    del Sxx_use1_labels

    self.Sxx_skimage = Sxx_use


  def label_hdbscan(self):
    Sxx_use = self.Sxx_skimage

    if Sxx_use.shape[0]==0:
      #self.wc_long = pd.DataFrame([])
      #self.Sxx_mask = np.full_like(self.Sxx_filt, np.nan)
      #return
      raise AbortPipeline("s23a.label (input): No labeled whistles. Aborting")

    # ETA 20 minutes


    # ETA 30 seconds on 1h audio, with chunking by 50 seconds
    labels_l = []

    # Update: at row 20k, it hits the 5 second mark
    # 200k -> ~1 minute
    # from ..utils import np_index_at_value
    # np_index_at_value(Sxx_use[:,1], 1000)
    #batch_size = int(20e3)
    #batch_size = int(200e3)
    #b_range = np.arange(0, Sxx_use.shape[0], batch_size)

    # Update: use skimage labels as each batch
    # batch every x blobs at a time
    # b_size = 20
    b_size = 400
    b_range = np.arange(int(Sxx_use["skimage_label"].min()), int(Sxx_use["skimage_label"].max()+1), b_size)

    if self.verbose:
      print(f"labeling by batch for {b_range.shape[0]} batches")
      b_range = tqdm(b_range)

    for batch_i, b_start in enumerate(b_range):
      #Sxx_sub = np.copy(Sxx_use[batch_start:(batch_start+batch_size), :])
      #Sxx_sub = Sxx_use[batch_start:(batch_start+batch_size), [0,1]]

      b_end = b_start + b_size
      b_idx = (Sxx_use["skimage_label"] >= b_start) & (Sxx_use["skimage_label"] < b_end)
      Sxx_sub = Sxx_use[b_idx].copy()

      min_cluster_size = 30
      if Sxx_sub.shape[0] < min_cluster_size: continue # skip

      Sxx_sub2 = Sxx_sub.copy()

      # convert to 10khz otherwise hdbscan thinks they're too far
      # The factor of 10k was selected by testing different factors in notebook t07.2c
      # and finding that this is the one that gives the most whistles after filtering
      # as well as does not merge the 40s-45s long whistle with the smaller whistle above it
      # Note: this scaling works with the t_sec being passed to hdbscan. If t_idx were used instead, a different factor would be needed
      # https://colab.research.google.com/drive/1SUwas0-syDGCfIAP38i31phXSn7wu27Z#scrollTo=csHrrl2wcqDw
      # Update: the "S" field is just 1, so useless
      f_factor = 10000
      Sxx_sub2["f_scaled"] = Sxx_sub2["f_hz"]/f_factor
      #Sxx_sub2 = Sxx_sub2[["t_idx", "f_idx", "S"]]
      #Sxx_sub2 = Sxx_sub2[["t_sec", "f_scaled", "S"]]
      Sxx_sub2 = Sxx_sub2[["t_sec", "f_scaled"]]

      # cluster_selection_epsilon: doesn't help
      import hdbscan
      segmenter = hdbscan.HDBSCAN(min_cluster_size=min_cluster_size, min_samples=10, allow_single_cluster=True).fit(Sxx_sub2.values)

      # if unable to create any clusters, skip
      if segmenter.labels_.max()==-1: continue

      Sxx_sub["hdbscan_label"] = segmenter.labels_
      Sxx_sub["batch_i"] = batch_i
      Sxx_sub["prob"] = segmenter.probabilities_

      labels_l.append(Sxx_sub)

    if len(labels_l)==0:
      #self.wc_long = pd.DataFrame([])
      #self.Sxx_mask = np.full_like(self.Sxx_filt, np.nan)
      #return
      raise AbortPipeline("s23a.label (after batches): No labeled whistles. Aborting")

    wc_long = pd.concat(labels_l, axis=0)
    wc_long = wc_long[wc_long.hdbscan_label != -1]
    wc_long = wc_long[wc_long.prob >= .4]
    wc_max = (
        (wc_long[["batch_i","hdbscan_label"]].groupby("batch_i").hdbscan_label.max()+1)
        .cumsum().shift(1).fillna(0).astype(np.int64).reset_index()
        .rename(columns={"hdbscan_label": "l2_max"})
    )
    wc_long = wc_long.merge(wc_max, how="left", on="batch_i")
    wc_long["l"] = wc_long["hdbscan_label"] + wc_long["l2_max"]

    self.wc_long = wc_long

    Sxx_mask = np.full_like(self.Sxx_filt, np.nan)
    Sxx_mask[wc_long.f_idx, wc_long.t_idx] = wc_long.l
    #Sxx_filt2 = np.multiply(Sxx_mask, sp_raw.Sxx)
    #Sxx_filt2[np.isnan(Sxx_filt2)] = 0

    self.Sxx_mask = Sxx_mask
    #self.segmenter = segmenter


  def label_skimage(self):
    """
    Alternative to label_hdbscan.
    Just use the skimage labels without running them through hdbscan.
    hdbscan was helping mend whistles together.
    """
    Sxx_use = self.Sxx_skimage

    if Sxx_use.shape[0]==0:
      #self.wc_long = pd.DataFrame([])
      #self.Sxx_mask = np.full_like(self.Sxx_filt, np.nan)
      #return
      raise AbortPipeline("s23a.label (input): No labeled whistles. Aborting")

    wc_long = self.Sxx_skimage
    wc_long["l"] = wc_long.skimage_label
    Sxx_mask = np.full_like(self.Sxx_filt, np.nan)
    Sxx_mask[wc_long.f_idx, wc_long.t_idx] = wc_long.l
    self.Sxx_mask = Sxx_mask
    self.wc_long = wc_long


  def print_stats(self):
    # use strung together labels
    wc_long = self.wc_long

    if wc_long.shape[0]==0:
      print("s23a segmenter stats: No whistles")
      return

    # Note that .max() here is only max label "per batch"
    print("s23a segmenter stats", wc_long.shape,
          wc_long.batch_i.min(), wc_long.batch_i.max(),
          wc_long.hdbscan_label.min(), wc_long.hdbscan_label.max(),
          )
    #print(pd.Series(segmenter.labels_).value_counts().head(n=10))



  def build_corpus(self, filter_noise_blobs):
    # aggregate whistles

    if self.wc_long.shape[0]==0:
      #self.whistle_corpus2 = pd.DataFrame([])
      #return
      raise AbortPipeline("s23a.build_corpus: No labeled whistles. Aborting")

    wc = (
      self.wc_long
      .groupby("l")
      .agg({
          "t_idx": ["min", "max"],
          "f_idx": ["min", "max"],
          "t_sec": ["min", "max"],
          "f_hz" : ["min", "max"],
        })
      .reset_index() # to get back column "l"
      #.astype(np.int64)
      #.reset_index(level=1)
      #.sort_values(["t_min", "f_min"])
    )

    # Column "l" will become "l_" because its second-level column name is "", so adding the strip call
    wc.columns = ['_'.join(col).strip("_") for col in wc.columns]

    # sort because label=1 is not necessarily the first whistle (since hdbscan does not give out labels in the time order)
    wc = wc.sort_values(["t_idx_min", "t_idx_max", "f_idx_min", "f_idx_max"]).reset_index(drop=True)

    # add the t_sec and f_hz if not already
    #wc["t_sec_min"] = wc["t_idx_min"].apply(lambda x: t[x]).round(2)
    #wc["t_sec_max"] = wc["t_idx_max"].apply(lambda x: t[x]).round(2)
    #wc["f_hz_min"]  = wc["f_idx_min"].apply(lambda x: f[x]).astype(np.int64)
    #wc["f_hz_max"]  = wc["f_idx_max"].apply(lambda x: f[x]).astype(np.int64)

    # features
    wc["t_len_sec"] = wc.eval("t_sec_max - t_sec_min")
    wc["f_len_hz"] = wc.eval("f_hz_max - f_hz_min")

    # drop shorter than 200 ms or longer than 5 seconds
    if filter_noise_blobs:
        if self.verbose: print(f"Before dropping noise blobs: {wc.shape[0]} whistles")
        wc = wc[wc.t_len_sec >=  .2]
        wc = wc[wc.t_len_sec <= 5  ]
        wc = wc.reset_index(drop=True)
        if self.verbose: print(f"After dropping noise blobs: {wc.shape[0]} whistles")

    if wc.shape[0]==0:
        print("No whistles left after dropping noise blobs")
        self.whistle_corpus2 = wc
        return

    def my_get_mask(r):
      # +1 to be inclusive
      S1 = self.Sxx_mask[int(r.f_idx_min):int(r.f_idx_max+1), int(r.t_idx_min):int(r.t_idx_max+1)]
      S1 = np.copy(S1)
      S1[ S1!=r.l ] = np.nan
      # return the original spectrogram values, since the hdbscan labels in S1 are not very useful from here onwards
      S2 = self.Sxx_filt[int(r.f_idx_min):int(r.f_idx_max+1), int(r.t_idx_min):int(r.t_idx_max+1)]
      S2 = np.copy(S2)
      S2[np.isnan(S1)] = np.nan
      return S2

    wc["S"] = wc.apply(my_get_mask, axis=1)
    wc["mask_area_pct"] = wc.apply(lambda r: int((~np.isnan(r.S)).sum()*100//np.prod(r.S.shape)), axis=1)
    wc["mask_freq_pct"] = wc.apply(lambda r: int((~np.isnan(r.S)).any(axis=1).sum()*100//r.S.shape[0]), axis=1)
    wc["mask_time_pct"] = wc.apply(lambda r: int((~np.isnan(r.S)).any(axis=0).sum()*100//r.S.shape[1]), axis=1)

    wc = wc.reset_index().rename(columns={"index": "hdbscan_label"})

    #wc.shape
    self.whistle_corpus2 = wc


  def filter_corpus(self):
    # keep smaller than 30%
    # Update change from 30 to 40%
    wc = self.whistle_corpus2
    if wc.shape[0]==0: return # already empty
    if self.verbose: print(f"Before filtering: {wc.shape[0]} whistles")

    # Update: after adding blur step in WhistleSegWrap, increasing the mask_area_pct limit from 40 to 60
    wc = wc[wc["mask_area_pct"] <= 60]
    wc = wc[wc["mask_freq_pct"] >= 90]
    wc = wc[wc["mask_time_pct"] >= 90]
    wc = wc.reset_index(drop=True)
    if self.verbose: print(f"After filtering: {wc.shape[0]} whistles")

    del wc["hdbscan_label"]
    wc = wc.reset_index().rename(columns={"index": "hdbscan_label"})

    self.whistle_corpus2 = wc


  def plot_corpus(self, sp_raw, sec_min=0, sec_max=50):
    wc = self.whistle_corpus2
    if wc.shape[0]==0:
      print("No whistles detected (or perhaps all whistles dropped in filtering stages) => no plot")
      return

    for sec_st in np.arange(sec_min,sec_max,5):
      wh_sub = wc[(sec_st <= wc.t_sec_min) & ((sec_st+5) >= wc.t_sec_min)]
      if wh_sub.shape[0]==0: continue
      sp_raw.plot_range(sec_st, sec_st, True, None, None, [], Sxx_mask=self.Sxx_mask, df_label=wc)
