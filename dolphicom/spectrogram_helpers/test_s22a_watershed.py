# https://docs.python.org/3/library/unittest.html
import unittest

class TestS22aWatershed(unittest.TestCase):
    def test_all(self):
      from .s22a_watershed import S22aWatershed
      import numpy as np
      
      dummy = np.zeros(shape=(129, 1000))
      Sxx_ori = np.copy(dummy)
      Sxx_input = np.copy(dummy)
      Sxx_markers = np.copy(dummy)
      
      s22a_watershed = S22aWatershed(Sxx_ori, Sxx_input, Sxx_markers, verbose=False)
      s22a_watershed.calc_input()
      s22a_watershed.calc_markers()
      s22a_watershed.calc_watershed()    

      assert hasattr(s22a_watershed, "watershed")


if __name__ == '__main__':
    unittest.main()
