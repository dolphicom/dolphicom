from matplotlib import pyplot as plt
import numpy as np



class S41Stats:

  def __init__(self, t, f, Sxx, whistle_corpus3):
    self.t, self.f, self.Sxx = t, f, Sxx
    self.whistle_corpus3 = whistle_corpus3


  def plot_1_hist_area_tlen_flen(self):
    whistle_corpus3 = self.whistle_corpus3

    hist_bins = np.arange(whistle_corpus3["mask_area_pct"].min(), whistle_corpus3["mask_area_pct"].max())
    whistle_corpus3["mask_area_pct"].hist(bins=hist_bins)
    #plt.axvline(x=28, color="orange")
    #plt.text(28-3, 2, "x=28", color="orange")
    plt.xlabel("Ratio of whistle to bounding box area (%)")
    plt.ylabel("Number of whistles")
    plt.show()
    
    # Whistle length distribution
    hist_bins_sec = np.arange(
        whistle_corpus3.t_len_sec.min(),
        whistle_corpus3.t_len_sec.max(),
        np.diff(self.t[:3])[0] * 10
        #.05
      )
    self.hist_bins_sec = hist_bins_sec
    whistle_corpus3.t_len_sec.hist(bins=hist_bins_sec)
    plt.xlabel("seconds")
    plt.title("Number of whistles by length\nWhistles having length < 0.2s & > 5s are dropped")
    plt.show()
    
    
    hist_bins_hz=np.arange(
        whistle_corpus3["f_len_hz"].min(),
        whistle_corpus3["f_len_hz"].max(),
        np.diff(self.f[:3])[0]
      )
    self.hist_bins_hz = hist_bins_hz
    whistle_corpus3["f_len_hz"].hist(bins=hist_bins_hz)
    plt.xlabel("Hz")
    plt.title("Number of whistles by length\nWhistles having length < 0.2s & > 5s are dropped")
    plt.show()
    

  def plot_2_hist_2d_tlen_flen(self):
    whistle_corpus3 = self.whistle_corpus3
    #hexbin_gridsize = (20,10)
    hexbin_gridsize = (self.hist_bins_hz.shape[0], self.hist_bins_sec.shape[0])

    plt.hexbin(whistle_corpus3["t_len_sec"], whistle_corpus3["f_len_hz"], mincnt=1, gridsize=hexbin_gridsize)
    plt.xlabel("length in time (secs)")
    plt.ylabel("length in frequency (Hz)")
    plt.title("Number of whistles by length (time & frequency)\nWhistles having length < 0.25s are dropped")
    #plt.ylim((0,f.max()))
    plt.xlim(0)
    plt.ylim(0)
    plt.axhline(y=700, color="red")
    plt.text(-.55, 700, "freq = 700 Hz", color="red")
    plt.axvline(x=.2, color="red")
    plt.text(.2, -1700, "t = 0.2 sec", color="red")
    plt.show()


  def set_S_samelen(self):
    whistle_corpus3 = self.whistle_corpus3

    whistle_corpus3["t_len_idx"] = whistle_corpus3.eval("t_idx_max - t_idx_min + 1")
    whistle_corpus3["f_len_idx"] = whistle_corpus3.eval("f_idx_max - f_idx_min + 1")
    whistle_corpus3["area_idx"] = whistle_corpus3.eval("t_len_idx*f_len_idx")
    whistle_corpus3.sort_values("area_idx", ascending=False, inplace=True)

    Sxx_filt = self.Sxx
    def my_get_maskedfilt(r):
      S_sub = Sxx_filt[int(r.f_idx_min):int(r.f_idx_max+1), int(r.t_idx_min):int(r.t_idx_max+1)]
      #print(S_sub.shape, r.S.shape)
      assert S_sub.shape == r.S.shape
      o = np.multiply(~np.isnan(r.S), S_sub)
      return o
    
    wc_samelen = whistle_corpus3.apply(my_get_maskedfilt, axis=1).tolist()
    
    max_len_t, max_len_f = whistle_corpus3.t_len_idx.max(), whistle_corpus3.f_len_idx.max()
    
    for i in range(len(wc_samelen)):
      x = wc_samelen[i]
      pad_f = np.full(shape=(max_len_f-x.shape[0], x.shape[1]), fill_value=np.nan)
      pad_t = np.full(shape=(x.shape[0] + pad_f.shape[0], max_len_t-x.shape[1]), fill_value=np.nan)
      x = np.concatenate([x, pad_f], axis=0)
      x = np.concatenate([x, pad_t], axis=1)
      wc_samelen[i] = x
      
      
    whistle_corpus3["S_samelen"] = wc_samelen
    
    del wc_samelen
    
    print("samelen stats: ", [x.shape for x in whistle_corpus3.S_samelen.head()])

    self.whistle_corpus3 = whistle_corpus3



  def plot_3_S_samelen(self, n_plt):
    whistle_corpus3 = self.whistle_corpus3

    # option
    #n_plt = 100 * 1 # quick plot for testing
    #n_plt = 100 * 10 # viz full whistles, 989 whistles => show 1k whistles
    
    
    t, f = self.t, self.f
    max_len_t, max_len_f = whistle_corpus3.t_len_idx.max(), whistle_corpus3.f_len_idx.max()
   
    # Sort to view most relevant first
    # option 1
    #i_range = range(n_plt)
    
    # option 2
    #i_range = np.where(whistle_corpus3.mask_area_pct <= 15)[0]
    
    # option 3: sort by decreasing total area
    #i_range = np.argsort(whistle_corpus3.mask_area_pct.values)
    i_range = np.argsort(whistle_corpus3.eval("t_len_idx * f_len_idx").values)
    i_range = list(reversed(i_range))
    i_range = i_range[:n_plt]
    
    
    import math
    #x_plt, y_plt = math.ceil(n_plt**.5), math.floor(n_plt**.5)
    
    x_plt, y_plt = 10, 10
    
    for i1, j in enumerate(i_range):
        i2 = i1 % (x_plt * y_plt)
        if i2 == 0:
            print("#"*100)
            print(f"plot set # {i1//(x_plt*y_plt)+1}")
            fig, ax = plt.subplots(x_plt, y_plt, figsize=(20,20), sharex=True, sharey=True)
            ax = ax.reshape((1,-1)).squeeze()
    
        wh_i = whistle_corpus3.iloc[j]
        f_start = wh_i.f_idx_min
        mask_area_pct = wh_i.mask_area_pct
    
        idx_3d = min(f_start+max_len_f, f.shape[0]) - f_start
        ax[i2].pcolormesh(
            t[:max_len_t],
            #f[:max_len_f],
            f[f_start:(f_start+max_len_f)],
            wh_i.S_samelen[:idx_3d, :], #mat_raw_3d[j, :idx_3d, :],
            # shading='gouraud', # this causes a rectangular square due to 0's and nan's
            cmap="Greys"
          )
        if i1==0:
          ax[i2].set_ylabel("Frequency (Hz, rebased)")
          ax[i2].set_xlabel("Time (seconds, rebased)")
    
        #ax[i2].set_xlim(0, t[max_len_t])
        #ax[i2].set_ylim(0, f[max_len_f])
        ax[i2].set_title(f"W #{j+1}: {mask_area_pct}%")
    
        if (i1+1) % (x_plt * y_plt) == 0:
            fig.tight_layout()
            plt.show()
