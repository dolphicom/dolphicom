# TEMPORARY till integrate
import numpy as np
from skimage import draw
from scipy import ndimage
from tqdm.auto import tqdm
from matplotlib import pyplot as plt



class S21StrengthenWhistles:

  def __init__(self, Sxx, detailed, verbose):
    """
    Build sloped-line kernels and apply grey_opening

    detailed - False: less memory consumption. True: returns the contribution of each angle kernel
               Was useful to use True in notebook t06b to analyze which line angle is detecting which whistles

    verbose: True|False. True to plot intermediate results
    """
    self.Sxx = Sxx
    self.detailed = detailed
    self.verbose = verbose


  def sloped_opening(self, KDIM, with_blimp):
      detailed = self.detailed

      # These slopes are too vertical/horizontal to be biologically possible
      # Update: just exclude the vertical slope
      #exclude_nonbio = np.arange(KDIM//2 - KDIM//10, KDIM//2 + KDIM//10 + 1).tolist() # +1 to be inclusive

      S_base = [] if detailed else np.zeros_like(self.Sxx)

      for kern_type in ["horizontal", "vertical"]:
        print(f"Strengthen with linear kernels of type: {kern_type}")
        for r0 in tqdm(range(KDIM-1)):
          # Update: just exclude the vertical slope
          # if r0 in exclude_nonbio: continue
          if (kern_type=="vertical") & (r0 == KDIM//2): continue

          linearKernel_i = np.zeros(shape=(KDIM, KDIM), dtype=int)
          if kern_type == "horizontal":
            linearKernel_i[draw.line(r0, 0, KDIM-r0-1, KDIM-1)] = 1
          else:
            if r0==0: continue # already done in horizontal
            linearKernel_i[draw.line(0, r0, KDIM-1, KDIM-r0-1)] = 1

          if with_blimp:
            #linearKernel_i[draw.line(r0-1, KDIM//2, KDIM-r0-1-1, KDIM//2)] = 1
            linearKernel_i[draw.rectangle(start=(KDIM//2-KDIM//10,KDIM//2-KDIM//10), extent=(3,3), shape=linearKernel_i.shape)] = 1
            #linearKernel_i[draw.line(r0+1, KDIM//2, KDIM-r0+1-1, KDIM//2)] = 1

          if self.verbose:
            plt.imshow(linearKernel_i, origin="lower")
            plt.title(r0)
            plt.show()

          S_single = ndimage.grey_opening(self.Sxx, footprint=linearKernel_i)
          if detailed:
            S_base.append(S_single)
          else:
            # Update: change from sum to max to avoid biasing towards flat angles that match THICK flat lines
            # eg at bottom of U
            # S_base += S_single
            S_base = np.maximum(S_base, S_single)

      if detailed: S_base = np.concatenate([x[:,:,None] for x in S_base], axis=2)
      return S_base
