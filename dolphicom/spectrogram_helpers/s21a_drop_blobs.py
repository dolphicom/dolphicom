from skimage.filters import gaussian
from skimage.measure import label, regionprops_table # regionprops
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from ..utils import my_imshow


def s21a_drop_blobs(Sxx_s13, verbose):
  if not Sxx_s13.any():
    if verbose: print("Empty spectrogram passed. Will just return as is")
    return Sxx_s13

  # Update: blur with sigma=2 was too much and causing imagined whistles. Going down to 1, along with thresholding on values
  #S_blur = gaussian(Sxx_s13, 2)
  S_blur = gaussian(Sxx_s13, 1)

  if verbose:
    my_imshow(Sxx_s13, "input")
    my_imshow(S_blur, "blurred")

  # try to remove blobs from S_blur
  S_label = label(S_blur > np.quantile(S_blur, q=.9))

  # Get regionprops
  #S_peri = pd.DataFrame(
  #    [(prop.label, prop.perimeter, prop.centroid) for prop in regionprops(S_label)],
  #    columns=["label", "perimeter", "centroid"]
  #  )

  properties=["label", "perimeter", "centroid", "eccentricity", "major_axis_length", "minor_axis_length"]
  S_peri = regionprops_table(S_label, properties=properties)

  # because of the check on Sxx_s13.any(), the below condition should always be True
  assert S_peri["label"].shape[0] > 0

  S_peri = pd.DataFrame(S_peri)

  S_peri["label"] = S_peri["label"].astype(int)
  S_peri["perimeter"] = S_peri["perimeter"].astype(int)
  S_peri["centroid-0"] = S_peri["centroid-0"].round(2)
  S_peri["centroid-1"] = S_peri["centroid-1"].round(2)
  
  # Filter based on perimeter
  # Update: after some experimentation, setting to a hard-coded value of 100
  #thresh_peri = S_peri.perimeter.mean()*3

  # threshold values below obtained by experimentation
  thresh_peri = 100
  thresh_major = 30
  thresh_ecc = .9
  def keep_whistle(row):
    # If the whistle passes all thresholds, then keep it
    c11 = row.perimeter >= thresh_peri
    c12 = row.major_axis_length >= thresh_major
    c13 = row.eccentricity >= thresh_ecc

    if c11 & c12 & c13: return True

    # if it passes 90% of thresholds for 2 conditions, then keep it
    #c21 = row.perimeter >= .9 * thresh_peri
    #c22 = row.major_axis_length >= .9 * thresh_major
    #c23 = row.eccentricity >= .9 * thresh_ecc
    #if ((c11+c12+c13)>=2) & ((c21+c22+c23)>=2): return True

    return False

  S_peri["keep"] = S_peri.apply(keep_whistle, axis=1)
  print(f"s21{{a,c}}_drop_blobs: After dropping blobs, number of regions (proportional to whistles) remaining: {S_peri['keep'].sum()}, dropped: {(~S_peri['keep']).sum()}")

  if verbose:
    # Use .head to avoid overflow in case of large number of rows
    print("Top 40 rows that pass filtering for whistle properties")
    col_skip = ["centroid-0", "centroid-1", "keep"]
    print(S_peri[S_peri["keep"]][[x for x in S_peri.columns if x not in col_skip]].head(40))

    fig = plt.figure(figsize=(20,3))
    plt.imshow(S_blur, origin="lower")

    for i in range(S_peri.shape[0]):
      l_i = S_peri.iloc[i]
      plt.scatter(l_i["centroid-1"], l_i["centroid-0"], color="yellow" if l_i.keep else "red", s=5)
      if l_i.keep:
        #plt.text(l_i["centroid-1"], l_i["centroid-0"], l_i.label, color="white")
        text_msg = f"#{l_i.label.astype(int)}: p={l_i.perimeter.astype(int)}, e={l_i.eccentricity.round(2)}, maj={l_i.major_axis_length.round(0)}, min={l_i.minor_axis_length.round(0)}"
        plt.text(l_i["centroid-1"]+5, l_i["centroid-0"], text_msg, color="white")

    plt.title("labels and perimeters")
    plt.show()


    def myplotthresh(fx, thr):
      plt.plot(S_peri[fx].sort_values().values, color="blue", marker="|")
      plt.axhline(thr, color="red")
      plt.title(f"sorted {fx} and threshold")
      plt.show()


    myplotthresh("perimeter", thresh_peri)
    myplotthresh("major_axis_length", thresh_major)
    myplotthresh("eccentricity", thresh_ecc)


  S_mask = np.full_like(S_label, fill_value=False, dtype=bool)
  for l in S_peri[S_peri.keep].label:
    S_mask = np.logical_or(S_mask, S_label==l)

  # Note returning the subset of blurred and thresholded, not subset of original
  # Update: change to return original instead of blur
  S_b2 = np.multiply(S_mask, Sxx_s13)
  # S_b2 = np.multiply(S_mask, S_blur)

  if verbose: my_imshow(S_b2, "dropped blobs")

  return S_b2

