from skimage.filters import gaussian
from skimage import measure
from scipy.ndimage import watershed_ift
import numpy as np
from matplotlib import pyplot as plt
import scipy
from dolphicom.utils import my_imshow
import random

class S22aWatershed:
  def __init__(self, Sxx_ori, Sxx_input, Sxx_markers, verbose):
    """
    Initially developed in notebook t09
    https://colab.research.google.com/drive/1gGijXCyS94lV_DyaQdc9Q9VKa-twrKim#scrollTo=p7RHw7W4ZCpx
    """
    #self.Sxx_checkpoints = Sxx_checkpoints
    #Sxx_ori = self.Sxx_checkpoints["s11_original"]
    #Sxx_input = self.Sxx_checkpoints["s12b_drop_bursts"]
    #Sxx_markers = self.Sxx_checkpoints["s22_connect_broken_whistles"]

    self.Sxx_ori = Sxx_ori
    self.Sxx_input = Sxx_input
    self.Sxx_markers = Sxx_markers

    self.verbose = verbose

    if self.verbose:
      my_imshow(self.Sxx_ori, "original (s11)")
      my_imshow(self.Sxx_markers!=0, "watershed markers != 0 (all, not just centroids, s22 connected?)")


  def calc_input(self):
    # np.copy doesn't maintain the dtype, so need to explicity cast
    # Update: s12 is no longer a csr_matrix
    #s11_norm = np.copy(self.Sxx_input.toarray()).astype(np.float64)
    s11_norm = np.copy(self.Sxx_input)

    # Method 1
    s11_norm = gaussian(s11_norm, 1)
    #s11_norm = gaussian(s11_norm, 2)

    # Method 2
    #from skimage.filters import meijering
    #s11_norm = meijering(s11_norm, black_ridges=False, sigmas=range(1, 5, 1))
    #s11_norm = meijering(s11_norm, black_ridges=False, sigmas=range(1, 15, 1))

    # normalize
    s11_norm = (s11_norm - s11_norm.min())/(s11_norm.max() - s11_norm.min()) * np.iinfo(np.uint16).max

    # FIXME do I still need this if it's based on s12 (drop bursts)? Originally needed this when basing on s11 (original)
    THRESH_NORM = .90
    thresh_val = np.quantile(s11_norm, THRESH_NORM)
    if self.verbose:
      my_imshow(s11_norm, f"watershed input before thresh")
      my_imshow(s11_norm > thresh_val, f"watershed input > q90% = {thresh_val}")

    # Note: the conversion to boolean is not a bug
    s11_norm = s11_norm > thresh_val
    #s11_norm = np.multiply(s11_norm, s11_norm > thresh_val)

    s11_norm = s11_norm.astype(np.uint16)

    if self.verbose:
      #plt.imshow(s11_norm, cmap='gray', origin="lower")
      my_imshow(s11_norm, "watershed input")

    self.input = s11_norm


  def calc_markers(self):
    # method 1: get centroids
    #s22_label = measure.label(self.Sxx_markers)
    #s22_centroids = [x.centroid for x in measure.regionprops(s22_label)]
    #s22_centroids = [(int(x),int(y)) for x,y in s22_centroids]
    #s22_centroids = [(x,y) for x,y in s22_centroids if self.input[x,y]!=0]

    # method 2: all s22 points
    # Note: could perhaps sample points from here to save time if this proves to be heavy
    s22_centroids = np.where(self.Sxx_markers)
    s22_centroids = zip(s22_centroids[0], s22_centroids[1])
    s22_centroids = [(x,y) for x,y in s22_centroids if self.input[x,y]!=0]

    # method 3: first, central, and last of each label
    #s22_label = measure.label(self.Sxx_markers!=0)
    #s22_centroids = []
    #for l in range(1, s22_label.max()+1):
    #  pall = np.where(s22_label==l)
    #  pall = zip(pall[0], pall[1])
    #  pall = [(x,y) for x,y in pall if self.input[x,y]!=0]
    #  pall = sorted(pall, key=lambda x: (x[1],x[0]))
    #  if len(pall) > 3: pall = [pall[0], pall[len(pall)//2], pall[-1]]
    #  s22_centroids += pall

    if self.verbose: print(f"{len(s22_centroids)} centroids, eg {s22_centroids[:10]}")

    # cannot do np.zeros_like(self.Sxx_22...) because it's a csr_matrix
    # Update: no longer csr_matrix, but keeping this as is anyway
    s_markers = np.zeros(shape=self.Sxx_markers.shape, dtype=np.int16)

    # choose an empty point in the input and mark it with 1
    # High frequency is likely 0, so try that as a shortcut
    if self.input[-1, 0] == 0:
      s_markers[-1, 0] = 1
    else:
      zero_x, zero_y = np.unravel_index(np.argmax(self.input==0), self.input.shape)
      if self.input[zero_x, zero_y] != 0:
        raise Exception("Found no 0s in spectrogram. Aborting")

      s_markers[zero_x, zero_y] = 1

    # mark the rest with 2
    for idx_f_i, idx_t_i in s22_centroids:
      s_markers[idx_f_i, idx_t_i] = 2

    self.markers = s_markers

    if self.verbose:
      plt.figure(figsize=(20,3))
      plt.imshow(self.input, origin="lower")
      # subset for 100 random points to avoid slow scatter
      n_show = 200
      s22_cent_sub = s22_centroids if len(s22_centroids) < n_show else random.sample(s22_centroids, n_show)
      for idx_f_i, idx_t_i in s22_cent_sub:
        plt.scatter(idx_t_i, idx_f_i, color="red", alpha=.5, s=10)
      plt.title("watershed input with markers in red")
      plt.colorbar()
      plt.show()


  def calc_watershed(self):
    # Update: s12 is no longer a csr_matrix
    #s_in_raw = np.copy(self.Sxx_input.toarray()).astype(np.float64)
    s_in_raw = np.copy(self.Sxx_input)
    ws_mask = watershed_ift(self.input, self.markers)
    self.watershed = np.multiply(ws_mask==2, s_in_raw)
    #self.watershed = ws_mask

    if self.verbose:
      my_imshow(self.Sxx_ori, "original (s11)")
      my_imshow(self.Sxx_markers!=0, "watershed markers != 0 (all, not just centroids, s22 connected?)")
      my_imshow(ws_mask==2, "watershed mask == 2")
      my_imshow(self.watershed, "watershed result (mask * ori)")
      my_imshow(self.watershed!=0, "watershed result != 0")
