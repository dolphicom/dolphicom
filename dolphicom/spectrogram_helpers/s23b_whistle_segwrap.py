"""
Wrap whistle_segmenter.WhistleSegmenter such that it mends whistles together that are "close" to each other
"""

from skimage.draw import line as draw_line
from .s23a_whistle_segmenter import WhistleSegmenter
import numpy as np
#from ..utils import iloc_nocoerce
import pandas as pd
from skimage.filters import meijering # gaussian
from ..utils import iter_batch, np_index_at_value
import scipy


euclidean_distance = lambda x1, y1, x2, y2: ((x2-x1)**2+(y2-y1)**2)**.5

# Note the need for int(...) casting because of pandas changing the dtype.
# Check note with itertuples below for an alternative, but it requires changing the ".apply(...)" to "[... for w2 in ...itertuples(...)]"
# Update: implemented and dropped int at several places
#wrap_np_where = lambda Sxx_filt, wh_i, t_idx_xxx: wh_i.f_idx_min + int(np.where(Sxx_filt[wh_i.f_idx_min:wh_i.f_idx_max+1, t_idx_xxx])[0].mean())

F_SCALE = 15000




class WhistleComparator:
  """
  Example
    from dolphicom.spectrogram_helpers.s23b_whistle_segwrap import WhistleComparator
    w1 = p.sp_filt.whistle_corpus.iloc[56]
    w2 = p.sp_filt.whistle_corpus.iloc[58]
    assert WhistleComparator(w1, w2, p.sp_filt, p.sp_filt.whistle_corpus).get_edges().is_close()

    w3 = p.sp_filt.whistle_corpus.iloc[57]
    assert WhistleComparator(w1, w3, p.sp_filt, p.sp_filt.whistle_corpus).get_edges().is_close()
  """
  def __init__(self, w1, w2, sp_filt, whistle_corpus2):
    self.w1, self.w2 = w1, w2
    self.sp_filt = sp_filt
    self.whistle_corpus2 = whistle_corpus2


  def get_edges(self):
    Sxx_filt, wc = self.sp_filt.Sxx, self.whistle_corpus2

    # in the plot_range function plot, 1 sec = 15khz in screen distance, so scale here
    w1, w2 = self.w1, self.w2

    # Update: instead of using wrap_np_where on Sxx_filt (which will include noise other than the pure whistle), use the w1.S
    #f_idx_start = wrap_np_where(Sxx_filt, w1, w1.t_idx_max)
    #f_idx_end = wrap_np_where(Sxx_filt, w2, w2.t_idx_min)

    # Computing f_idx_{start,end} from w{1,2}.S
    # FIXME Note that in case of a whistle that is falsely displaying a branch,
    # then the mean below will select a point that might be between the 2 branches,
    # hence not belonging to either branch.
    f_idx_start = w1.f_idx_min + int(np.where(~np.isnan(w1.S[:,-1]))[0].mean())
    f_idx_end   = w2.f_idx_min + int(np.where(~np.isnan(w2.S[:, 0]))[0].mean())

    # to hertz
    f_hz_start = self.sp_filt.f[f_idx_start]
    f_hz_end = self.sp_filt.f[f_idx_end]

    self.edges = {
      "t_idx_start": w1.t_idx_max,
      "t_sec_start": w1.t_sec_max,
      "f_idx_start": f_idx_start,
      "f_hz_start": f_hz_start,
      "t_idx_end": w2.t_idx_min,
      "t_sec_end": w2.t_sec_min,
      "f_idx_end": f_idx_end,
      "f_hz_end": f_hz_end,
    }
    return self


  # visually choosing threshold = 0.1 by using 35s-40s as a prototype
  def is_close(self):
    # in the plot_range function plot, 1 sec = 15khz in screen distance, so scale here
    x1, y1, x2, y2 = self.edges["t_sec_start"], self.edges["f_hz_start"], self.edges["t_sec_end"], self.edges["f_hz_end"]
    d = euclidean_distance(x1, y1/F_SCALE, x2, y2/F_SCALE)
    o = (d <= 0.1)
    return o



# Update 2021-06-10: cannot import spectrogramPlotter here ... circular dependency
#from ..spectrogram import Spectrogram
class WhistleSegWrap:
  #def __init__(self, sp_filt: Spectrogram):
  def __init__(self, sp_filt, verbose, include_hdbscan=True):
    self.sp_filt = sp_filt
    self.verbose = verbose
    self.include_hdbscan=include_hdbscan


  def blur(self):
    # blur so that hdbscan in s23a doens't detect "constricted" segments of whistles as separation points
    # Alternatively, could have added the "2d slope" as a feature (which was still in testing but abandoned in notebook t07.2e)
    # Update: use a meijering filter instead of gaussian. This avoids thickening all the line together, but thickens along the direction of the whistle
    #self.sp_filt.Sxx = gaussian(self.sp_filt.Sxx, 1)

    def my_blur(Sxx_sub):
      Sxx_blur1 = meijering(Sxx_sub, black_ridges=False, sigmas=range(1, 5, 1))
      # to avoid 1-pixel entries
      Sxx_blur1[Sxx_blur1 < Sxx_blur1.max()/10] = 0
      return Sxx_blur1

    # Update: mobysound.org have different sampling rates
    sps = np_index_at_value(self.sp_filt.t, 1)

    b_size = 120*sps
    gen = iter_batch(self.sp_filt.Sxx, b_size, "s23b/blur (meijering)")
    Sxx_l = [my_blur(Sxx_i) for _, _, Sxx_i in gen]
    self.sp_filt.Sxx = np.concatenate(Sxx_l, axis=1)
    print("s23b/blur (meijering) done, continuing pipeline with hdbscan, etc")


  def segment_init(self):
    # initial segmentation
    t, f, Sxx_filt = self.sp_filt.t, self.sp_filt.f, self.sp_filt.Sxx
    ws = WhistleSegmenter(t, f, Sxx_filt, verbose=self.verbose)
    ws.wide_to_long()
    if self.include_hdbscan:
        ws.label_hdbscan()
    else:
        ws.label_skimage()

    if self.verbose: ws.print_stats()

    # don't drop anything at this stage
    ws.build_corpus(filter_noise_blobs=False)
    #ws.filter_corpus()
    self.ws_init = ws


  def get_close(self):
    wc = self.ws_init.whistle_corpus2
    cl_all = []

    # Use itertuples to maintain int dtype https://stackoverflow.com/questions/48722661/how-to-preserve-the-datatype-while-iterating-dataframe-in-pandas#48722907
    for wh_i in wc.itertuples(index=False):
      #wh_i = wc.iloc[i]
      #wh_i = iloc_nocoerce(wc, i)
      wh_comp = self._get_close_single(wh_i)
      if wh_comp is None: continue

      # Use itertuples to maintain int dtype https://stackoverflow.com/questions/48722661/how-to-preserve-the-datatype-while-iterating-dataframe-in-pandas#48722907
      # Also note how this could make multiple connections from a starting point
      for wh_j in wh_comp.itertuples(index=False):
        d = {"w1_id": wh_i.hdbscan_label, "w2_id": wh_j.hdbscan_label}
        d.update(wh_j.comparator.edges)
        cl_all.append(d)

    print(f"Will make {len(cl_all)} new connections (mended(2))")
    self.cl_all = pd.DataFrame(cl_all)

    if self.cl_all.shape[0] > 0:
      print("Statistics (probably mostly 1 connection per whistle, with few 2 connections per whistle)")
      print(self.cl_all.w1_id.value_counts().value_counts())


  def _get_close_single(self, wh_i):
      wc = self.ws_init.whistle_corpus2
      
      # Initial filtering based on bounding box, with further check in WhistleComparator.is_close below
      # Update: improving this below
      #idx_comp = (
      #      (wc.t_sec_min >= (wh_i.t_sec_min - .5))
      #    & (wc.t_sec_max <= (wh_i.t_sec_max + .5))
      #    & (wc.hdbscan_label != wh_i.hdbscan_label)
      #  )

      # Initial filtering based on bounding box, with further check in WhistleComparator.is_close below
      # Improved version that checks both time and frequency and is more correct.
      # Btw could sort dataframe and use a more efficient filtering then nested for loops
      idx_comp = (
          (
              (wc.t_sec_max >= (wh_i.t_sec_min - .25))
            | (wc.t_sec_min <= (wh_i.t_sec_max + .25))
          ) & (
              (wc.f_hz_max >= (wh_i.f_hz_min - 2000))
            | (wc.f_hz_min <= (wh_i.f_hz_max + 2000))
          ) & (
            wc.hdbscan_label != wh_i.hdbscan_label
          )
        )

      if not idx_comp.any(): return None
      wh_comp = wc[idx_comp].copy()

      #wh_comp["comparator"] = wh_comp.apply(lambda w2: WhistleComparator(wh_i, w2, self.sp_filt, wc).get_edges(), axis=1)
      wh_comp["comparator"] = [WhistleComparator(wh_i, w2, self.sp_filt, wc).get_edges() for w2 in wh_comp.itertuples(index=False)]
      d_comp = wh_comp.comparator.apply(lambda comp: comp.is_close())
      if not d_comp.any(): return None
      return wh_comp[d_comp]



  def connect(self):
    if self.cl_all.shape[0] == 0: return

    # draw thick line in sp_filt.Sxx, like mending with lines from hough transform

    #Sxx_filt = ws.Sxx_mask # for immediate testing and plotting
    Sxx_filt = self.sp_filt.Sxx # p.wm.sp_filt.Sxx # for proper calculation
    minmax_sxx = lambda idx, axis: max(0, min(Sxx_filt.shape[axis]-1, idx))

    for cl_i in self.cl_all.itertuples(index=False):
        t_start, f_start, t_end, f_end = cl_i.t_idx_start, cl_i.f_idx_start, cl_i.t_idx_end, cl_i.f_idx_end

        #fill_value = wh_i.hdbscan_label # for immediate testing and plotting
        f_max, t_max = Sxx_filt.shape
        fill_value = Sxx_filt[max(0,f_start-10):min(f_max, f_start+10), max(0,t_start-10):min(t_max, t_start+10)].max() # for proper calculation
        for i3 in range(-3,3,1):
          Sxx_filt[draw_line(minmax_sxx(f_start+i3, 0), minmax_sxx(t_start+i3, 1), minmax_sxx(f_end+i3, 0), minmax_sxx(t_end+i3, 1))] = fill_value

    # ws.Sxx_mask = Sxx_filt # for testing
    #return Sxx_filt
    self.sp_filt.Sxx = Sxx_filt


  def segment_repeat(self):
    # If no mended(2) connections made, then no need to re-run segmentation
    # but ATM leaving it as such to avoid having to manually populate ws from ws_init
    # Simple "    if self.cl_all.shape[0] == 0: ws = self.ws_init" will just set ws to be a reference to the original ws_init instance
    # and hence will not keep a copy "before filtering" and another "after filtering" for later debugging

    # Repeat segmentation after possibly making whistle connections
    t, f, Sxx_filt = self.sp_filt.t, self.sp_filt.f, self.sp_filt.Sxx
    ws = WhistleSegmenter(t, f, Sxx_filt, verbose=self.verbose)
    ws.wide_to_long()

    if self.include_hdbscan:
        ws.label_hdbscan()
    else:
        ws.label_skimage()

    if self.verbose: ws.print_stats()

    # now can drop
    ws.build_corpus(filter_noise_blobs=True)
    ws.filter_corpus()

    # until I factor out the field names "l" and "hdbscan_label" here and in whistle_segmenter.py, just processing them here
    if ws.whistle_corpus2.shape[0] > 0:
      del ws.whistle_corpus2["l"]
      ws.whistle_corpus2.rename(columns={"hdbscan_label": "label_whistle"}, inplace=True)

    # done
    self.ws_final = ws


  def remove_intermediate_variables(self):
    """
    Removing the below variables reduces the pipeline.to_pickle filesize from 3G to 2G for the 1h audio file of yt-q1
    """
    del self.sp_filt

    del self.ws_init.Sxx_filt
    del self.ws_init.Sxx_mask
    del self.ws_init.wc_long
    del self.ws_init.Sxx_skimage

    del self.ws_final.Sxx_filt
    del self.ws_final.Sxx_mask
    del self.ws_final.wc_long
    del self.ws_final.Sxx_skimage


  def to_dict(self):
    # if any AbortPipeline was raised during the execution,
    # then this class will still have sp_filt member (since no call to the remove_intermediate_variables function above)
    # Update: just add hasattr checks below on each member
    # if hasattr(self, "sp_filt"): return None

    d5 = {}

    if hasattr(self, "ws_init"):
      d5["ws_init__whistle_corpus"] = self.ws_init.whistle_corpus2

    if hasattr(self, "cl_all"):
      d5["cl_all"] = self.cl_all

    if hasattr(self, "ws_final"):
      d5["ws_final__whistle_corpus"] = self.ws_final.whistle_corpus2

    return d5


  @classmethod
  def from_dict(cls, d5, sp_filt):
    self = cls(sp_filt, False)

    # dummy value for Sxx_filt
    if "ws_init__whistle_corpus" in d5:
      Sxx_dummy = sp_filt.Sxx
      if type(Sxx_dummy) == scipy.sparse.csr_matrix: Sxx_dummy = Sxx_dummy.toarray()
      self.ws_init  = WhistleSegmenter(sp_filt.t, sp_filt.f, Sxx_dummy, False)
      self.ws_init.whistle_corpus2 = d5["ws_init__whistle_corpus"]

    # connections
    if "cl_all" in d5:
      self.cl_all   = d5["cl_all"]

    if "ws_final__whistle_corpus" in d5:
      # dummy value for Sxx_filt
      self.ws_final  = WhistleSegmenter(sp_filt.t, sp_filt.f, Sxx_dummy, False)
      self.ws_final.whistle_corpus2 = d5["ws_final__whistle_corpus"]

    return self
