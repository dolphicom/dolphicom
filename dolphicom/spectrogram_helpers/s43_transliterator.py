import math
from skimage import measure
from scipy import interpolate
from ..utils import close_to_zero, np_apply_piecewise_notnan
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


class S43WhistleToBraille:

    def __init__(self, verbose=False):
        self.verbose = verbose

    def oneshot(self, S_1d, standard_scale=True):
        if np.isnan(S_1d).any(): return ""
        if S_1d.shape[0] <=3: return ""

        S_sc = np.copy(S_1d)
        #S_sc = pd.Series(S_sc).fillna(method="ffill").fillna(method="bfill").values

        if standard_scale: S_sc = (S_sc - S_sc.min())/(S_sc.max() - S_sc.min())

        # scale from standard to 3 so that braille output is one line (i.e. just the 4 rows in braille)
        S_sc = S_sc*-3 + 3 # flip sign and offset to get to positive
    
        # When this is 20 => downsample in steps of 20
        # Update: block_reduce is not needed with the below compression
        """
        #TIME_NEWSTEPS = 20
        TIME_NEWSTEPS = 10 # Since compression of uninteresting events below, can make this smaller
        S_sc = measure.block_reduce(S_sc, (TIME_NEWSTEPS,), np.nanmax)
        """

        if self.verbose:
          fig, ax = plt.subplots(1,3, figsize=(20,3))
          
          ax[0].plot(S_sc, marker="|")
          ax[0].set_ylim(0,3)
          ax[0].set_title("scaled, flipped, downsampled")
    
        # COMPRESS
        # get gradient and use it to compress uninteresting events
        # Same as in slashes converter
        gs = np.diff(S_sc, prepend=S_sc[0]-(S_sc[1]-S_sc[0]))

        # smooth to reduce gitter
        #gs = pd.Series(gs).rolling(10, center=True).mean().fillna(method="ffill").fillna(method="bfill").values

        # small changes in 1st order difference don't matter
        idx_pos = gs > 0

        if self.verbose:
          ax[1].scatter(np.where( idx_pos)[0], gs[ idx_pos], color="green", s=5)
          ax[1].scatter(np.where(~idx_pos)[0], gs[~idx_pos], color="red",   s=5)
          ax[1].axhline(y=0, color="blue")
          ax[1].plot(gs, color="blue")
          ax[1].set_title("diff")

        # we only care about up down flat, not more
        gs=np.sign(gs).astype(int)

        # filter the "interesting" points to drop "short windows"
        # first and last are always interesting
        mask_l = [gs==1, gs==0, gs==-1]
        df_int = self._filter_short_windows(mask_l, "interesting")

        # now the events we want are the ones that are not short AND that are different than the previous event
        idx_notshort = ~df_int.is_short
        df_sub = df_int[idx_notshort].copy()
        df_sub["mid_prev"] = df_sub.mask_id.shift(1).fillna(0).astype(int)
        df_sub["keep"] = df_sub.eval("(mask_id!=mid_prev)")

        # copy back to df_int
        df_int["keep"] = False
        df_int.loc[idx_notshort, "keep"] = df_sub["keep"].values

        # get start and end indices + append start and end points
        #get_indices = lambda df: sorted(df.start.tolist() + (df.end-1).tolist() + [0] + [gs.shape[0]-1])
        get_indices = lambda df: sorted(list(set(df.start.tolist() + [0] + [gs.shape[0]-1])))
        idx_int1 = get_indices(df_int)
        idx_int2 = get_indices(df_int[df_int.keep])

        if self.verbose:
          ax[2].plot(S_sc)
          ax[2].scatter(idx_int1, S_sc[idx_int1], color="green", alpha=.5, label="ignored")
          ax[2].scatter(idx_int2, S_sc[idx_int2], color="red", alpha=.5, label="honored")
          ax[2].legend()
          ax[2].set_title("interesting points")

        S_sc = S_sc[idx_int2]
            
        if self.verbose:
          #print(f"Interesting values: {S_sc}")
          plt.show()

        # convert to braille
        import drawille
        c = drawille.Canvas()
        for x, y in enumerate(S_sc.tolist()): c.set(x, y)
        S_br = c.frame()
        assert '\n' not in S_br # make sure we're getting 1 line

        if self.verbose: print(f"braille: {S_br}")
        return S_br


    def _filter_short_windows(self, mask_l, description):
        """
        idx_interesting: np.array of bool
        """
        assert type(mask_l)==list
        for mask_i in mask_l: assert mask_i.dtype == bool

        # Method 1: drop windows clustered as "small" by clustering the lengths into 2 groups: close to 0, not close to 0
        # Update: deprecated since not robust
        # for "v" below (value at window center), to get "0" when two windows at +1 and -1 are consecutive,
        # and the current window is the middle one (which should be ignored in the quantile calculation)
        # we only care about changes in up down flat
        #idx_interesting = np.diff(gs, prepend=gs[0]-(gs[1]-gs[0])) != 0
        #
        #gs_interpolater = interpolate.interp1d(np.arange(gs.shape[0]), gs)
        #
        #df_int = pd.DataFrame({"start": np.where(idx_interesting)[0].astype(int)})
        #df_int["end"] = df_int.start.shift(-1)
        #df_int = df_int.iloc[:-1] # drop last na row
        #df_int["end"] = df_int["end"].astype(int)
        #df_int["len"] = df_int.eval("end-start")
        #df_int["center_value"] = df_int.apply(lambda r: gs_interpolater((r.start+r.end-1)/2), axis=1) # intentionally keep fraction and use interpolater
        #df_int["is_win"] = (df_int.center_value!=0) & (df_int.center_value!=0.5)
        #is_short = close_to_zero(df_int.len[df_int["is_win"]].tolist())
        #df_int["is_short"] = False
        #df_int.loc[df_int.is_win, "is_short"] = is_short
        #df_int["is_short"] = df_int["is_short"].astype(bool) # pandas cannot guess it's a bool if is_short==[]
        #df_int["keep"] = df_int.eval("is_win & (~is_short)")

        # Method 2: Get start/end, of windows in the masks
        # https://numpy.org/doc/stable/reference/generated/numpy.ma.clump_masked.html
        df_int = []
        for i, mask_i in enumerate(mask_l):
          slices = np.ma.clump_masked(np.ma.masked_where(mask_i, mask_i))
          df_int = df_int + [{"start": s.start, "end": s.stop, "mask_id": i} for s in slices]

        # important to sort so that events get interleaved
        df_int = pd.DataFrame(df_int).sort_values(["start", "end"])
        df_int["len"] = df_int.eval("end-start")
        df_int["is_short"] = close_to_zero(df_int.len.tolist())

        if self.verbose:
          print("")
          print(f"Dataframe of filter short windows: {description}")
          print(df_int)

        return df_int
        

    def _fill_short_nan(self, S_1d):
      """
      find which nan windows are too short, and just fillna on them
      """
      S_sc = np.copy(S_1d)

      # pass both non-nans and nans to _filter_short_windows so it can use the non-nans as benchmark for lengths
      mask_l = [np.isnan(S_1d), ~np.isnan(S_1d)]
      df_int = self._filter_short_windows(mask_l, "_fill_short_nan")
      df_int = df_int[df_int.mask_id==0] # keep the isnan indices
      
      # update: do linear interpolation isntead of ffill and bfill cause .diff to give 0's and hence increase gitter
      #S_filled = pd.Series(S_sc).fillna(method="ffill").fillna(method="bfill").values
      S_filled = pd.Series(S_sc).interpolate()
      for r in df_int.itertuples():
        if not r.is_short: continue # keep these nan
        if np.abs(S_filled[r.start] - S_filled[r.end-1]) >= .25: continue # jump is "too large"
        S_sc[r.start:r.end] = S_filled[r.start:r.end] # end is not included

      return S_sc


    def piecewise(self, S_1d):
      """
      To avoid the fillna
      """
      if np.isnan(S_1d).all(): return ""
      
      S_sc = np.copy(S_1d)

      S_sc = self._fill_short_nan(S_sc)
        
      # Scale before splitting into pieces
      if np.nanmax(S_sc) == np.nanmin(S_sc):
        S_sc[~np.isnan(S_sc)] = 0 # just flat
      else:
        # Update: instead of scaling by min/max, scale to min/10000 Hz
        # S_sc = (S_sc - np.nanmin(S_sc))/(np.nanmax(S_sc) - np.nanmin(S_sc))
        # where 10k is the max length of a whistle in frequency, determined from data
        # Update: some whistles go beyond 10khz in length (eg to 15khz), so scale to 10khz if len is < 10khz, otherwise scale to the actual length, which is equivalent to clipping at 10khz
        # Update: better do 5khz instead of 10khz in order to transliterate pattern of smaller whistles and not just transliterate as flat
        max_freq_len = 5000 # 10000
        actual_freq_len = np.nanmax(S_sc) - np.nanmin(S_sc)
        S_sc = (S_sc - np.nanmin(S_sc))/max(max_freq_len, actual_freq_len)
        assert np.nanmax(S_sc) <= 1 # check that indeed no whistle has frequency coverage more than 10khz


      # find which non-nan windows are too short to be significant
      # Update: non-nans should be considered short or not relative to nans as well,
      # so if there are too many nans, then short non-nans get ignored
      # This only harms very fast-up whistles if they are not cleanly segmented
      #df_int = self._filter_short_windows([~np.isnan(S_1d)], "non-nan")
      df_int = self._filter_short_windows([~np.isnan(S_1d), np.isnan(S_1d)], "non-nan")
      min_len = df_int.len[~df_int.is_short].min()
    
      #func_isnan = lambda v: [" "*math.ceil(v.shape[0]/20)] # downsample and return spaces
      func_isnan = lambda v: [] # nothing since I'm compressing
      func_notnan = lambda v: [self.oneshot(v, False)] if v.shape[0] >= min_len else []
      o = np_apply_piecewise_notnan(S_sc, func_isnan, func_notnan, False)
      return "".join(o.tolist()).strip()



def s43_replace_similar_br(S_str, verbose):
  # Merge similar representations
  # Useful to construct this:
  #     np.array(sorted(list(set(S_1d_str__counts.head(30).reset_index()["index"].tolist()))))
  # Obtained after the value_counts call below
  similar_braille = [
     ['\\', '⠑⡀', '⠢⡀', '⠡⡀', '⢁'],
     ['/', '⡐', '⡐⠁', '⡠', '⡠⠁', '⡠⠁', '⡠⠂', '⡠⠂', '⡀⠁', '⡈',],

     ['_/', '⣀⠁', '⣀⠂', '⣀⠂', '⣀⠄', '⣀⠉', '⣀⡈', '⣀⣀⠁'],
     ['\\_', '⠢⠄', '⢄⡀', '⠂⣀', '⢂⡀', '⢁⡀', '⢁⣀', '⢁⣀⡀', ],
     ['/‾', '⡈⠁',  '⡐⠂', '⡠⠄', '⡈⠉'],

     ['‾\\', '⠒⠄', '⠒⡀', '⠤⡀', '⠉⡀', '⠉⢁',],
     ['-', '⣀', '⣀⡀'],

     ['\\_/', '⠢⠁', '⠢⠂', '⢄⠁', '⢄⠂', '⢄⠄', '⠄⣀⠂', '⢂⠁', '⢁⠁', '⢁⠂', '⢁⠄', '⢁⣀⣀⣀⠄'],
     ['/‾\\', '⡈⠂',  '⡈⠄', '⡈⡀', '⡐⠄', '⡐⡀', '⡠⡀', '⠔⡀', '⠊⡀', '⠌⡀', '⠌⣀', '⡈⠂'],
     ['/-/', '⡐⠊', '⡠⠌'], 

     ['/\\/\\', '⠊⡀⡐⠄', '⠊⡈⠂', '⠊⡈⠌', '⠊⡐⠄', '⠌⠌⠄', '⠌⡠⠊⡀', '⠌⢄⡀⠡', '⡈⠌⠂', '⡈⠔⠒⠑', '⡈⡐⠄', '⡈⡐⡀', '⡠⠄⠔⡈⠂', '⢂⢁⠡', '⣀⢁⠑'],
     ['/\\/', '⠊⡐', '⠊⡠', '⠌⡠', '⡈⠌', '⡈⡐',],

  ]

  def my_replace(b):
    for s in similar_braille:
      if b in s: return s[0]
    return b

  S_str = S_str.copy().apply(my_replace)

  if verbose:
    S_1d_str__counts = S_str.value_counts()
    print(S_1d_str__counts.head(30).tail(30))

    S_1d_str__counts.plot()
    #S_1d_str__counts.n_pair.plot()
    plt.axvline(x=15, color="red")
    plt.text(15+3, 10, "x=15", color="red")


  return S_str


def plot_s43_braille_examples(whistle_corpus3):
  for idx_test in whistle_corpus3.index.values[:10]:
    wh_i = whistle_corpus3.loc[idx_test]
    S_braille = S43WhistleToBraille(False).piecewise(wh_i.S_1d)
    
    plt.plot(wh_i.S_1d)
    plt.title(idx_test)
    plt.title(f"loc {idx_test}: {S_braille}")
    plt.show()
