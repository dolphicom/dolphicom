# testing
from dolphicom.utils import my_imshow
from scipy import ndimage
import numpy as np
from skimage.filters import gaussian

# np.multiply: element-wise multiplication
def s12_drop_bursts(S_in, s1, s2, hard, verbose):
      if verbose: print(f"s21_drop_bursts hard={hard}, s1={s1}, s2={s2}")
      if verbose: my_imshow(S_in, "input")

      # Add an artificial vertical burst that gets easily removed by this step
      # This is necessary because in case there is no noise (i.e. clean S_in)
      # this step will drop the real whistle.
      # Set it to the max. If max is a burst, then it gets removed along with the other burst.
      # If max is a whistle, the whistle will prosper because this artificial burst will absorb the removal
      # Note that this addition will not be visible when plotting because it is at time index 0
      # Do this just for the mask
      # FIXME Note: might want to limit the artificial burst to just hard=True .. just a thought that needs testing
      S_in2 = np.copy(S_in)
      S_in2[:s1, :s2] = S_in2.max()

      # create a mask
      S_mask = ndimage.uniform_filter(S_in2, size=(s1,s2), mode="nearest")

      if verbose: my_imshow(S_mask, "mask initial")


      if hard:
        # keep large values only, i.e. 90% * the max (same as 99% quantile)
        #thresh_val = np.quantile(S_mask,.99)*99/100
        thresh_val = S_mask.max()*1/4
        if verbose: print(f"thresholding at {thresh_val}")
        S_mask[S_mask<thresh_val]=0

        # Blur the mask a bit to capture small pixels missed by the filter above
        #S_mask = gaussian(S_mask, .5)

        # normalize
        #S_mask = S_mask/S_mask.max()

        if verbose: my_imshow(S_mask, "mask postprocessed with hard=True")
        if verbose: my_imshow(S_mask!=0, "mask!=0 postprocessed with hard=True")

      # Vanilla subtraction of mask locations. Deprecated in favor of below average subtraction
      #mask_max = S_mask.max()
      #S_out = np.multiply(S_in, 1-S_mask/mask_max)

      # Note: This is not intended to be a vanilla subtraction of locations in S_in that have high mask values
      # Instead, this computes the "average" from the uniform_filter above, and keeps in S_in what is above the average
      # Update: using the average for non-hard drop, and the subtraction for hard drop
      if hard:
        # No need for !=0 since normalized
        S_drop = np.multiply(S_in, S_mask!=0)
        #S_drop = np.multiply(S_in, S_mask)
        if verbose: my_imshow(S_drop, "output multiplied by mask")

        S_out = S_in - S_drop

        if verbose: my_imshow(S_out, "output minus output * mask")

      else:
        S_out = np.maximum(0, S_in - S_mask)

      # drop small values
      S_out[S_out<np.quantile(S_out,.99)*1/10]=0

      # drop artificial burst. Doesn't remove real whistle because these are very low frequencies
      # Update: no need since only applied the artificial burst to S_in2 for S_mask
      # S_out[:s1, :s2] = 0

      if verbose: my_imshow(S_out, "output")
      if verbose: my_imshow(S_in, "input")
      if verbose: my_imshow(S_out!=0, "output!=0")
      if verbose: my_imshow(S_in, "input!=0")
      return S_out

