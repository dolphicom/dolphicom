# Connect curves with hough transform
# https://scikit-image.org/docs/dev/auto_examples/edges/plot_line_hough_transform.html
# probabilistic hough transform section

from skimage.transform import probabilistic_hough_line
from matplotlib import cm
import pandas as pd
from skimage.feature import canny
import numpy as np
from matplotlib import pyplot as plt
from tqdm.auto import tqdm
from skimage.draw import line as draw_line
from ..utils import np_index_at_value



pd_atan = lambda s: np.arctan(s) * 180 / np.pi


class WhistleConnector:
  def __init__(self, t, f, S):
    if np.isnan(S).any():
      raise ValueError("Error in s22.WhistleConnector: Please replace nan values from Sxx before passing to this function")

    assert len(t.shape)==1
    assert len(f.shape)==1
    assert len(S.shape)==2
    assert t.shape[0] == S.shape[1]
    assert f.shape[0] == S.shape[0]
    self.t = t
    self.f = f
    self.Sxx = S


  def get_canny_edges(self):
    # https://scikit-image.org/docs/dev/auto_examples/edges/plot_canny.html
    # https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.canny
    #self.edges = canny(self.Sxx, 2, .15, .25, use_quantiles=True)
    self.edges = canny(self.Sxx, 2) # canny returns dtype=bool
    if (~self.edges.any()) and (self.Sxx!=0).any():
      print(f"[WARNING] For time range {self.t[0].round(1)}..{self.t[-1].round(1)}, skimage.features.canny returned an empty matrix even though the spectrogram has some content")
      #plt.imshow(self.Sxx!=0)
      #plt.title("Sxx with some content despite canny edges being empty")
      #plt.show()


  def get_hough_lines(self):
    edges = self.edges
    if (~edges.any()):
      # no edges to begin with
      self.cood_all = []
      return False

    # Classic straight-line Hough transform
    # Set a precision of 0.5 degree.
    tested_angles = np.linspace(-np.pi / 2, np.pi / 2, 360, endpoint=False)

    # line_gap not same as THRESH_LEN below
    # Repeat multiple times because this is random, so as to reduce chances of missing lines
    lines_l = set()
    n_prev, n_same = 0, 0
    #l_ncurr = []
    MAX_SAME, MAX_ALL = 20, 100
    for i in range(MAX_ALL):
      lines_i = probabilistic_hough_line(edges, threshold=10, line_length=5, line_gap=3, seed=i)
      lines_i = [ ((x0,y0), (x1,y1)) for (x0,y0), (x1,y1) in [sorted(p01) for p01 in lines_i] if x1!=x0]
      for l in lines_i: lines_l.add(l)
      n_curr = len(lines_l)
      #l_ncurr.append(n_curr)
      if n_curr == n_prev: n_same+=1
      if n_same > MAX_SAME:
        #print(f"[WARNING]: Early return from hough after plateau'ing at {n_curr} lines")
        break # break early if no new points in x iterations
      n_prev = n_curr

    #print("hough n lines per iteration")
    #plt.plot(l_ncurr)
    #plt.show()

    if len(lines_l)==0:
      self.cood_all = []
      return False

    # sort p0 and p1 first, then take set, then sort all list
    # skip  x1==x0  to avoid division by zero in slope
    cood_all = sorted(list(set(lines_l)))
    self.cood_all = cood_all
    return True


  def keep_increasing(self):
    # filter lines such that they're going to the right
    # Not sure if this is correct, but doing it for now
    cood_all = self.cood_all

    keep_l = []
    i_ref = 0
    c_prev = None
    for i in range(len(cood_all)):
      if len(keep_l)==0:
        keep_l.append(i)
        c_prev = cood_all[i]
        continue

      (x0_prev,_),(x1_prev,_) = c_prev
      (x0_curr,_),(_,_) = cood_all[i]
      assert x0_prev <= x0_curr
      if x0_curr < x1_prev: continue # this is a point that is not to the right
      # not doing this ATM # if ci.y1 < c_prev.y0_n: continue # this is a point that is not to a higher frequency
      keep_l.append(i)
      c_prev = cood_all[i]

    #print(f"Dropped connections to check from {cood_all.shape[0]} to {len(keep_l)}")
    cood_all = [cood_all[i] for i in keep_l]
    self.cood_all = cood_all


  def concat_next(self):
    cood_all = self.cood_all

    cood_all = [(x0,y0,x1,y1) for (x0,y0), (x1,y1) in cood_all] # flatten
    cood_all = pd.DataFrame(cood_all, columns=["x0","y0","x1","y1"])
    cood_all["s"] = cood_all.eval("(y1-y0)/(x1-x0)")
    cood_all["a"] = pd_atan(cood_all.s)

    cood_next = (
        cood_all
        .shift(-1, fill_value=0)
        .rename(columns = {k: f"{k}_n" for k in cood_all.columns})
    )
    cood_all = pd.concat([cood_all, cood_next], axis=1)
    cood_all = cood_all.iloc[:-1] # since last point doesn't have a "next"  

    cood_all["s_prev_next"] = cood_all.eval("(y0_n-y1)/(x0_n-x1)")
    cood_all["a_prev_next"] = pd_atan(cood_all.s_prev_next)
    cood_all["cl_p2n"]    = cood_all.eval("abs(a-a_n)") # close_prev_2_next
    cood_all["cl_b2n"] = cood_all.eval("abs(a_prev_next-a_n)") # close_between_2_next

    cood_all["x_mid"] = cood_all.eval("(x1+x0_n)/2").astype(np.int64)
    cood_all["y_mid"] = cood_all.eval("(y1+y0_n)/2").astype(np.int64)

    # add time units and frequency
    t, f = self.t, self.f
    cood_all["t1"]   = t[cood_all.x1.values]
    cood_all["t0_n"] = t[cood_all.x0_n.values]
    cood_all["f1"]   = f[cood_all.y1.values]
    cood_all["f0_n"] = f[cood_all.y0_n.values]
    cood_all["t_mid"] = t[cood_all.x_mid.values]
    cood_all["f_mid"] = f[cood_all.y_mid.values]

    cood_all = cood_all.round(2)
    cood_all[["f0_n","f1"]] = cood_all[["f0_n","f1"]].round(0)

    self.cood_all = cood_all


  def keep_connections(self):
    cood_all = self.cood_all
    if cood_all.shape[0]==0:
      self.cood_connect = pd.DataFrame([])
      return

    # keep connections that are "mid-air", i.e. not lying on a non-zero part of the edge
    keep_midair = [(self.Sxx[(max(0,cood_i.y_mid-10)):(cood_i.y_mid+10), cood_i.x_mid] == 0).all() for cood_i in cood_all.itertuples(index=False)]
    cood_all = cood_all[keep_midair]

    # keep connections whose lines are close in slope and within a threshold distance
    THRESH_ANGLE = 15 # degrees
    THRESH_LEN = 20
    cood_connect = cood_all[(cood_all.cl_p2n < THRESH_ANGLE) & (cood_all.cl_b2n < THRESH_ANGLE)]

    # keep subset of columns
    cood_connect = cood_connect[["x1", "x0_n", "y1", "y0_n", "x_mid", "y_mid",
                                 "t1", "t0_n", "f1", "f0_n", "t_mid", "f_mid"]]

    cood_connect["l"] = (cood_connect.eval("(x0_n-x1)**2 + (y0_n-y1)**2")**.5).astype(int)
    cood_connect = cood_connect[cood_connect.l < THRESH_LEN]
    self.cood_connect = cood_connect.copy()


  def plot_connection(self):
    cood_connect = self.cood_connect
    if cood_connect.shape[0]==0:
      print("No data, so nothing to plot")
      return

    # Generating figure 1
    fig, axes = plt.subplots(1, 1, figsize=(15, 16))
    #ax = axes.ravel()
    ax = [axes]

    ax[0].imshow(self.edges, cmap=cm.gray)
    ax[0].set_ylim((self.edges.shape[0], 0))
    ax[0].set_axis_off()
    ax[0].set_title('Detected lines')

    for i in range(cood_connect.shape[0]):
        ci = cood_connect.iloc[i]
        ax[0].plot((ci.x1,ci.x0_n), (ci.y1,ci.y0_n), color="red")
        #ax[0].scatter((ci.x1,ci.x0_n), (ci.y1,ci.y0_n), color="purple")
        ax[0].scatter(ci.x1, ci.y1, color="purple")
        ax[0].scatter(ci.x0_n, ci.y0_n, color="blue")
        break

    plt.tight_layout()
    plt.show()


  def fill_blanks(self):
    cood_connect = self.cood_connect

    # fill in blanks in spectrogram
    minmax_sxx = lambda idx, axis: max(0, min(self.Sxx.shape[axis]-1, idx))
    Sxx2 = np.copy(self.Sxx)
    for ci in cood_connect.itertuples(index=False):
      #ci = cood_connect.iloc[i].astype(int)
      wrap_dl = lambda i: draw_line(minmax_sxx(ci.y1+i,0), minmax_sxx(ci.x1+i,1), minmax_sxx(ci.y0_n+i,0), minmax_sxx(ci.x0_n+i,1))
      fill_value = Sxx2[wrap_dl(0)].max()
      for i in range(-3,3,1):
        Sxx2[wrap_dl(i)] = fill_value

    self.Sxx2 = Sxx2
    return Sxx2

  def plot_filled(self):
    plt.imshow(self.Sxx2, cmap=cm.gray)
    plt.show()


##########################

def wrap_connector(t, f, Sxx, verbose):
  wc = WhistleConnector(t, f, Sxx)
  wc.get_canny_edges()
  found_any = wc.get_hough_lines()
  if not found_any:
    if verbose: print("[WARNING] no lines returned by hough")
    wc.cood_connect = pd.DataFrame([]) # since no call to keep_connections
    wc.Sxx2 = Sxx # since no call to fill_blanks
    return wc, Sxx

  wc.keep_increasing()
  wc.concat_next()
  wc.keep_connections()
  if verbose: print(f"Mended {wc.cood_connect.shape[0]} broken whistle(s)")
  Sxx2 = wc.fill_blanks()
  #wc.plot_connection()
  #wc.plot_filled()
  return wc, Sxx2


def chunked_connector(t, f, Sxx, batch_size_sec):
  # Start: 2021-06-04 21:40:52.814775
  # End: 2021-06-04 21:43:13.142620
  # ETA: ~ 2.5 minutes
  print("Chunked whistle connector")

  # samples per second, should get from "t" instead of hard-coded
  # Update: mobysound.org have different sampling rates
  # sps = 195
  sps = np_index_at_value(t, 1)

  # batch_size_sec = 5
  l_chunk = (sps*batch_size_sec) # 5 seconds per chunk
  i_range = np.arange(0, Sxx.shape[1], l_chunk)
  n_chunks = i_range.shape[0]
  print(f"Break into {n_chunks} batches of {batch_size_sec} seconds each")

  # Note: in chunked mode, I could stop wrap_connector early at "keep_connections" and just run a single "fill_blanks" here, maybe it's more efficient
  # For now, leaving it as such. FIXME
  Sxx2_l = []
  n_connect = 0
  wc_cc_l = []
  for i, i_st in enumerate(tqdm(i_range)):
    i_end = (i+1)*l_chunk
    t_i, Sxx_i = t[i_st:i_end], Sxx[:, i_st:i_end]
    # FIXME perhaps can skip the connector if np.all(Sxx==0), i.e. no whistles
    if Sxx_i.max()==0:
      #print(f"No whistles in chunk {i}. Skipping.")
      Sxx2_l.append(Sxx_i)
      continue

    wc_i, Sxx2_i = wrap_connector(t_i,f,Sxx_i,False)
    nc_i = wc_i.cood_connect.shape[0]
    n_connect += nc_i

    # correct the cood_connect time offsets by the batch offset
    if wc_i.cood_connect.shape[0] > 0:
      wc_i.cood_connect["batch_i"] = i
      wc_i.cood_connect["x1"]   += i_st
      wc_i.cood_connect["x0_n"] += i_st
      wc_cc_l.append(wc_i.cood_connect)

    Sxx2_l.append(Sxx2_i)

  print(f"Mended {n_connect} broken whistle(s)")
  Sxx2_l = np.concatenate(Sxx2_l, axis=1)
  wc_cc_l = pd.DataFrame([]) if len(wc_cc_l)==0 else pd.concat(wc_cc_l, axis=0).reset_index(drop=True)
  assert Sxx.shape == Sxx2_l.shape

  return Sxx2_l, wc_cc_l
