# https://docs.python.org/3/library/unittest.html
import unittest
import numpy as np

class TestS43Transliterator(unittest.TestCase):
    def test_filter_short_windows(self):
      from .s43_transliterator import S43WhistleToBraille
      S_1d = np.array([1,2,3,4,5,6,7,np.nan,4])
      df_int = S43WhistleToBraille(False)._filter_short_windows([ ~np.isnan(S_1d) ], "test")
      assert df_int.is_short.tolist() == [False,True]

    def test_fill_short_nan(self):
      from .s43_transliterator import S43WhistleToBraille
      S_1d = np.array([1,2,3,4,5,6,7,np.nan,7])
      np.testing.assert_almost_equal(
          S43WhistleToBraille(False)._fill_short_nan(S_1d),
          np.array([1,2,3,4,5,6,7,7,7])
        )
    


if __name__ == '__main__':
    unittest.main()
