from ..utils import np_index_at_value
from scipy.ndimage.filters import uniform_filter1d
from scipy.signal import find_peaks
from skimage.filters import gaussian
from skimage.measure import label # , regionprops_table # regionprops
from matplotlib import pyplot as plt
import numpy as np


class S14AbortNoPeriodicBlobs(Exception):
  pass


class S14DropPeriodicBlobs:
  """
  Drop periodic bursts around 10khz and happening every 8 seconds.
  This is present in the NOAA PIFSC dataset, eg file "1705/1705_20170827_174922_524_ch_1.mp3"

  Example:
    S_in = p.sp_filt.Sxx_checkpoints["s13_remove_noise"].toarray().copy()

    s14 = S14DropPeriodicBlobs(S_in, p.sp_filt.t, p.sp_filt.f, True)
    s14.calc_in_out_band()
    s14.find_peaks()
    s14.calc_peak_start()
    s14.label_bursts()

    s14.S_out.shape # this is the spectrogram without the bursts  
  """

  # min and max of burst
  f_bmin_hz =  9_250
  f_bmax_hz = 10_250

  def __init__(self, S_in, t, f, verbose):
    self.S_in = S_in
    self.t = t
    self.f = f
    self.verbose = verbose

    self.f_bmin_pt = np_index_at_value(self.f, self.f_bmin_hz)
    self.f_bmax_pt = np_index_at_value(self.f, self.f_bmax_hz)

    if self.verbose: print(f"Freq range of bursts to drop: {self.f_bmin_hz} Hz - {self.f_bmax_hz} Hz")

    if self.verbose: self.my_plot_5s_ranges(S_in, None, "input")


  def my_plot_5s_ranges(self, S_xxx, peaks_vec, title_prefix):
    t_05s = np_index_at_value(self.t, 5)
    f_bmin = self.f_bmin_pt
    f_bmax = self.f_bmax_pt

    for i in range(int(self.t.max()//5+1)):
      if i > 20: break
      x = S_xxx[:, i*t_05s:(i+1)*t_05s]
      title = f"{title_prefix}: slice # {i}"

      fig = plt.figure(figsize=(20,3))
      plt.imshow(x, origin="lower")

      if peaks_vec is not None:
        for pk_i in peaks_vec:
          if i*t_05s < pk_i < (i+1)*t_05s:
            plt.axvline(pk_i - i*t_05s, color="green")

      plt.axhline(f_bmin)
      plt.axhline(f_bmax)
      plt.title(title)
      plt.colorbar()
      plt.show()

    
  def calc_in_out_band(self):
    f_bmin = self.f_bmin_pt
    f_bmax = self.f_bmax_pt
    S_in = self.S_in

    S_yesburst = S_in[f_bmin:f_bmax, :].mean(axis=0)
    S_nonburst = S_in[:f_bmin, :].mean(axis=0) + S_in[f_bmax:, :].mean(axis=0)

    if self.verbose:
      print(S_yesburst.shape, S_nonburst.shape)

      # Need to see a periodic burst, at least 3 bursts in 9-second interval
      plt.figure(figsize=(20,3))
      plt.plot(self.t, S_yesburst, label="in band")
      plt.plot(self.t, S_nonburst, label="out of band")
      plt.xlabel("t (seconds)")
      plt.title("burst band: 9.25 khz - 10.25 khz")
      plt.legend()
      plt.show()

    # Calculate SNR of burst band to non-burst
    # again but moving average to smooth it and find start/end
    # Add .1 to avoid nan in division
    # Ref: efficient moving average: https://stackoverflow.com/a/22621523/4126114
    my_filt = lambda x: uniform_filter1d(x, size=40)
    S_snr = my_filt(S_yesburst)
    #S_snr = my_filt(S_yesburst) / (.1 + my_filt(S_nonburst))
    #S_snr = uniform_filter1d(S_snr, size=40)
    if self.verbose:
      print("S_snr.shape:", S_snr.shape)

      plt.figure(figsize=(20,3))
      plt.plot(self.t, S_snr, label="ma(in band)")
      plt.xlabel("t (seconds)")
      plt.title("burst band: 9.25 khz - 10.25 khz")
      plt.legend()
      plt.show()

    self.S_snr = S_snr


  def find_peaks(self):
    S_snr = self.S_snr

    # find peaks
    #peaks_vec = find_peaks(S_snr, height=5, distance=40)[0]
    peaks_vec = find_peaks(S_snr, height=1, distance=40)[0]

    if peaks_vec.shape[0] == 0:
      raise S14AbortNoPeriodicBlobs("Did not find any periodic blobs")

    if peaks_vec.shape[0] < 3:
      raise S14AbortNoPeriodicBlobs(f"Found {peaks_vec.shape[0]} blobs (< 3) => Abort")

    # assert that min peak is > 5
    #assert S_snr[peaks_vec].min() >= 5
    assert S_snr[peaks_vec].min() >= 1

    # assert that peaks are periodic at 8 seconds
    # FIXME what if the reference to subtract was not the first one found, i.e. peaks_vec[0]
    # FIXME What about other periods,
    # eg 2 seconds in https://storage.googleapis.com/dolphicom/analyses/t17.1-f1-v20210824a-noaa_pifsc-pipeline/html/p633.html
    # File: 1706/1706_20170918_190020_615_ch_1 in NOAA PIFSC dataset
    peak_period = 8 # seconds
    peak_modper = ((self.t[peaks_vec] - self.t[peaks_vec[0]]).round(0) % peak_period)
    n_pre = peaks_vec.shape[0]
    peaks_vec = peaks_vec[peak_modper == 0]

    if peaks_vec.shape[0] <= 1:
      raise S14AbortNoPeriodicBlobs(f"Found {n_pre} blobs, but none with periodicity {peak_period} seconds => Abort")

    if peaks_vec.shape[0] <= 2:
      raise S14AbortNoPeriodicBlobs(f"Found {n_pre} blobs, but only 2 have periodicity {peak_period} seconds. Need at least 3 => Abort")

    self.peaks_vec = peaks_vec

    if self.verbose:
      print("n peaks: ", peaks_vec.shape[0])
      print("peaks[:20]=", peaks_vec[:20])

      plt.figure(figsize=(20,3))
      plt.plot(self.t, S_snr, label="in/out band")
      plt.scatter(self.t[peaks_vec], S_snr[peaks_vec], label="peaks", color="red")
      plt.xlabel("t (seconds)")
      plt.title("burst band: 9.25 khz - 10.25 khz")
      plt.legend()
      plt.show()

      # plot with just peak locations
      self.my_plot_5s_ranges(self.S_in, peaks_vec, "input with burst ends")


  
  def calc_peak_start(self):
    peaks_vec = self.peaks_vec

    # Use "-20" by trial and error to identify burst width as ~ 0.1 seconds
    # by trial and error, get 0.1 seconds before peak as the start (peak is the end)
    t_0p1 = np_index_at_value(self.t, 0.1)

    # check that indeed it covers it
    #assert S_snr[peaks_vec-t_0p1].max() < 1
    #assert S_snr[peaks_vec].max() < 1

    self.pk_start_end = np.sort(np.concatenate([peaks_vec, peaks_vec-t_0p1]))
    self.t_0p1 = t_0p1

    if self.verbose:
      # plot with start/end of bursts (peak is end)
      self.my_plot_5s_ranges(gaussian(self.S_in, 1), self.pk_start_end, "blurred bursts with start/end")

      plt.figure(figsize=(20,3))
      plt.plot(self.t, self.S_snr, label="ma(in band)")
      plt.scatter(self.t[peaks_vec], self.S_snr[peaks_vec], label="burst end", color="red")
      plt.scatter(self.t[peaks_vec-t_0p1], self.S_snr[peaks_vec-t_0p1], label="burst start", color="green")
      plt.xlabel("t (seconds)")
      plt.title("burst band: 9.25 khz - 10.25 khz")
      plt.legend()
      plt.show()


  def label_bursts(self):
    f_2khz = np_index_at_value(self.f, 2000)
    peaks_vec = self.peaks_vec
    t_0p1 = self.t_0p1
    f_bmin = self.f_bmin_pt
    f_bmax = self.f_bmax_pt

    # get relaxed copy of spectrogram around the bursts
    S_intermediate = np.zeros_like(self.S_in)
    for t_start, t_end in zip(peaks_vec-t_0p1, peaks_vec):
      S_intermediate[(f_bmin-f_2khz):(f_bmax+f_2khz), (t_start-t_0p1):(t_end+t_0p1)] = self.S_in[(f_bmin-f_2khz):(f_bmax+f_2khz), (t_start-t_0p1):(t_end+t_0p1)]

    if self.verbose:
      self.my_plot_5s_ranges(S_intermediate, self.pk_start_end, "lax bursts with start/end")

    # get region of each burst
    S_blur = gaussian(S_intermediate, .5)
    S_label = label(S_blur > np.quantile(S_blur, q=.9))
    #properties=["label", "perimeter", "centroid", "eccentricity", "major_axis_length", "minor_axis_length"]
    #S_peri = regionprops_table(S_label, properties=properties)

    #S_label.max()


    # get labels INSIDE the burst boxes
    label_l = []
    for t_start, t_end in zip(peaks_vec-t_0p1, peaks_vec):
      S_region = S_label[f_bmin:f_bmax, t_start:t_end]

      # Assert that region only has 1 label
      #print(S_region.min(), S_region.max())
      #assert S_region.min() == S_region.max()

      label_reg = set(S_region.reshape((-1,1)).squeeze().tolist())
      label_reg = label_reg.difference({0,})
      # only accept regions with a single label as an accepted "blob"
      if len(label_reg)==1:
        label_reg = label_reg.pop()
        label_l.append(label_reg)

    if self.verbose:
      print(f"Will keep {len(label_l)} bursts, eg first 10: {label_l[:10]}")

    # drop labels not in label_l
    for i in range(S_label.max()+1):
      if i==0: continue
      if i not in label_l:
        S_label = np.multiply(S_label, S_label!=i)

    # delete the blocks in the spectrogram in the burst bands
    # by using the labeled regions from S_label
    S_out = np.multiply(self.S_in, S_label==0)

    if self.verbose:
      print("*"*100)
      print("Spectrogram without the bursts")
      #my_plot_5s_ranges(S_in - S_out, pk_start_end, "bursts")
      self.my_plot_5s_ranges(S_out, self.pk_start_end, "output")

    self.S_out = S_out
