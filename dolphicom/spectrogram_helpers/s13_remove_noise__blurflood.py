"""
Noise removal from spectrograms.
Developed in notebook t07.2c
https://colab.research.google.com/drive/1SUwas0-syDGCfIAP38i31phXSn7wu27Z#scrollTo=HmmQKWOCIVYb

Removes background noise


Demo:
from skimage import data, color
img = data.astronaut()
img_hsv = color.rgb2gray(img)

plt.imshow(img_hsv)
plt.show()

from dolphicom.spectrogram_helpers.s13_remove_noise__blurflood import remove_noise
plt.imshow(remove_noise(img_hsv))
plt.show()
"""


from matplotlib import pyplot as plt
from skimage import filters
import numpy as np
from skimage.segmentation import flood
from collections import deque
from ..utils import AbortPipeline, np_index_at_value

from scipy import ndimage
from skimage.filters import gaussian
from tqdm.auto import tqdm


def norm_power(S1_raw, do_plot):
    S_power = (S1_raw).mean(axis=1)
    S_deno = S_power.sum().max()

    if S_deno == 0:
      print("[WARNING] Found power=0 in input to norm_power, so returning signal as is")
      #return np.copy(S1_raw)
      return S1_raw

    S_power = S_power/S_deno

    if do_plot:
      plt.plot(S_power)
      plt.title("power spectrum mean over time")
      plt.xlabel("frequency (hz)")
      plt.ylabel("% power (n points)")
      plt.show()

    S_power = np.repeat(S_power[:,None], S1_raw.shape[1], axis=1)

    S2_norm = np.divide(S1_raw, 1+1000*S_power)

    # Use 99% quantile to avoid outlier strong peaks
    q_pre, q_norm = np.quantile(S1_raw, q=.99), np.quantile(S2_norm, q=.99)

    # If the 99% quantile still gives 0's, then forced to use the .max
    if q_pre==0: q_pre = S1_raw.max()
    if q_norm==0: q_norm = S2_norm.max()

    # no way that q_pre or q_norm can be 0 at this stage because otherwise this function would have early-returned from the S_deno==0 step
    assert q_pre > 0
    assert q_norm > 0

    # proceed
    S2_norm = np.clip(S2_norm, a_min=None, a_max=q_norm)
    S2_norm = S2_norm / q_norm * q_pre

    #q_pre, q_norm
    return S2_norm


def blur_flood(S1_raw, sigma, f_vec, verbose):
    """
    Perform blur then remove noise by random sampling and flood-filling
    f_vec: indices in frequency to focus on, eg np.arange(3,5) if f==[0,10,20,30,40,50,60,70] Hz and it is desired to focus on [30,40,50] hz. Pass None to use all frequency.
    """

    S2 = filters.gaussian(S1_raw, sigma=sigma)

    assert S2.shape == S1_raw.shape

    # Method 3: sample randomly and cross fingers that won't drop a whistle
    S3_bf = np.zeros_like(S1_raw)
    n_rep = 20

    # Update: calculate n_rand from the input shape as 20% of time axis
    #n_rand = 200
    # check there are enough points along t (not necessarily f) for n_rand, otherwise all matrix would be abolished
    #if S2.shape[1] < 3*n_rand:
      #print(f"[WARNING] Not enough points along axis=1 (time) for random sampling of {n_rand} points. Got {S2.shape[1]} but require minimum 3*{n_rand}={3*n_rand}.")
      #print(f"[WARNING] Will just return 0s")
      #return S3_bf

    n_rand = S2.shape[1]//5

    p_prev = deque(maxlen=3)

    count_points = lambda S3_bf: int((S3_bf!=0).sum()) # just count non-zero
    dtype = np.int64
    # multiply by 10000 for bps
    def max_ratio_bps(d1):
      if (np.array(d1)==0).all(): return 0
      d2 = (1+np.array(d1)[:-1]) # do +1 to avoid division by 0 if p_prev has 0's
      return (np.diff(d1)/d2*10000).astype(dtype).max()

    t_vec = np.arange(S2.shape[1])
    if f_vec is None: f_vec = np.arange(S2.shape[0])

    # for checking integer overflow
    iinfo = np.iinfo(dtype)

    #for k in tqdm(range(n_rep)):
    for k in range(n_rep):
      mask_noise = np.full_like(S2, False)
      i_idx = np.random.random_integers(0, t_vec.shape[0]-1, n_rand)
      j_idx = np.random.random_integers(0, f_vec.shape[0]-1, n_rand)
      for ti, fi in zip(i_idx, j_idx):
        if mask_noise[fi,ti]: continue # skip if already marked this as noise
        tv, fv = t_vec[ti], f_vec[fi]
        mask_noise = np.logical_or(mask_noise, flood(S2, (fv, tv), tolerance=.1))

      # Important np.copy!
      S3_i = np.copy(S1_raw)
      S3_i[mask_noise==1] = 0
      S3_bf = (S3_bf*k + S3_i)/(k+1) # running average
      p_prev.append(count_points(S3_bf))

      # early break when less than x% points being dropped
      if len(p_prev) == p_prev.maxlen:
        p_ratio = max_ratio_bps(p_prev)

        # check for overflow
        if (p_ratio < .9 * iinfo.min) or (p_ratio > .9 * iinfo.max):
          raise Exception(f"Overflow in calculations for dtype {dtype}. p_ratio = {p_ratio}")

        if verbose: print(f"blur flood, sigma={sigma}: iter # {k+1}, p_prev={p_prev}, ratio={p_ratio} bps")
        if p_ratio < 50:
          if verbose: print(f"blur flood, sigma={sigma}: early return since avg of last 3 < 50 bps")
          break

    assert S3_bf.shape == S1_raw.shape

    return S3_bf


def bf_avg(S2_norm, f_vec, verbose):
    # apply blur_flood for different values of sigma, then average
    # The smaller sigma retains thin lines in the dolphin whistle, but keeps non-whistle background noise
    # The larger sigma loses thin lines in the whistle, but removes background noise well
    #
    # Update: instead of storing 4 arrays, just add to a single one
    #S3_bf = [blur_flood(S2_norm, sigma=sigma, f_vec=f_vec, verbose=verbose) for sigma in [1,2,3,4]]
    #S3_bf = [x[None,:,:] for x in S3_bf]
    #S3_bf = np.concatenate(S3_bf).mean(axis=0)

    # Method 2: just add up matrices together
    # Update: csr_matrix are a headache in their interoperability with numpy, doing some weird stuff silently
    # so will just stick with regular numpy array, and assign into csr_matrix after the operations are done
    # S3_bf = scipy.sparse.csr_matrix(S2_norm.shape, dtype=float)
    S3_bf = np.zeros_like(S2_norm, dtype=float)
    sig_l = [1,2,3,4]
    for sigma in sig_l:
      S3_bf += blur_flood(S2_norm, sigma=sigma, f_vec=f_vec, verbose=verbose)

    S3_bf = S3_bf/len(sig_l)

    # At this point, the 95% quantile and below is cloudy noise from the above averaging, so drop it
    # Btw 95% because most of the matrix is 0's or small numbers
    THRESH_NOISE = .95
    S3_bf = np.multiply(S3_bf, S3_bf > np.quantile(S3_bf,q=THRESH_NOISE))

    return S3_bf


def get_f_focus(S, verbose):
    """
    Return list of tuples of 2 indeces of start/end of frequency range to focus on (in points, not in Hz)
    """
    linearKernel_i = np.ones(shape=(2, 120))
    #linearKernel_i[np.arange(0,30,1)]
    S_single = ndimage.grey_opening(gaussian(S,1), footprint=linearKernel_i)

    #S_mean_t = np.quantile(S_single, q=.5, axis=1)
    S_mean_t = S_single.mean(axis=1)
    q90 = np.quantile(S_mean_t, q=.9)

    idx_out = S_mean_t > 2*q90
    #idx_out = S_mean_t > .03
    am = np.ma.array(S_mean_t, mask=~idx_out)
    f_focus = [(x.start, x.stop) for x in np.ma.notmasked_contiguous(am)]

    if verbose:
        fig = plt.figure(figsize=(20,3))
        plt.imshow(S, origin="lower")
        plt.show()

        fig = plt.figure(figsize=(20,3))
        plt.imshow(S_single, origin="lower")
        plt.title("Spectrogram after grey opening")
        plt.show()

        plt.plot(S_mean_t)
        plt.axhline(y=2*q90)
        #plt.scatter(np.arange(0,S_mean_t.shape[0])[idx_out], S_mean_t[idx_out], color="red")
        for f_start, f_end in f_focus:
          plt.axvline(x=f_start, color="green")
          plt.axvline(x=f_end-1, color="red")

        plt.ylabel("Avg spectrogram power over time")
        plt.xlabel("Frequency (index in points, not in hertz")
        plt.title("Spectrogram averaged over time")
        plt.show()

    return f_focus




def remove_noise(S1_raw, f_focus, do_plot):
    """
    S1_raw: 2d spectrogram (along f and t) to clean
    f_focus: list of tuples of pairs of frequencies (start and end, in points, not Hertz) at which to focus (axis=0)
    do_plot: True|False. Whether or not to generate plots. Same as verbosity.
    """
    if (S1_raw==0).all():
      print("Input to remove_noise is all 0s. Returning as is.")
      #return np.copy(S1_raw)
      return S1_raw

    if do_plot:
      plt.figure(figsize=(20,3))
      plt.imshow(S1_raw)
      plt.title("input")
      plt.show()

    # normalize the power
    S2_norm = norm_power(S1_raw, do_plot)

    if do_plot:
      plt.figure(figsize=(20,3))
      plt.imshow(S2_norm)
      plt.title("after normalized power")
      plt.show()

      #plt.figure(figsize=(20,3))
      #plt.imshow(np.abs(S2_norm-S1_raw))
      #plt.title("after normalized power - input")
      #plt.show()

    # apply blur_flood for different values of sigma, then average
    # The smaller sigma retains thin lines in the dolphin whistle, but keeps non-whistle background noise
    # The larger sigma loses thin lines in the whistle, but removes background noise well
    S3_bf = bf_avg(S2_norm, f_vec=None, verbose=do_plot)

    if do_plot:
      plt.figure(figsize=(20,3))
      plt.imshow(S3_bf)
      plt.title("after blur-flood-fill on all freq")
      plt.show()

      #plt.figure(figsize=(20,3))
      #plt.imshow(np.abs(S1_raw - S3_bf))
      #plt.title("input - after remove noise")
      #plt.show()

    # focus on the 7.4-7.9khz range and 0-6khz
    # Update: had hard-coded values, but shifting to non-hardcoded
    S4_drop_narrow = S3_bf
    if len(f_focus)>0:
      for f_start, f_end in f_focus:
        S4_drop_narrow = bf_avg(S4_drop_narrow, f_vec=np.arange(f_start, f_end), verbose=do_plot)

      if do_plot:
        plt.figure(figsize=(20,3))
        plt.imshow(S4_drop_narrow)
        plt.title("after blur-flood-fill on f_focus")
        plt.show()

    return S4_drop_narrow


def test_samples(sp_raw):
  # Update: mobysound.org have different sampling rates
  sps = np_index_at_value(sp_raw.t, 1)

  S_all = [
    #(sp_raw.Sxx[30:60, 400:450],   "1 of 3 whistles"),
    (sp_raw.Sxx[:, 400:700],       "3 whistles"),
    (sp_raw.Sxx[:, 700:1200],      "just noise"),
    (sp_raw.Sxx[:, s*15:s*20],     "15-20 secs, 7.5khz noise"),
    (sp_raw.Sxx[:, 6200:6600],     "overlapping whistles and very noisy"),
    (sp_raw.Sxx[:, s*35:s*40],     "some whistles in 6khz"),
    (sp_raw.Sxx[:, (132*s):(137*s)], " one big whistle + narrow-band noise"),
  ]

  fig, ax = plt.subplots(len(S_all), 2, figsize=(20,20))

  for i, (S_raw, title_i) in enumerate(tqdm(S_all)):
      S_clean = remove_noise(S_raw, False)
      ax[i,0].imshow(S_raw)
      ax[i,0].set_title(title_i)
      ax[i,1].imshow(S_clean)

  plt.show()
