from ..utils import np_rolling_savgol_piecewise, np_index_at_value
from scipy.signal import find_peaks
from skimage.filters import gaussian
from scipy import signal
import matplotlib.pyplot as plt
import numpy as np


def nan_to_zero(S):
  S[np.isnan(S)] = 0
  return S


# Copy again from previous deprecated section of extracting lower, upper, and mid frequencies

#%pdb on


def spectrogram_peak_1d(f, Sxx_keep):
    s_1d = []
    for t_i in range(Sxx_keep.shape[1]):
      Sxx_i = Sxx_keep[:,t_i]

      if np.all(np.isnan(Sxx_i)):
        s_1d.append({"w": np.nan, "f": np.nan, "peaks_idx": [], "peaks_val": []})
        continue

      wm, _ = find_peaks(Sxx_i)
      if len(wm)==0:
        s_1d.append({"w": np.nan, "f": np.nan, "peaks_idx": [], "peaks_val": []})
        continue

      if len(wm)==1:
        wm = wm[0]
        fm = f[wm]
        s_1d.append({"w": wm, "f": fm, "peaks_idx": [wm], "peaks_val": [fm]})
        continue

      fm = f[wm]
      n_back = 3

      if len(s_1d) < n_back:
        s_1d.append({"w": np.nan, "f": np.nan, "peaks_idx": wm, "peaks_val": fm})
        continue

      # choose the peak that is closer to the average of previous 3 points
      last_points = [x["w"] for x in s_1d[-n_back:]]
      lp_mean = np.nanmean(last_points)
      if np.isnan(lp_mean):
        s_1d.append({"w": np.nan, "f": np.nan, "peaks_idx": wm, "peaks_val": fm})
        continue
      
      # Update: in this case, the argmin(abs(...)) call should not be replaced by np_index...
      wc = wm[np.argmin(np.abs(wm - lp_mean))]
      #wc = wm[np_index_at_value(wm, lp_mean)]

      fc = f[wc]
      s_1d.append({"w": wc, "f": fc, "peaks_idx": wm, "peaks_val": fm})

    # Update: only need the "f" column since it's the one that is "closest to previous 3 points"
    #s_1d = pd.DataFrame(s_1d)
    #s_1d.shape
    #return [np.array(x["peaks_val"]) for x in s_1d]
    return [x["f"] for x in s_1d]




def my_call_spec1d(f, S):
  S = gaussian(S, 2) # # With blur, just 1 peak per timestep
  S[S < .05] = 0 # thresholding for small values introduced by gaussian
  s_1d  = np.array(spectrogram_peak_1d(f, S))
  assert len(s_1d.shape)==1

  # drop biologically difficult rises or drops
  # 400 is biologically possible as per data, eg whistle # 730 in ytq1 v2021-06-17
  g = np.gradient(s_1d)
  s_1d[np.abs(g) > 1000] = np.nan

  # do not smooth out no-trace whistles, skip them later
  if np.isnan(s_1d).all(): return s_1d

  # 50 is needed to smooth the whistle enough to stop showing bumps from frequency sample rate
  # Use 40 since shortest seems to be 41
  # Update savgol doesn't need to go up to 50 for smoothing, 19 works and preserves some quick changes in
  # in the whistle, like whistle #734 in ytq1
  #assert s_1d.shape[0] > 40
  s_1d = np_rolling_savgol_piecewise(s_1d, 39)

  return s_1d


def my_gradient_smooth(x):
  g = np.gradient(x)
  g = np_rolling_savgol_piecewise(g, 29)
  return g


class S42Tracer:

  def __init__(self, whistle_corpus3):
    self.whistle_corpus3 = whistle_corpus3


  def trace_whistles(self, f):
    whistle_corpus3 = self.whistle_corpus3
    whistle_corpus3["S_zeroed"] = whistle_corpus3.S.apply(lambda S: np.copy(S)).apply(nan_to_zero)
    whistle_corpus3["S_1d"] = whistle_corpus3.S_zeroed.apply(lambda S: my_call_spec1d(f, S))

    # FIXME Commenting out the below lower-bound condition because it worked on ytq1 but will not work with others
    # I don't know what the more relaxed version would be, so just commenting out anyway
    # assert whistle_corpus3["S_1d"].apply(lambda x: x.shape[0]).min() == 41

    # Drop the only "whistle" which doesn't have a trace
    idx_notrace = whistle_corpus3.S_1d.apply(lambda S_1d: np.isnan(S_1d).all())

    # Update: below assert was for ytq1. Relaxing it to 10%
    # assert idx_notrace.sum()==1
    assert idx_notrace.sum() <= max(10, whistle_corpus3.shape[0]//10)

    whistle_corpus3 = whistle_corpus3[~idx_notrace].copy()
    whistle_corpus3["S_gradient"] = whistle_corpus3["S_1d"].apply(my_gradient_smooth)
    self.whistle_corpus3 = whistle_corpus3


  def plot_examples_1(self):
      whistle_corpus3 = self.whistle_corpus3

      plt.plot(whistle_corpus3.iloc[0].S_1d)
      plt.show()

      plt.plot(whistle_corpus3.iloc[41].S_1d)
      plt.show()

      plt.imshow(whistle_corpus3["S_zeroed"].iloc[41], origin="lower")
      plt.show()

      plt.plot(whistle_corpus3.loc[217].S_1d)
      plt.show()

      plt.plot(whistle_corpus3.loc[730].S_1d)
      plt.show()

      plt.plot(whistle_corpus3.loc[734].S_1d)
      plt.show()

      # testing case of s_1d all nan
      # Update: was due to np.mean not np.nanmean. Anyway, added fallback window=10 also
      plt.plot(whistle_corpus3.loc[561].S_1d)
      plt.show()


  def plot_examples_2(self, t, f):
    whistle_corpus3 = self.whistle_corpus3


    #spectrogram_peak_1d(f, whistle_corpus3.S_zeroed.iloc[0])

    n_plt = 60

    f_samplerate = np.diff(f[:3])[0]
    t_1s = np_index_at_value(t, 1)

    for i in range(n_plt):
      wh_i = whistle_corpus3.iloc[i]
      if i % 30 == 0:
        fig, ax = plt.subplots(10, 3, figsize=(20,20))
        ax = ax.reshape((-1,1)).squeeze()

      ax[i%30].imshow(wh_i.S, origin="lower")
      #ax[i%30].imshow(wh_i.S_filt, origin="lower")
      #ax[i%30].imshow(wh_i.S_zeroed, origin="lower")
      ax[i%30].plot(np.arange(0, wh_i.S_zeroed.shape[1], 1), wh_i.S_1d/f_samplerate, color="red")
      ax[i%30].set_xlim(0, t_1s)
      ax[i%30].set_title(f"whistle # {wh_i.label_whistle}")

      if (i+1) % 30 == 0:    
        plt.tight_layout()
        plt.show()


  def plot_examples_3_gradient(self):
    whistle_corpus3 = self.whistle_corpus3
    #plt.plot(np_rolling_savgol_piecewise(np.gradient(whistle_corpus3.iloc[0].S_1d),29))
    #plt.plot(whistle_corpus3.iloc[0].S_gradient)
    plt.plot(whistle_corpus3.loc[922].S_gradient)
    plt.axhline(y=0)
    plt.show()
