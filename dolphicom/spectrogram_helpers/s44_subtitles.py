import pandas as pd

class S44Subtitles:
  def __init__(self, s23_whistle_corpus):
    self.s23_whistle_corpus = s23_whistle_corpus

  def df(self):
    whistle_subtitles = self.s23_whistle_corpus.sort_values(["t_sec_min", "t_sec_max"])[["t_sec_min", "t_sec_max", "S_str_2"]]
    whistle_subtitles["t_start"] = pd.to_datetime(whistle_subtitles["t_sec_min"]*1000000000)
    
    whistle_subtitles = ( 
        whistle_subtitles
        .groupby(pd.Grouper(key='t_start', freq='5s'))
        .S_str_2
        #.apply(lambda g: " ... ".join(g))
        .apply(lambda g: "   ".join(g))
        .reset_index()
    )
    
    whistle_subtitles["t_end"  ] = whistle_subtitles["t_start"] + pd.to_timedelta(5, unit='s')
    
    dt_to_ts = lambda x: x.dt.strftime("%H:%M:%S,%f").apply(lambda x: x[:-3])
    whistle_subtitles["t_start"] = dt_to_ts(whistle_subtitles["t_start"])
    whistle_subtitles["t_end"] = dt_to_ts(whistle_subtitles["t_end"])
    
    whistle_subtitles.reset_index(inplace=True)
    
    #whistle_subtitles["S_str_2"] = whistle_subtitles["S_str_2"].apply(lambda x: "..." if len(x)==0 else x)
    
    #whistle_subtitles.head()
    self.whistle_subtitles = whistle_subtitles

  def write(self, fn_srt):
    whistle_subtit2 = []
    for i, r in self.whistle_subtitles.iterrows():
      whistle_subtit2.append(f"{i}\n")
      whistle_subtit2.append(f"{r.t_start} --> {r.t_end}\n")
      whistle_subtit2.append(f"{r.S_str_2}\n")
      whistle_subtit2.append("\n")

    with open(fn_srt, "w") as fh:
      fh.writelines(whistle_subtit2)

