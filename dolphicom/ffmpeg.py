from tqdm.auto import tqdm
#import ffmpeg
import os
import math


class FfmpegMan:
  def __init__(self, wm):
    self.wm     = wm

  def generate_wav(self, fn_wav_full):
    """
    Save wav segments to play along the gif
    Note: Could replace system call to ffmpeg with pydub package https://github.com/jiaaro/pydub/
    """
    plt_params_use = self.wm.plt_params[self.wm.plt_params["key"]]
    for i, j, j_step, fn_png in tqdm(self.wm.fn_all):
      fn_wav_part = fn_png.replace(".png", ".wav")
      start_ts = plt_params_use[i]["sec_start"] + j
      # FIXME replace with ffmpeg.input(...) call as in merge_png_wav ?
      # ffmpeg.input(in_filename, ss=time)
      ffmpeg_cmd = f'ffmpeg -loglevel panic -ss "{start_ts}" -i "{fn_wav_full}" -t {j_step} -y -c copy "{fn_wav_part}"'
      res = os.system(ffmpeg_cmd)
      if res!=0:
        print(ffmpeg_cmd)
        raise Exception("Got error in generate_wav from ffmpeg. Command above. Aborting")

  # Couldn't figure out the right way to do this
  #def merge_png_wav_py(self):
  #  fn_str = [x[2].replace(".png", "") for x in self.wm.fn_all]
  #
  #  # Original ffmpeg cmd:
  #  # cat $PAIRS_TXT | awk '{print "ffmpeg -loglevel panic -r 1 -i " $0 ".png -i " $0 ".wav -c:v libx264 -c:a aac -vf fps=1 -pix_fmt yuv420p " $0 ".mp4"}' >> doit.sh
  #
  #  for fn_i in tqdm(fn_str):
  #    stream = (
  #        ffmpeg
  #        .output(
  #            ffmpeg.input(f"{fn_i}.png"),
  #            ffmpeg.input(f"{fn_i}.wav"),
  #            f"{fn_i}.mp4",
  #            **{ "r": 1, # framerate
  #                "c:v": "libx264",
  #                "c:a": "aac",
  #                "vf": "fps=1",
  #                "pix_fmt": "yuv420p"
  #            }
  #        )
  #    )
  #    #print(stream.get_args())
  #    #print(stream.compile())
  #    try:
  #      stream.run(capture_stderr=True)
  #    except ffmpeg.Error as e:
  #      if e.stderr:
  #        print("ffmpeg error")
  #        print(e.stderr)
  #        raise Exception("Got ffmpeg error above")


  def merge_png_wav_cmd(self):
    # if no fn_all field (eg if png generation not done) can generate as follows
    # self.wm.fn_all = [(None,None,None,fn_i) for fn_i in glob.glob(f"{PNGDIR}/*png")]
    import moviepy.editor as mpe
    fn_str = [fn_i.replace(".png", "") for i, j, j_step, fn_i in self.wm.fn_all]
    for fn_i in tqdm(fn_str):
      # calculate frame rate needed to match video with audio
      fn_wav_part = f"{fn_i}.wav"

      # Update 2021-05-24: Using moviepy instead of ffmpeg because I couldn't get ffmpeg to output equal durations for video and audio
      # which borked when multiple videos of different frame rates needed to be merged (as in my ffmpeg hack above with frame rate)
      clip_png = mpe.ImageClip(f"{fn_i}.png")
      clip_wav = mpe.AudioFileClip(f"{fn_i}.wav")

      # Keep only integer multiples of the duration, i.e. 5 for 5 seconds, or 4 if 4.2 seconds
      # Update 2022-02-15: Changing this from int() to math.ceil() so that 0.5 -> 1 not 0
      clip_wav = clip_wav.set_duration(math.ceil(clip_wav.duration))

      clip_mp4 = clip_png.set_audio(clip_wav)
      clip_mp4.duration = clip_wav.duration
      clip_mp4.write_videofile(f"{fn_i}.mp4", fps=1, verbose=False)  # Can just use 1 fps since audio is truncated to integer multiples


  # Keeps throwing error that the glob pattern doesn't exist
  #def concat_mp4_py(self, PNGDIR, FNMP4):
  #  stream = (
  #      ffmpeg.input(f"{PNGDIR}/*.mp4", pattern_type='glob')
  #      .output(FNMP4, codec="copy")
  #  )
  #  print(stream.compile())
  #  try:
  #    stream.run(capture_stderr=True)
  #  except ffmpeg.Error as e:
  #    if e.stderr:
  #      print("ffmpeg error")
  #      print(e.stderr)
  #      raise Exception("Got ffmpeg error above")
  
  def concat_mp4_cmd(self, PNGDIR, FNMP4):
    if len(self.wm.fn_all)==0:
      print("No whistles detected? No file generated")
      return

    import glob
    fn_mp4 = glob.glob(f'{PNGDIR}/*.mp4')
    fn_mp4 = sorted(fn_mp4)
    concat_txt = f"{PNGDIR}/concat.txt"
    with open(concat_txt, "w") as f:
      for fn_i in fn_mp4:
        fn_i = os.path.basename(fn_i)
        f.write(f"file '{fn_i}'\n")

    #!head -n3 concat.txt
    #!echo "..."
    #!tail -n3 concat.txt

    ffmpeg_cmd = f"ffmpeg -f concat -safe 0 -i '{concat_txt}' -c copy '{FNMP4}'"
    res = os.system(ffmpeg_cmd)
    if res!=0:
      print(ffmpeg_cmd)
      raise Exception("Got error in concat_mp4_cmd from ffmpeg. Command above. Aborting")
