import os
import datetime as dt
from tqdm.auto import tqdm
import shutil
#from ..core.audiofile import AudioFile

# https://docs.python.org/3/library/multiprocessing.html
import multiprocessing as mp

class BaseMultiple:
  def __init__(self, dir_out, func_done, dir_drop_prefix):
    """
    dir_out: directory to save pkl files of the AudioFile objects
    func_done: function to run on the .pkl.tgz file (tar of AudioFile's pkl)
    dir_drop_prefix - dirname prefix from each filepath passed to run() that needs to be dropped. Use "" to not drop anything
    """
    from .local_single import LocalSingle as PipelineLocalSingle
    self.pp=PipelineLocalSingle(dir_out=dir_out, func_done=func_done, generate_gallery=True, dir_drop_prefix=dir_drop_prefix)


  def fit_one(self, pool_in_i):
    i, n, fn_wav = pool_in_i
    p_pre = f"File {i+1}/{n}: {os.path.basename(fn_wav)}"
    print(p_pre)
    self.pp.fit(fn_wav)


  def get_file_iterator(self):
    """
    Should return a generator and the number of elements in it
    Each generator entry should be a tuple of 3:
    an index (< length of generator), the number of elements (again), and a file path
    """
    raise Exception("To be implemented by derived classes.")


  def fit(self, n_jobs):
    """
    n_jobs: number of processes to use in parallel. Use -1 to use all processors.
    """
    fn_iter, n1 = self.get_file_iterator()

    if n_jobs == -1:
      n_jobs = mp.cpu_count()
    
    # Serial run
    if n_jobs == 1:
      for i, n2, fn_wav in tqdm(fn_iter, total=n1):
        self.fit_one((i, n2, fn_wav))
      return
    
    # parallel per file
    #pool_in_l = zip(range(fn_n), [fn_n]*fn_n, fn_iter)
    with mp.Pool(processes=n_jobs) as pool:
      #pool.map(self.fit_one, pool_in_l)
      pool.map(self.fit_one, fn_iter)


  def generate_html_gallery(self, html_title_prefix):
    from ..io.gallery_html_generator import GalleryHtmlGenerator
    GalleryHtmlGenerator.run(dir_lookup=self.pp.dir_out, html_title_prefix=html_title_prefix)
