from __future__ import unicode_literals
import youtube_dl
import os



class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


"""
def my_hook(d):
    if d['status'] == 'finished':
      print(d)
      print('Done downloading, now converting ...')
"""

from youtube_dl.utils import sanitize_filename as ydl_sanitize_filename
def my_sanitize_filename(s):
  return ydl_sanitize_filename(s, restricted=True)


from youtube_dl.postprocessor.common import PostProcessor
from .local_single import LocalSingle as PipelineLocalSingle
class PipelinePP(PipelineLocalSingle, PostProcessor):
  def __init__(self, downloader, dir_out, func_done, generate_gallery, dir_drop_prefix):
    """
    downloader: required for youtube_dl.postprocessor.common.PostProcessor
    Other args from PipelineLocalSingle
    """
    super(PipelineLocalSingle).__init__(dir_out, func_done, generate_gallery, dir_drop_prefix)
    super(PostPorcessor).__init__(downloader)


  def run(self, information):
      """
      Utility function needed for YoutubeDlWrap
      """
      # fn_in = os.path.join(self.dir_audio, d["filename"]), self.n, self.dir_out, self.func_done)
      fn_in = information["filepath"]
      #n = 0 # FIXME ?
      super(PipelineLocalSingle).fit(fn_in)

      # In case of error, return None to not continue to other postprocessors
      # Update: turns out that even though the Postprocessor class documentation [1] says "return None to break the chain",
      # the source code of the postprocessor execution for loop [2] doesn't have any such mechanism.
      # Will just return as usual and depend on later postprocessors figuring it out
      # [1] Postprocessor.__docs__: https://github.com/ytdl-org/youtube-dl/blob/5208ae92fc3e2916cdccae45c6b9a516be3d5796/youtube_dl/postprocessor/common.py#L22
      # [2] YoutubeDL.post_process: https://github.com/ytdl-org/youtube-dl/blob/5208ae92fc3e2916cdccae45c6b9a516be3d5796/youtube_dl/YoutubeDL.py#L2095
      #return None

      return [], information



import re
def pct_str2int(x):
  # assert pct_str2int("  0.0%") ==   0
  # assert pct_str2int(" 10.0%") ==  10
  # assert pct_str2int(" 34.0%") ==  34
  # assert pct_str2int("100.0%") == 100
  return int(re.sub(re.compile("\s*(\d+).\d+%"), "\\1", x))


class CounterHook:
    """
    A function that saves state of number of videos downloaded so far
    """
    def __init__(self):
      self.n = 0
      self.fn_prev = None
      self.pct_prev = None

    def __call__(self, d):
      if d['status'] == 'finished':
        self.n += 1
        print(f"Finished downloading {self.n} videos")

      if self.fn_prev != d["filename"]:
        print(f"Starting new download: {d['filename']}")
        self.fn_prev = d["filename"]

      if d['status'] == 'downloading':
        pct_now = (pct_str2int(d["_percent_str"])//10)*10 # count in 10's
        if self.pct_prev != pct_now:
          print(f"Download milestone {d['_percent_str']}")
          self.pct_prev = pct_now


class SkipperPreprocessor:
  """
  From the youtube_dl docs:

  match_filter: A function that gets called with the info_dict of every video.
  If it returns a message, the video is ignored.
  If it returns None, the video is downloaded.
  match_filter_func in utils.py is one example for this.
  """
  def __init__(self, dir_out):
    self.dir_out = dir_out

  def __call__(self, info_dict):
      # limit on duration
      # https://stackoverflow.com/a/53474744/4126114
      MAX_SEC = 20*60
      if info_dict["duration"] > MAX_SEC:
        msg = f"Title too long: '{info_dict['title']}' has duration {info_dict['duration']} seconds > max {MAX_SEC} seconds"
        print(msg)
        return msg

      # check if already run pipeline
      # fn_pkl = f"%(title)s-%(id)s.pkl" % info_dict
      fn_pkl = my_sanitize_filename(info_dict["title"])
      fn_pkl = f"{fn_pkl}-{info_dict['id']}-dolphicom.pkl"
      fn_pkl = os.path.join(self.dir_out, fn_pkl)

      if os.path.exists(fn_pkl):
        msg = f"*.pkl file already processed, skipping download/pipeline: {fn_pkl}"
        print(msg)
        return msg

      fn_ptgz = f"{fn_pkl}.tgz"
      if os.path.exists(fn_ptgz):
        msg = f"*.pkl.tgz file already processed, skipping download/pipeline: {fn_ptgz}"
        print(msg)
        return msg

      msg = f"pkl file not generated yet, will download and run pipeline: {fn_pkl}"
      print(msg)
      return None



class Youtube:
  """
  Update 2021-08:
  This pipeline is deprecated in favor of downloading the audio with youtube-dl and using the dolphicom.pipeline.local_multiple.LocalMultiple pipeline.
  Originally, I created this pipeline because I was only downloading the audio from youtube in the wav format.
  This resulted in large files that did not fit on the disk in colab (100 GB) when I was querying for 250 videos.
  Consequently, I wrote this class to download a file, analyze it, delete the downloaded file, move on to the next video, repeat.
  However, I later figured out how to use youtube-dl to download the audio in compressed formats.
  This allowed me to download the audio of 250 youtube videos and fit them on the colab disk.
  The LocalMultiple pipeline was enough to analyze the downloaded audio.

  --------------------------
  This pipeline can download a list of audio files from youtube and call the local_single.LocalSingle pipeline upon each download.
  The func_done argument can be used to delete the downloaded file to free up space for the next video to download.

  Example:

  from dolphicom.pipeline.remote_youtube import Youtube
  ydl = Youtube(verbose=False)
  ydl.fit(query=["ytsearch250:dolphin bottlenose whistles"], dir_audio="ytq1_audio", dir_out="ytq1_dolphicom")
  """
  def __init__(self, verbose):
    self.verbose = verbose

  def download(self, query, dir_audio, dir_out, func_done=None, with_whistle_extraction=True):
    """
    query: passed to youtube_dl.YoutubeDL(ydl_opts).download(query)
    dir_audio: path in which to store the downloaded audio files from youtube.com
    dir_out: path to folder in which to store results, eg "ws-pipeline_yt_whistles-v20210511a"
    func_done: function or lambda to run on tgz of pkl file of result, eg move to gdrive folder, upload to aws s3. Pass None to skip.
    with_whistle_extraction: True to identify whistles. False to just download the files.
    """
    assert type(query) == list

    ydl_opts = {
        #'format': 'bestaudio/best', # for audio + video
        'format': 'bestaudio', # for audio only

        # Update: after utils.pydub_readaudio, no need for just wav format
        #'postprocessors': [
        #  {'key': 'FFmpegExtractAudio',
        #    #'preferredcodec': 'mp3',
        #    'preferredcodec': 'wav',
        #    'preferredquality': '192',
        #  },
        #],

        'logger': MyLogger(),

        # https://github.com/ytdl-org/youtube-dl/blob/master/README.md#how-do-i-put-downloads-into-a-specific-folder
        "outtmpl": f"{dir_audio}/%(title)s-%(id)s.%(ext)s",

        #'progress_hooks': [my_hook],
        'progress_hooks': [CounterHook()],
    
        "verbose": self.verbose,
        "simulate": False,
        #"daterange": youtube_dl.DateRange(start="20020101"), # , end=""
        "min_views": 10,
        #"max_views": 
        "cachedir": "yt_cache",
        "download_archive": "yt.txt",

        # max_filesize is a number in bytes
        # Update: This filters on the compressed format (m4a) not uncompressed (wav). To get the uncompressed format to be < 200M, pass 20M
        # Note that this will still download partial files (up till the limit), so I'm not sure if it will pass the incomplete file to the pipeline or abort FIXME
        # Update: added limit on number of minutes in SkipperPreprocessor, but keeping this as a safety switch
        "max_filesize": int(20e6),

        # Even-though the CLI arg is --restrict-filenames, the parameter name here doesn't have the dash (unlike other parameters where the dash is an underscore)
        # Filed an issue with youtube-dl https://github.com/ytdl-org/youtube-dl/issues/29442
        "restrictfilenames": True
    }
    
    if with_whistle_extraction:
      ydl_opts["match_filter"] = SkipperPreprocessor(dir_out)
    
    # Query for "dolphin bottlenose whistles"
    #url_q1 = "https://www.youtube.com/results?search_query=dolphin+bottlenose+whistles"
    #url_q1 = "ytsearch25:dolphin bottlenose whistles"
    #url_q1 = "ytsearch250:dolphin bottlenose whistles"
    
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        if with_whistle_extraction:
          ydl.add_post_processor(PipelinePP(downloader=None, dir_out=dir_out, func_done=func_done))

        #ydl.download(['https://www.youtube.com/watch?v=BaW_jenozKc'])

        try:
          ydl.download(query)
        except youtube_dl.DownloadError as e:
          print(f"youtube_dl.DownloadError: {str(e)}")
          if "403" in str(e):
            print("Since http 403 error, trying again")
            ydl.download(query)
          else:
            raise
