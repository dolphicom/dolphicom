from dolphicom.pipeline.base_multiple import BaseMultiple
import os
import shutil
#from tqdm.auto import tqdm
import glob

# https://cloud.google.com/storage/docs/downloading-objects#storage-download-object-python
from google.cloud import storage

from ..utils import readlines_strip, gsutil_ls


class RgsHelper:
  def __init__(self, path_l, dirout_dirname_r, dirout_basename, dir_ws, delete_when_done):
    """
    Helper class to RemoteGs. Arguments share docs.
    Split out of RemoteGs because this is all mostly private methods in RemoteGs,
    and in a helper they'd be public methods.
    """
    self.path_l = path_l
    self.dirout_dirname_r = dirout_dirname_r
    self.dirout_basename = dirout_basename
    self.dir_ws = dir_ws
    self.delete_when_done = delete_when_done


  def init_late(self, bucket_name, dst_bucket_name):
    """
    # set this here in fit_one so that it doesn't need to be pickled and raise an Exception with multiprocessing
    # And if this class is run serially then in parallel, this member needs to be deleted first
    """
    self.bucket_mir = self.get_bucket_obj(bucket_name)
    self.dst_bucket_obj = self.get_bucket_obj(dst_bucket_name)


  def get_bucket_obj(self, bname):
    storage_client = storage.Client()
    bucket_mir = storage_client.bucket(bname)
    return bucket_mir


  def func_done(self, fn_dict):
      """
      Note that fn_tgz will include dirout_basename as a prefix
      """
      fn_k = ["metadata", "tgz", "pkl", "png_full", "png_thumbnails", "csv"]
      fn_metadata, fn_tgz, fn_pkl, fn_png_full, fn_png_thumbnails, fn_csv = [fn_dict[fn_i] for fn_i in fn_k]

      #fn_base = os.path.basename(fn_tgz)
      #fn_dirname = fn_base[:4]
      #assert fn_dirname in ["1705", "1706"]

      #dirout_remote2 = self.bucket_mir.name + "/" + dirout_remote

      #!gsutil cp "$fn_metadata" gs://"$dirout_remote2"/"$fn_metadata"
      self.gsutils_cp_single(fn_metadata)

      #!gsutil cp "$fn_tgz" gs://"$dirout_dirname_r"/"$fn_tgz"
      self.gsutils_cp_single(fn_tgz)

      # before removing the spectrogram directories, copy them to google storage
      if len(glob.glob(os.path.join(fn_png_full, "*")))>0:
        #!gsutil cp -r "$fn_png_full"/* gs://"$dirout_remote2"/"$fn_png_full"
        #!gsutil cp -r "$fn_png_thumbnails"/* gs://"$dirout_remote2"/"$fn_png_thumbnails"
        #!gsutil cp -r "$fn_csv"/* gs://"$dirout_remote2"/"$fn_csv"
        self.gsutils_cp_recursive(fn_png_full)
        self.gsutils_cp_recursive(fn_png_thumbnails)
        self.gsutils_cp_single(fn_csv)


  def gsutils_cp_single(self, x):
    b = self.dst_bucket_obj.blob(os.path.join(self.dirout_dirname_r, x))
    b.upload_from_filename(x)


  def gsutils_cp_recursive(self, d):
    y = glob.glob(d+"/*", recursive=True)
    for x in y: self.gsutils_cp_single(x)


  def fitone_pre(self, fn_remote_mp3, only_check_exists):
    #print(f"N blobs in mirror: {len(list(bucket_mir.list_blobs()))}\n")
    #print("#"*20)
    #print(fn_remote_mp3)
    #print("#"*20)

    blob_remote_mp3 = self.bucket_mir.blob(fn_remote_mp3)
    fn_local_mp3 = os.path.join(self.dir_ws, fn_remote_mp3)
    # Note: do not replace fn_remote_mp3 below with os.path.basename(fn_remote_mp3), o.w. will lose any parent directories
    fn_local_tgz  = os.path.join(self.dirout_basename, fn_remote_mp3.replace(".mp3", ".tgz"))
    fn_local_dir = fn_local_tgz.replace(".tgz", "")
    fn_local_complete_ok = os.path.join(fn_local_dir, "complete_ok.txt")
    fn_remote_complete_ok = os.path.join(self.dirout_dirname_r, fn_local_dir, "complete_ok.txt")
    #fn_local_complete_error = os.path.join(fn_local_dir, "complete_error.txt")
    #fn_remote_complete_error = os.path.join(self.dirout_dirname_r, fn_local_dir, "complete_error.txt")

    # Note: even though the check below will indeed avoid re-running the detector for files that were already done,
    # it isn't enough for parallel processing where the full list is distributed on cores.
    # In that case, with the current mp.Pool.map usage in multiple_base.py, some cores would get a subset of files that are all done, and just terminate early.
    # This would result in not 100% usage of the cores.
    # The necessary step to avoid that would be to reduce the initial list of files before distributing to the cores.
    # Skipping the done files there would ensure that all cores get files that need processing, and no cores are idle early.
    blob_remote_complete_ok = self.dst_bucket_obj.blob(fn_remote_complete_ok)
    if only_check_exists: return blob_remote_complete_ok.exists()

    if blob_remote_complete_ok.exists():
      # note that the pipeline will check for the local file and skip
      print(f"Already uploaded {fn_remote_complete_ok}")
      if os.path.exists(fn_local_tgz): os.remove(fn_local_tgz) # late cleanup
      if os.path.exists(fn_local_dir): shutil.rmtree(fn_local_dir) # late cleanup
      return

    #print(f"{fn_remote_wav} -> {fn_remote_mp3}")

    # core
    fn_post_tuple = fn_local_tgz, fn_local_complete_ok, fn_local_dir, fn_local_mp3
    return blob_remote_mp3, fn_local_mp3, fn_post_tuple


  def fitone_post(self, fn_post_tuple):
    fn_local_tgz, fn_local_complete_ok, fn_local_dir, fn_local_mp3 = fn_post_tuple

    # cleanup
    assert os.path.exists(fn_local_tgz) # otherwise I might have a mistake in the above exists check
    os.remove(fn_local_tgz) # early cleanup

    # the fn_local_complete_ok file is not available yet in func_done, so need to upload it here
    # It indicates the completion of this file (and identify broken uploads/executions)
    #!gsutil cp "$fn_local_complete_ok" gs://"$dirout_dirname_r"/"$fn_local_dir"/
    self.gsutils_cp_single(fn_local_complete_ok)

    shutil.rmtree(fn_local_dir) # continue cleanup

    # final clean up
    if not self.delete_when_done:
      os.remove(fn_local_mp3)


  def drop_complete_ok(self):
    """
    Check note in fitone_pre for variable blob_remote_complete_ok
    """
    #gs_uri = f"gs://{self.bucket_mir.name}/{self.dirout_dirname_r}/{dirout_basename}/**/**/complete_ok.txt"
    #!gsutil ls "$gs_uri" > ls_complete_ok.txt
    #!sed -i "s/gs:\/\/dolphicom\/analyses\/{dirout_basename}\///g" ls_complete_ok.txt
    #!wc -l ls_complete_ok.txt
    #!head -n3 ls_complete_ok.txt
    #!tail -n3 ls_complete_ok.txt
    #fn_ls = readlines_strip("ls_complete_ok.txt", header=None)

    print("Filtering for files that already not yet run through dolphicom:")

    # FIXME perhaps cache this result for 5 minutes?
    fn_ls = gsutil_ls(self.dst_bucket_obj.name, prefix=os.path.join(self.dirout_dirname_r, self.dirout_basename), suffix="complete_ok.txt")

    if len(fn_ls)==0:
      print("Found none. Will run on all")
      return len(self.path_l), self.path_l

    fn_ls = [x.replace("/complete_ok.txt", ".mp3") for x in fn_ls]

    fn_missing = set(self.path_l).difference(set(fn_ls))
    fn_missing = sorted(list(fn_missing))
    #len(fn_missing), fn_missing[:5], fn_missing[-5:]

    n_all = len(self.path_l)
    n_missing = len(fn_missing)
    if n_all == n_missing:
      raise Exception("Internal error. Found files with complete_ok.txt ubt did not drop them from full list. Aborting. Requires fix in code")

    print(f"Stats on number of files: Full: {n_all}. Done: {n_all - n_missing}. Todo: {n_missing}.")
    return n_missing, fn_missing




class RemoteGs(BaseMultiple):
  def __init__(self, bucket_name, path_l, dst_bucket_name, dirout_dirname_r, dirout_basename, dir_ws, delete_when_done):
    """
    Class that reads audio files in a GS bucket, iterates on a list of files in it, and runs the dolphicom pipeline on each.
    It skips files that are already done.
    It uploads results back into the GS bucket.

    bucket_name: Name of a GS bucket.
       Cannot directly pass a google storage bucket object because otherwise python.multiprocessing
       will complain that it's not picklable
    path_l: list of paths in bucket to read as audio files
    dst_bucket_name: destination bucket name
    dirout_dirname_r: path in bucket in which to put the dirout_basename and its contents
    dirout_basename: name of local folder to put results into
    dir_ws: name of local folder in which to temporarily download audio files
    delete_when_done: True to delete the downloaded audio file (in `dir_ws`) on pipeline completion, False to keep it

    Contents of dir_ws and dirout_basename will be automatically deleted
    as the class iterates through the files in path_l
    """
    self.bucket_name = bucket_name
    self.dst_bucket_name = dst_bucket_name
    self.helper = RgsHelper(path_l, dirout_dirname_r, dirout_basename, dir_ws, delete_when_done)
    super().__init__(dir_out=dirout_basename, func_done=self.helper.func_done, dir_drop_prefix=dir_ws)


  def fit_one(self, pool_in_i):
    """
    Over-ride the parent definition
    """
    i, n, fn_remote_mp3 = pool_in_i

    self.helper.init_late(self.bucket_name, self.dst_bucket_name)

    fn_pre_tuple = self.helper.fitone_pre(fn_remote_mp3, False)
    if fn_pre_tuple is None: return

    blob_remote_mp3, fn_local_mp3, fn_post_tuple = fn_pre_tuple
    if not os.path.exists(fn_local_mp3):
      os.makedirs(os.path.dirname(fn_local_mp3), exist_ok=True)
      blob_remote_mp3.download_to_filename(fn_local_mp3)

    pii2 = i, n, fn_local_mp3
    super().fit_one(pii2)
    self.helper.fitone_post(fn_post_tuple)


  def get_file_iterator(self):
    #n_all = len(self.path_l)
    n_missing, path_missing = self.helper.drop_complete_ok()
    #g = enumerate(self.path_l)
    g = ((i, n_missing, fn_wav) for i, fn_wav in enumerate(path_missing))
    return g, n_missing


  def generate_html_gallery(self, html_title_prefix):
    """
    Over-ride parent def because need to mirror gs dir struct locally first
    """
    # make sure that the dirout_basename is empty
    assert len(glob.glob(f"{self.helper.dirout_basename}/**/*", recursive=True)) == 0

    # copy dir structure from google storage to local without download
    from ..io.gs_dir_struct_mirror import GsDirStructMirror
    dirout_dirname_l = "." # FIXME move to constructor and integrate better
    GsDirStructMirror.run(self.bucket_name, self.helper.dirout_dirname_r, self.helper.dirout_basename, dirout_dirname_l)

    # call parent def to create html
    super().generate_html_gallery(html_title_prefix)

    print("To upload, use .upload_html_gallery(dry_run=False)")


  def upload_html_gallery(self, dry_run):
    if dry_run: print("*****START OF DRY RUN UPLOAD******")

    # sync to gs
    d1 = f"{self.helper.dirout_basename}/html"
    d2 = f"gs://{self.dst_bucket_name}/{self.helper.dirout_dirname_r}/{self.helper.dirout_basename}/html"
    cmd = f'gsutil -m rsync "{d1}" "{d2}"'
    print(f"{'(DRY RUN)' if dry_run else '(EXECUTING)'} Command: {cmd}")
    if not dry_run:
      res = os.system(cmd)
      assert res==0
      print(f"Uploaded \n from '{d1}' \n to '{d2}'")

    msg_hint = f"""
To set public read access on html only:
!gsutil -m acl set -R -a public-read \
  "gs://{self.bucket_name}/{self.helper.dirout_dirname_r}/{self.helper.dirout_basename}/html"/* 2> /dev/null

To set public read on both html folder and spectrogram_png and spectrogram_thumbnails subfolders
(ETA ~ 1h for 127k files)
!gsutil -m acl set -R -a public-read \
  "gs://{self.bucket_name}/{self.helper.dirout_dirname_r}/{self.helper.dirout_basename}"/* 2> /dev/null

PS: Remember to set bucket permissions in cloud console to "fine-grained" instead of "uniform bucket-level access"

Gallery will be accessible at:
https://storage.googleapis.com/{self.bucket_name}/{self.helper.dirout_dirname_r}/{self.helper.dirout_basename}/html/index.html
"""
    print(msg_hint)
    if dry_run: print("*****END OF DRY RUN UPLOAD******")
