import os
from ..core.spectrogram import AbortPipeline
from ..core.audiofile import AudioFile
import datetime as dt
import tarfile
import shutil
from ..utils import pngdir_to_thumbnails, corpus_to_csv, os_touch
#from .io.gallery_html_generator import GalleryHtmlGenerator
import traceback
from .. import dolphicom_version


class LocalSingle:
  def __init__(self, dir_out, func_done=None, generate_gallery=True, dir_drop_prefix=None):
    """
    This is the most fundamental pipeline and is used in the other pipeline classes.

    func_done - function to execute on generated files, eg pkl.tgz file. Pass None to skip.
          Will be called as self.func_done(fn_dict)
          where fn_dict is a dictionary with keys: metadata, tgz, pkl, png_full, png_thumbnails, csv
          More keys could be added later

    generate_gallery - boolean: true to generate full-size spectrograms and thumbnails, False to skip

    dir_drop_prefix - dirname prefix from each filepath passed to fit() that needs to be dropped. Do not include the "/". Use None to skip
    """
    self.dir_out = dir_out
    #assert not dir_out.endswith("/")

    self.func_done = func_done
    self.generate_gallery = generate_gallery

    self.dir_drop_prefix = dir_drop_prefix
    if dir_drop_prefix is not None:
      assert not dir_drop_prefix.endswith("/")


  def fit(self, fn_in, include_s22b_connect_broken_whistles=True, include_s23_blur=True, include_s23_hdbscan=True, include_s23_mender=True):
      """
      Parameters:
      include_s22b_connect_broken_whistles: False to skip. Passed as is to AudioFile.__init__()
      include_s23_blur -- False to skip meijering (which contributes to mending)
      include_s23_hdbscan -- False to skip hdbscan (contributes to mending)
      include_s23_mender -- False to skip mender in step s23 (hdbscan segmenter)
      """
      if self.dir_drop_prefix is None:
        fn_use = fn_in
      else:
        assert fn_in.startswith(self.dir_drop_prefix+"/")

        # drop the length of the prefix + 1 for "/". No need for another + 1 to start at the next letter since 0-based indexing
        fn_use = fn_in[len(self.dir_drop_prefix)+1:]



      # create a dir for the dir_out folder to put everything in it
      os.makedirs(self.dir_out, exist_ok=True)

      # build path to subdir and pkl within
      #fn_exp = os.path.join(
      #    #"/content/ws-pipeline_yt_whistles-v20210511a",
      #    dir_out,
      #    #os.path.basename(fn_in).replace(".wav", ".mp4") # to check generated video
      #    os.path.basename(fn_in).split(".")[0] # to check dir
      #  )
      #os.makedirs(fn_exp, exist_ok=True)

      # Update: put the pkl file in dir_out directly
      # This check is redundant with the SkipperPreprocessor, but keeping it anyway because it caught some stuff with which I had a bug there
      #fn_pkl = os.path.join(fn_exp, "pipeline.pkl")
      #fn_pkl = os.path.join(self.dir_out, os.path.basename(fn_in).split(".")[0] + ".pkl")

      # Update: just use the dirname of fn_use
      # fn_parent = os.path.basename(os.path.dirname(fn_in)) # if no parent, this will just be "", and if passed to os.path.join(...), it will just get ignored
      fn_parent = os.path.dirname(fn_use)

      fn_base = os.path.join(fn_parent, os.path.splitext(os.path.basename(fn_in))[0]) # keep the filename as dir + prefix with parent dirname
      fn_dir = os.path.join(self.dir_out, fn_base)
      os.makedirs(fn_dir, exist_ok=True)
      fn_pkl = os.path.join(self.dir_out, fn_base, "dolphicom_audiofile.pkl")
      fn_png_full = os.path.join(self.dir_out, fn_base, "spectrogram_png")

      fn_png_thumbnails = os.path.join(self.dir_out, fn_base, "spectrogram_thumbnails")
      fn_csv =  os.path.join(self.dir_out, fn_base, "corpus.csv")
      fn_tgz = os.path.join(self.dir_out, fn_base+'.tgz')
      fn_complete_ok =  os.path.join(self.dir_out, fn_base, "complete_ok.txt")
      fn_complete_error =  os.path.join(self.dir_out, fn_base, "complete_error.txt")
      fn_metadata =  os.path.join(self.dir_out, fn_base, "metadata.txt")

      # After version v0.4.3, moving to a .tgz file
      fn_ptgz = os.path.join(self.dir_out, fn_base+"-dolphicom.pkl.tgz")
      if os.path.exists(fn_ptgz):
        msg = f"File *.pkl.tgz exists, but that's the format from dolphicom v0.4.3 and earlier. The currently installed dolphicom version is {dolphicom_version}. Please install v0.4.3 to continue this calculation."
        print(msg)
        raise Exception(msg)

      # After version v0.4.3, need not just pkl file, but gallery and corpus files as well, so just the tgz file.
      #if os.path.exists(fn_pkl):
      #  print(f"File already processed, skipping: {fn_in}")
      #  return

      # prior to version 0.5.2, check the tgz file
      #if os.path.exists(fn_tgz):
      #  print(f"Tgz file already exists, so file was already processed. Skipping: {fn_in}")
      #  return

      # As of v0.5.2, check the complete_ok.txt or complete_error.txt file
      if os.path.exists(fn_complete_ok):
        print(f"{os.path.basename(fn_complete_ok)} file already exists, so file was already processed. Skipping: {fn_in}")
        return

      if os.path.exists(fn_complete_error):
        print(f"{os.path.basename(fn_complete_error)} file already exists, so file was already processed. Skipping: {fn_in}")
        return


      #p_pre = f"\nFile {n}: {fn_in[:30]}..."
      p_pre = f"\nFile: {fn_in[:30]}..."
      print(f"{p_pre}: Start: {dt.datetime.now()}")

      # save a metadata file with some info
      os_touch(fn_metadata, f"Date: {dt.datetime.now()}\nDolphicom version: {dolphicom_version}")

      # core of function
      p = AudioFile(fn_in, channel=0, spectrogram__generic_func=None, include_s22b_connect_broken_whistles=include_s22b_connect_broken_whistles, include_s23_blur=include_s23_blur, include_s23_hdbscan=include_s23_hdbscan, include_s23_mender=include_s23_mender)
      try:
        _ = p.fit(remove_noise__fast=False)
      except AbortPipeline as e:
        print(f"{p_pre}: AbortPipeline Error: {e}")

      except Exception as e:
        # print stack trace
        # https://stackoverflow.com/questions/52742612/how-to-print-the-stack-trace-of-an-exception-object-in-python#52742770
        print(f"{p_pre}: Exception error: {e}")
        traceback.print_exception(type(e), e, e.__traceback__)
        os_touch(fn_complete_error, str(e))
        return

      except AssertionError:
        # http://stackoverflow.com/questions/11587223/ddg#11587247
        _, _, tb = sys.exc_info()
        traceback.print_tb(tb) # Fixed format
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]
        msg = 'An assertion error occurred on line {} in statement {}'.format(line, text)
        print(msg)
        os_touch(fn_complete_error, msg)
        return


      # save pkl
      p.to_pickle(fn_pkl, all_checkpoints=False)
      #fn_out = p.create_video(full=False, video_type="intermediate", with_marker=False)
      print(f"{p_pre}: Saved dolphicom audiofile object to {fn_pkl}")

      if self.generate_gallery:
        if hasattr(p, "wm"):

          # mkdir only after successful execution of spectrogram processing
          os.makedirs(fn_png_full, exist_ok=True)
          p.wm.generate_gallery(fn_png_full)
          print(f"{p_pre}: generated spectrogram galleries for timespans with detected whistles in {fn_png_full}")

          os.makedirs(fn_png_thumbnails, exist_ok=True)
          pngdir_to_thumbnails(fn_png_full, fn_png_thumbnails)
          print(f"{p_pre}: generate thumbnails of spectrogram gallery in {fn_png_thumbnails}")

          # generating html file to browse the images
          # is meant to be run on a "full folder" of parents of spectrograms_png
          # As such, it doesn't belong here


        else:
          print(f"{p_pre}: no whistles => no gallery")

      if not hasattr(p.sp_filt, "s23_whistle_corpus"):
        print(f"{p_pre}: no whistle corpus => no csv")
      else:
        if p.sp_filt.s23_whistle_corpus.shape[0] == 0:
          print(f"{p_pre}: whistle corpus exists but is empty => no csv")
        else:
          df_corpus = corpus_to_csv(p.sp_filt.s23_whistle_corpus, fn_csv)
          print(f"{p_pre}: Saved corpus csv file to {fn_csv}")

      # tar the pkl file to save space
      with tarfile.open(fn_tgz, "w:gz") as tar:
        if os.path.exists(fn_pkl): tar.add(fn_pkl)
        if os.path.exists(fn_png_full): tar.add(fn_png_full, recursive=True)
        if os.path.exists(fn_png_thumbnails): tar.add(fn_png_thumbnails, recursive=True)
        if os.path.exists(fn_csv): tar.add(fn_csv)

      if self.func_done is not None:
        fn_dict = {
          "metadata": fn_metadata,
          "tgz": fn_tgz,
          "pkl": fn_pkl,
          "png_full": fn_png_full,
          "png_thumbnails": fn_png_thumbnails,
          "csv": fn_csv,
        }
        self.func_done(fn_dict)

      # drop the pkl file since it's taking up space and is already available via *.pkl.tgz file
      os.remove(fn_pkl)

      # create a complete.txt file to use for tracking which pipeline executions were done
      os_touch(fn_complete_ok)

      print(f"{p_pre}: End  : {dt.datetime.now()}")
      return
