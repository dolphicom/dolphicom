from .base_multiple import BaseMultiple
import glob

class LocalGlob(BaseMultiple):
  def set_glob_str(self, glob_str):
    """
    glob_str: string to pass to python's glob.glob(), eg "dirname/*.wav"
    """
    self.glob_str = glob_str


  def get_file_iterator(self):
    if not hasattr(self, "glob_str"):
      raise Exception("Need to call LocalGlob.set_glob_str(glob_str) first")

    fn_l = glob.glob(self.glob_str, recursive=True)
    fn_l = sorted(fn_l)
    n = len(fn_l)
    #g = (fn_wav for fn_wav in fn_l)
    #g = enumerate(fn_l)
    g = ((i, n, fn_wav) for i, fn_wav in enumerate(fn_l))
    return g, n
