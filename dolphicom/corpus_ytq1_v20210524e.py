from matplotlib import pyplot as plt

def plot_corpus_examples(sp_filt):
  sp_filt.plot_corpus(0, 5) # 3 whistles up
  sp_filt.plot_corpus(15, 20) # 7.4khz noise
  sp_filt.plot_corpus(22, 27) # one false positive
  sp_filt.plot_corpus(35, 40) # multiple flat'ish whistles, some under 6khz
  sp_filt.plot_corpus(42, 47) # line down
  sp_filt.plot_corpus(int(2*60+12), int(2*60+12)+5) # big whistle
  sp_filt.plot_corpus(int(2*60+47), int(2*60+47)+5, "all", 2) # big whistle
  sp_filt.plot_corpus(4*60+15, 4*60+15+5) # big whistle
  sp_filt.plot_corpus(4*60+20, 4*60+20+5) # big whistle
  sp_filt.plot_corpus(550, 555) # false positives
  sp_filt.plot_corpus(3608, 3608+5) # vertically overlaid
  sp_filt.plot_corpus(int(2*60+52), int(2*60+52)+5, "all", 2) # big whistle


from .spectrogram_helpers.s43_transliterator import S43WhistleToBraille
def plot_s43_examples(whistle_corpus3):
  for l in [765, 888, 233, 104, 548, 261]:
    S_1d = whistle_corpus3.loc[l].S_1d
    b = S43WhistleToBraille(True).piecewise(S_1d)
     
    plt.plot(S_1d)
    plt.title(b)
    plt.show()
    print(f"braille: '{b}'")
    # with comp + thresh: ⠤⠄⠊
    # with comp, without thresh: ⠤⣀⡠⠄⠊
    # without comp: ⠤⢄⣀⣀⣀⣀⡠⠤⠤⠄⠊


# FIXME: should figure out how to integrate these tests without major hard-coding
#def assert_s43_examples(whistle_corpus3):
#  assert S43WhistleToBraille(False).piecewise(whistle_corpus3.loc[765].S_1d) == "" # iloc[822]
#  assert S43WhistleToBraille(False).piecewise(whistle_corpus3.loc[888].S_1d) == "⢁" # "⢁⡀" # less redundant than "⢄⣀"
#  assert S43WhistleToBraille(False).piecewise(whistle_corpus3.loc[233].S_1d) == "⢁"
#  assert S43WhistleToBraille(False).piecewise(whistle_corpus3.loc[104].S_1d) == "⢄⠂" # "⢄⠄"
