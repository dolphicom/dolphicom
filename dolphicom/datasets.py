"""
In the spirit of sklearn.datasets [1],
provide some convenience classes for analyzing datasets without worrying about where they are.

[1] https://scikit-learn.org/stable/modules/classes.html#module-sklearn.datasets
"""

from .utils import readlines_strip, gsutil_ls
from .pipeline.remote_gs import RemoteGs
from google.cloud import storage
import os


class GsBase:
  """
  Base class for datasets on dolphicom's google storage bucket
  Derived classes set values for class members: files_prefix, files_suffix, files_subset_whistles (optional)
  Check derived classes docs for Usage examples.
  """
  # bucket name on google storage
  bucket_name = "dolphicom"

  # path in bucket containing the audio files
  files_prefix = None

  # suffix to filter files in gs://{self.bucket_name}/{self.files_prefix}/**/*
  files_suffix = None

  # Text file containing subset of filenames with whistles from gs://{self.bucket_name}/{self.files_prefix}/**/*{self.files_suffix}
  # Set to None if want to use full files list
  files_subset_whistles = None


  def __init__(self):
      if self.files_prefix is None: raise ValueError("Derived class must set self.files_prefix")
      if self.files_suffix is None: raise ValueError("Derived class must set self.files_suffix")


  def get_detector(self, dst_bucket_name, dirout_dirname_r, dirout_basename, dir_ws, delete_when_done):
    #dirout_basename = "t17.1-f1-v20210811a-noaa_pifsc-pipeline"
    #dirout_basename = "t17.1-f1-v20210819a-noaa_pifsc-pipeline"
    #dirout_basename = "t17.1-f1-v20210824a-noaa_pifsc-pipeline"
    path_l = self._get_path_l()
    rgs = RemoteGs(self.bucket_name, path_l, dst_bucket_name, dirout_dirname_r, dirout_basename, dir_ws, delete_when_done)
    return rgs


  def _get_path_l(self):
    if self.files_subset_whistles is None:
      return gsutil_ls(self.bucket_name, prefix=self.files_prefix, suffix=self.files_suffix, drop_prefix=False)

    # according_to_dolphicom_050(self):
    fn_d050_uri = f"gs://{self.bucket_name}/{self.files_subset_whistles}"
    print(f"Filtering for whistles from {fn_d050_uri}:")

    #!gsutil cp "$gs_uri" .

    fn_d050_local = os.path.basename(self.files_subset_whistles)

    if not os.path.exists(fn_d050_local):
      storage_client = storage.Client()
      bucket_obj = storage_client.bucket(self.bucket_name)
      blob_d050_txt = bucket_obj.blob(self.files_subset_whistles)
      if not blob_d050_txt.exists():
        raise ValueError(f"File not found: {fn_d050_uri}. Please fix in dolphicom source.")

      blob_d050_txt.download_to_filename(fn_d050_local))

    fn_d050_l = readlines_strip(fn_d050_local)
    print(f"Found {len(fn_d050_l)} files with whistles")
    print(f"First 3: {fn_d050_l[:3]}")
    print(f"Last 3: {fn_d050_l[-3:]}")

    if self.files_prefix is not None:
      fn_d050_l = [self.files_prefix+x for x in fn_d050_l]

    return fn_d050_l


class Dclde2020Mp3Ch0(GsBase):
  """
  My mp3 mirror of the DCLDE 2020 dataset.
  Using mp3 to compress audio files, albeit limits to 20khz (some dolphin/whale calls go up to 50khz?).
  Only the first channel is exported from the original uncompressed wav files.

  Related links:
  - Workshop homepage: http://www.soest.hawaii.edu/ore/dclde/
  - GCP Marketplace: https://console.cloud.google.com/marketplace/product/noaa-public/noaa-pifsc-bioacoustic
  - GCP Storage: https://console.cloud.google.com/storage/browser/noaa-pifsc-bioacoustic
  - GCP Bigquery (accompanying metadata): https://console.cloud.google.com/bigquery?p=bigquery-public-data&d=noaa_pifsc_metadata&page=dataset

  Note: The label "2020" in "DCLDE 2020" is different than the workshop homepage listing "2022" because it was originally scheduled for 2020, but was postponed to 2022 due to COVID-19

  Usage:
  from dolphicom.datasets import Dclde2020Mp3
  detector = Dclde2020Mp3Ch0().get_detector(dst_bucket_name="dolphicom", dirout_dirname_r="analyses", dirout_basename="t17.1-f1-v20210824a-noaa_pifsc-pipeline", dir_ws="workspace", delete_when_done=True)
  detector.fit(n_jobs=1)

  where detector is a dolphicom.pipeline.remote_gs.RemoteGs object
  """
  # path in bucket containing the audio files
  files_prefix = "datasets/DCLDE-2020--noaa-pifsc-bioacoustic--mirror-mp3"

  # suffix to filter files in gs://{self.bucket_name}/{self.files_prefix}/**/*
  # Note that in my export step, I used "1" in the suffix, but this is not to be confused with "Ch0" that I have here in the class name. I'm using "0" in the classname because python is 0-based. I probably should have used "9" in the file suffix as well, but... (shrug)
  files_suffix = "_ch_1.mp3"

  # Text file containing subset of filenames with whistles from gs://{self.bucket_name}/{self.files_prefix}/**/*{self.files_suffix}
  # Set to None if want to use full files list
  files_subset_whistles = "analyses/t17.5-f4-v20210813a-noaa_pifsc-files_with_whistles_by_dolphicom_050.txt"


class Dclde2018Mp3Ch0(GsBase):
  """
  My mp3 mirror of the DCLDE 2018 dataset.
  Workshop page: http://sabiod.lis-lab.fr/DCLDE/
  Remaining documentation is like Dclde2020Mp3Ch0
  """
  # path in bucket containing the audio files
  files_prefix = "datasets/DCLDE 2018 - NOAA - mirror_mp3_ch0"

  # suffix to filter files in gs://{self.bucket_name}/{self.files_prefix}/**/*
  files_suffix = ".mp3"

  # Text file containing subset of filenames with whistles from gs://{self.bucket_name}/{self.files_prefix}/**/*{self.files_suffix}
  # Set to None if want to use full files list
  files_subset_whistles = None


class PamguardAudioSamples(GsBase):
  """
  Audio files from pamguard at https://sourceforge.net/projects/pamguard/files/soundFiles/
  Remaining documentation is like Dclde2020Mp3Ch0
  except can use delete_when_done=False since small size.
  Audio files will be left after download in dir_ws
  """
  # path in bucket containing the audio files
  files_prefix = "datasets/pamguard-audio-samples"

  # suffix to filter files in gs://{self.bucket_name}/{self.files_prefix}/**/*
  files_suffix = ".wav"

  # Text file containing subset of filenames with whistles from gs://{self.bucket_name}/{self.files_prefix}/**/*{self.files_suffix}
  # Set to None if want to use full files list
  files_subset_whistles = None
