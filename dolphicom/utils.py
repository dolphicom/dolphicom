import glob
import os
def my_rm_glob(pattern):
  for filename in glob.glob(pattern):
      os.remove(filename)



import numpy as np
import pandas as pd
def np_wide_to_long_pd(Sxx_use2):
    if np.isnan(Sxx_use2).any():
      raise ValueError("Error in np_wide_to_long_pd: Please replace nan values from Sxx before passing to this function")

    Sxx_use3 = np.where(Sxx_use2!=0)
    Sxx_use3 = np.transpose(np.array(Sxx_use3))
    ind = np.lexsort((Sxx_use3[:,0], Sxx_use3[:,1])) # sort time first, then frequency
    Sxx_use3 = Sxx_use3[ind]

    r, c = Sxx_use3[:,0], Sxx_use3[:,1]
    Sxx_use4 = np.column_stack((r.ravel(), c.ravel(), Sxx_use2[r, c].ravel()))

    Sxx_use5 = pd.DataFrame(Sxx_use4, columns=["f_idx","t_idx","S"])
    Sxx_use5["f_idx"] = Sxx_use5["f_idx"].astype(np.int64)
    Sxx_use5["t_idx"] = Sxx_use5["t_idx"].astype(np.int64)
    Sxx_use5["S"] = Sxx_use5["S"].astype(np.float64)

    return Sxx_use5



"""
Due to https://github.com/pandas-dev/pandas/issues/11617
and https://stackoverflow.com/questions/58531998/prevent-coercion-of-pandas-data-frames-while-indexing-and-inserting-rows
pandas will coerce datatypes when .loc and .iloc return a single row (Series)

Update: ended up solving my particular situation with changing iteration from iterrows to itertuple,
so I don't need this anymore

Example:

import pandas as pd

df = pd.DataFrame({"a": [1,2,3], "b":[1.1,2.2,3.3]})
print(df.iloc[0].a)
# 1.0, coerced to float

iloc_nocoerce = lambda df, i: list(df.iloc[[i]].itertuples())[0]
print(iloc_nocoerce(df, 0).a)
# 1, not coerced
"""



import numpy as np
def np_apply_piecewise_notnan(S, func_isnan, func_notnan, verbose):
    """
    Split signal on nan and apply function on nan segment and non-nan segment separately
    """

    l = np.diff(~np.isnan(S), prepend=0)
    l[l<0] = 0
    l = np.cumsum(l)
    if verbose: print(f"Number of non-nan segments: {l.max()}")
    if l.max() == 0:
      #raise ValueError("All nan passed!")
      o = func_isnan(S)
      return o

    def do_yield():
      for i in range(l.max()+1):
        v = S[l==i]
        if i==0 and v.shape[0]>0:
          yield func_isnan(v)
          continue

        idx_nan = np.isnan(v)
        yield func_notnan(v[~idx_nan])
        if idx_nan.any(): yield func_isnan(v[ idx_nan]) # trailing nan

    o = list(do_yield())
    if verbose: print(o)
    o = np.concatenate(o)
    return o



from scipy.signal import savgol_filter
import numpy as np
def np_rolling_savgol_piecewise(S, window):
    """
    Split signal on nan and calculate moving average of each separately
    Use Savitzky-Golay filter to follow trace more closely than regular moving average
    Also, savgol filter doesn't deal with nan, so needs a piece-wise wrapper
    """

    verbose = False
    win_bkp = 11

    # pass-through nan's
    func_isnan = lambda v: v

    def func_notnan(v):
      if verbose: print(f"savgol input shape: {v.shape[0]}. Input: {v}")
      if v.shape[0] >= window:
        v = savgol_filter(v, window, 2)
      else:
        # fall back onto small window length
        if v.shape[0] >= win_bkp:
          v = savgol_filter(v, win_bkp, 2)
        else:
          # for small segments, just replace with flat mean
          # with drop nan above, no need for np.nanmean
          v = [np.mean(v)]*v.shape[0]

      return v

    S = np_apply_piecewise_notnan(S, func_isnan, func_notnan, verbose)

    return S



def close_to_reference(zero_or_not, examples_A, examples_B):
  """
  Return a boolean marking numbers in zero_or_not that are close to examples_A

  examples_A: list of numbers to be considered in the reference class, eg [0] or [0,1,2]
  examples_B: same as examples_A, but for those NOT considered as reference class, eg [100] or [50,100] (to include a midpoint)
  """
  import hdbscan

  assert type(zero_or_not) == list
  assert type(examples_A) == list
  assert type(examples_B) == list
  if len(zero_or_not)==0: return []

  # append several 0's so that hdbscan understands that there is a group there
  # append the max as well, to create a center of gravity there
  # Choose len(zero_or_not)*X as a repetition, and set min_samples in hdbscan constructor to it.
  # What this does is that it makes it "impossible" for list entries other than 0 and max to create their own cluster
  # since they will never be able to gather "min_samples" points on their own
  # Update: also adding max/2 as a cluster
  # Update: -1 so that each centroid has the exact same repetitions, otherwise hdbscan would start returning -1 for confusion
  # Update: +5 so that each centroid can get its own label
  min_samples = len(zero_or_not) * 2
  #artificial_pts = [max(zero_or_not), max(zero_or_not)//2]*min_samples + [0]*min_samples
  #artificial_pts = (examples_A + examples_B)*min_samples
  artificial_pts = []
  for x in examples_A + examples_B:
    artificial_pts += [x]*(min_samples - (1 if x in zero_or_not else 0) + 5)

  hdb_in = np.array(list(set(zero_or_not)) + artificial_pts)

  segmenter = hdbscan.HDBSCAN(min_cluster_size=2, min_samples=min_samples, allow_single_cluster=True).fit(hdb_in.reshape(-1, 1))

  # some verbosity
  if False:
    verb_df = pd.DataFrame({"v":hdb_in,"l":segmenter.labels_, "p": segmenter.probabilities_})
    verb_stat = verb_df.groupby("l").v.apply(lambda x: [(i, x.tolist().count(i)) for i in sorted(list(set(x.tolist())))])
    #print(verb_df.transpose())
    print(verb_df)
    #print(verb_df.p.values)
    print(verb_stat)

  # drop uncertain labels
  #segmenter.labels_[segmenter.probabilities_ < .9] = -1

  in_A = np.array([x in examples_A for x in hdb_in])
  labels_A = segmenter.labels_[in_A] # a number in A, eg 0, could have 2 labels in edge cases
  labels_A = np.array([x for x in labels_A.tolist() if x!=-1]) # do not let -1 (hdbscan => unclassified) to be considered as class A
  is_zero_1 = [x in labels_A for x in segmenter.labels_]

  is_zero_1 = is_zero_1[:-len(artificial_pts)] # drop the manually added values
  hdb_in    = hdb_in[:-len(artificial_pts)] # drop the manually added values

  #print(is_zero_1, hdb_in)
  is_zero_1 = hdb_in[is_zero_1].tolist()

  is_zero_2 = [x in is_zero_1 for x in zero_or_not]

  assert len(zero_or_not) == len(is_zero_2)
  return is_zero_2


def close_to_zero(zero_or_not):
  if len(zero_or_not)==0: return []
  # Update: use only 0 as examples_A so that hdbscan doesn't get confused and set -1 when it intendes one of the values
  # Also, for the max, only add the 25% value if sufficiently large
  #return close_to_reference(zero_or_not, [0, 1, 2], [max(zero_or_not)//4, max(zero_or_not)])
  mymax = max(zero_or_not)
  examples_B = [mymax] if mymax < 20 else [mymax//4, mymax]
  return close_to_reference(zero_or_not, [0], examples_B)



import os
def print_filesize(fn_x):
  print("#"*60)
  #!du -sh "$fn_x"
  fn_s = os.path.getsize(fn_x)
  if fn_s > 0:
    fn_s = fn_s//1000000
    if fn_s == 0: fn_s = "< 1"

  print(f"{fn_s} MB \t {fn_x}")
  print("#"*60)



class AbortPipeline(Exception):
  """
  Exception class to use to abort the pipeline at any point
  """
  pass



from tqdm.auto import tqdm
def iter_batch(Sxx_sub, b_size, tqdm__desc):
    b_start = np.arange(0, Sxx_sub.shape[1], b_size).astype(int)
    b_end = pd.Series(b_start).shift(-1).fillna(Sxx_sub.shape[1]).values.astype(int)

    # Last entry in batches might be too small, so merge it with the previous one
    if b_start.shape[0] > 2:
        b_start = b_start[:-1]
        b_end[-2] = b_end[-1]
        b_end = b_end[:-1]

    for i_start, i_end in zip(tqdm(b_start, desc=tqdm__desc), b_end):
      yield i_start, i_end, Sxx_sub[:, i_start:i_end]


import numpy as np
def np_index_at_value(a, v):
    """
    Kind of a general purpose utility function
    Currently using it to find values in the t and f vectors, which are usually sorted and have equidistant points.
    """
    if a.shape[0]==0:
      raise ValueError("Empty array")

    if a.shape[0]==1:
      if a[0]!=v:
        raise ValueError("Array with shape==1 but not as requested value. Aborting")

    tol = a[1] - a[0] # tolerance is sampling rate
    i = np.argmin(np.abs(a - v))
    if np.abs(a[i] - v) > tol:
      amin = a.min() # a[0]
      amax = a.max() # a[-1]
      raise ValueError(f"Failed to find index in array with value = {v} and tolerance {tol}. Min/Max values are ({amin}, {amax}).")

    return i


# Based on
# https://stackoverflow.com/a/52593312/4126114
# http://stackoverflow.com/questions/38015319/ddg#38021664
from pydub import AudioSegment
import numpy as np
def pydub_readaudio(fn_in, t_sec_start, t_sec_end):
  # fn_in: could be wav, or m4a file
  audio = AudioSegment.from_file(fn_in)

  # Update: for some reason, pydub doesn't do both conditions when run one step at a time
  if (t_sec_start is not None) & (t_sec_end   is not None):
    audio = audio[t_sec_start*1000:t_sec_end*1000]
  else:
    if t_sec_start is not None: audio = audio[t_sec_start*1000:]
    if t_sec_end   is not None: audio = audio[:t_sec_end*1000]

  print(f"Reading file of length {int(audio.duration_seconds)} seconds (possibly truncated with t_sec_start and/or t_sec_end): '{fn_in}'")

  samples = audio.get_array_of_samples()
  f3 = np.reshape(samples, (-1, audio.channels))
  return audio.frame_rate, f3


from matplotlib import pyplot as plt
def my_imshow(x, t):
    fig = plt.figure(figsize=(20,3))
    plt.imshow(x, origin="lower")
    plt.title(t)
    plt.colorbar()
    plt.show()


# Update: cannot use multiprocessing if this were a function returned from a function,
# i.e. def get_func_done(dir_out):
#          def func_done(fn_tgz): ...
#          return func_done
#
# but class with __call__ works
import os
class FuncTgzS3Cp:
  def __init__(self, dir_out):
    self.dir_out = dir_out

  def __call__(self, fn_tgz):
    # Copy to aws s3
    fn_remote = f"s3://mobysound.org-mirror/dolphicom_results/{self.dir_out}/{os.path.basename(fn_tgz)}"

    # colab/jupyter just replaces the !... with os.system(...)
    #!aws s3 cp fn_tgz fn_remote
    os.system(f"aws s3 cp '{fn_tgz}' '{fn_remote}'")

    print(f"Copied to aws s3:")
    print(f"\tfrom: {fn_tgz}")
    print(f"\tto: {fn_remote}")



import os, sys
from PIL import Image
def pngdir_to_thumbnails(dir_in, dir_out):
  """
  Generate thumbnails for a folder of png files
  Based on
  https://stackoverflow.com/a/2612451/4126114
  """
  size = 1440//3, 360//3 # 30% of original size
  glob_l = sorted(glob.glob(f"{dir_in}/*png"))
  for infile in glob_l:
    inext = os.path.splitext(infile)[1].replace(".","")

    # option: nested structure
    outfile = os.path.join(dir_out, os.path.basename(infile))

    # option: flat structure
    #print(dir_out)
    #dir_base   = os.path.basename(dir_out)
    #dir_parent = os.path.dirname(dir_out)
    #print(dir_base)
    #print(dir_parent)
    #in_base = os.path.basename(infile)
    #outfile = os.path.join(dir_parent, f"{dir_base}~{in_base}")

    #os.makedirs(dir_parent, exist_ok=True)

    #print(infile)
    #print(outfile)

    try:
      im = Image.open(infile)
      im.thumbnail(size)
      im.save(outfile, inext)
    except IOError as e:
      print("cannot create thumbnail for", infile)
      raise e


from . import dolphicom_version
def warn_dolphicom_version(version_alien):
    if version_alien != dolphicom_version:
      print(f"[WARNING] pkl file saved with dolphicom version {version_alien}, but installed version is {dolphicom_version}. Will try reading the file anyway.")


def to_dict_dolphicom(classname, d1):
    d2 = {
      "dolphicom": {
        "version": dolphicom_version,
        "class": classname,
        "data": d1
      }
    }
    return d2


def from_dict_dolphicom(d2, cls_expected, verbose=True):
  d2 = d2.copy() # fork from original dict
  if "dolphicom_version" in d2:
    # This is version 0.4.3 or 0.5.0 or earlier
    dv = d2["dolphicom_version"]
    del d2["dolphicom_version"]
    d2 = {"dolphicom": {"version": dv, "data": d2}}

  if "dolphicom" not in d2:
    raise ValueError(f"Not a dolphicom-formatted dict: {d2}")

  if verbose: warn_dolphicom_version(d2["dolphicom"]["version"])

  if "class" not in d2["dolphicom"]:
    if verbose: print("[warning] missing class field => cannot check if the from_pickle class of the right class is being called. Proceed with care.")
  else:
    cls_actual = d2['dolphicom']['class']
    if cls_actual != cls_expected:
      raise Exception(f"Expecting to load class {cls_expected} but found {cls_actual}. Aborting")

  return d2["dolphicom"]["data"]


def corpus_to_csv(s23_whistle_corpus, fn_csv):
  """
  Utility function to do some preprocessing on the whistle corpus before saving to csv
  """
  df_corpus = s23_whistle_corpus.copy()

  # drop some useless fields in the csv
  fx_del = ['S', 'S_samelen', 'S_zeroed', 'S_1d', 'S_gradient', 'S_str_2', 'label_cluster']
  for fx_i in fx_del:
    if fx_i in df_corpus.columns:
      del df_corpus[fx_i]

  # round decimals of some fields
  fx_round = [("t_sec_min",3), ("t_sec_max",3),
              ("f_hz_min",0), ("f_hz_max",0),
              ("t_len_sec",3), ("f_len_hz",0)]
  for fx_i, prec in fx_round: df_corpus[fx_i] = df_corpus[fx_i].round(prec)

  # extract to csv
  df_corpus.to_csv(fn_csv, index=False)


def os_touch(fn, s=""):
  with open(fn, "w") as f:
    f.write(s)


def readlines_strip(fn_d050_txt):
  with open(fn_d050_txt, "r") as f:
    fn_d050_l = f.readlines()
    fn_d050_l = [x.strip() for x in fn_d050_l]
    return fn_d050_l


from google.cloud import storage
from tqdm.auto import tqdm
import re
def gsutil_ls(bucket_name, n_max=None, prefix=None, suffix=None, suffix_re=None, drop_prefix=True):
  """
  https://cloud.google.com/storage/docs/listing-objects#storage-list-objects-python

  bucket_name: string
  n_max: maximum number of files to get. Pass None to get all.
  prefix: string, like suffix but prefix with startswith. Pass None to skip. Must end with "/" if passed.
  suffix: string to match with blob.name.endswith(suffix). Pass None to get all.
  suffix_re: like suffix, but from re.compile("spectrogram_png/*.png")
  drop_prefix: True|False. True to drop the <prefix> value from filenames
  """
  # prefix must end with /
  if prefix is not None:
    assert prefix[-1]=="/"

  storage_client = storage.Client()
  bucket_obj = storage_client.bucket(bucket_name)

  fn_mir_l = []
  #for x in bucket_obj.list_blobs():
  print(f"gsutil_ls(bucket_name={bucket_name}, n_max={n_max}, prefix={prefix}, suffix={suffix})")

  # tqdm with context manager: https://stackoverflow.com/a/67856383/4126114
  with tqdm(desc=f"gsutil ls") as pbar:
    for x in bucket_obj.list_blobs(prefix=prefix):
      pbar.update(1)
      do_append = True
      #if prefix is not None:
      #  do_append &= x.name.startswith(prefix)

      if suffix is not None:
        do_append &= x.name.endswith(suffix)

      if suffix_re is not None:
        do_append &= suffix_re.search(x.name) is not None

      if do_append:
        fn_mir_l.append(x.name)
        if n_max is not None:
          if len(fn_mir_l) >= n_max: break

  fn_mir_l = sorted(fn_mir_l)
  #len(fn_mir_l), fn_mir_l[:3]

  if drop_prefix:
    if prefix is not None:
      fn_mir_l = [x.replace(prefix, "") for x in fn_mir_l]

  print("Result of gsutil_ls...")
  print(f"\tn files: {len(fn_mir_l)}")
  print(f"\tFirst 3: {fn_mir_l[:3]}")
  print(f"\tLast 3: {fn_mir_l[-3:]}")

  return fn_mir_l
