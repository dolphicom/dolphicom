# https://docs.python.org/3/library/unittest.html
import unittest

class TestDatasets(unittest.TestCase):
    def test_DcldeSubset(self):
        from dolphicom.datasets import Dclde2020Mp3
        dirout_local = "test"
        dirout_remote = "dolphicom-ci-tests-dataset"
        dir_ws = "workspace"

        dclde = Dclde2020Mp3()
        dclde.only_whistles = False
        dclde.files_prefix = "dolphicom-ci-tests-dataset/"
        dclde.files_suffix=".mp3"

        assert len(dclde._get_path_l(dirout_remote)) == 2

        prgs = dclde.get_detector(dirout_local, dirout_remote, dir_ws)
        prgs.fit(n_jobs=1)
        prgs.generate_html_gallery("ci-test")
        prgs.upload_html_gallery(dry_run=True)
        assert 1==1
    


if __name__ == '__main__':
    unittest.main()
